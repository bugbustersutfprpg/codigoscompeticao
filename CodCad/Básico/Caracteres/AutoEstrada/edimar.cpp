#include <stdio.h>
#include <iostream>

using namespace std;

int main(){

    int n, t;
    string s;

    cin >>n>>s;

    t = 0;
    int i = 0;
    while(s[i]){
        if (s[i] == 'C' || s[i] == 'P')
            t += 2;
        else if (s[i] == 'A')
            t++;
        i++;
    }

    cout <<t<<"\n";
}