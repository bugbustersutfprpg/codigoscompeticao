#include<iostream>

using namespace std;

int main(){
    double a, b;
    char c;
    
    cin >>c>>a>>b;
    
    cout.precision(2);
    cout.setf(ios::fixed);
    
    if (c == 'M')
        cout <<(a*b)<<"\n";
    else
        cout <<(a/b)<<"\n";
}