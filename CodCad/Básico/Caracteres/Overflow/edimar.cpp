#include<iostream>

using namespace std;

int main(){
    int n, p, q;
    char c;
    
    cin >>n>>p>>c>>q;
    
    if (c == '+')
        p += q;
    else
        p *= q;
    
    if (p > n)
        puts("OVERFLOW");
    else
        puts("OK");
}