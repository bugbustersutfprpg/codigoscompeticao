#include <cmath>
#include <iostream>

using namespace std;

bool eh_primo(int x){
	if (x == 1 || (x%2) == 0)
	    return false;
	double a = sqrt(x);
	for (int i = 3; i <= a; i+=2){
	    if (x%i == 0)
	        return false;
	}
	return true;
}

int main(){
	int x;

	cin>>x;

	if(eh_primo(x)){
		cout<<"S"<<"\n";
	}else{
		cout<<"N"<<"\n";
	}
}