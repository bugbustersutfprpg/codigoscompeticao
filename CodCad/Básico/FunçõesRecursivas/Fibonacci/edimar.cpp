#include <iostream>

using namespace std;

int main(){

    int ant, fib, i;

    cin>>i;

    ant = 1;
    fib = 1;
    for(int j = 1; j < i; j++){
        fib += ant;
        ant = fib - ant;
    }

    cout<<fib<<"\n";
}