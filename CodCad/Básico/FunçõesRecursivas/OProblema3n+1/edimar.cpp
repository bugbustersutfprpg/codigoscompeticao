#include <iostream>

using namespace std;

int main(){

    int i, cont;
    cin>>i;

    if (i == 1)
        cout<<"0\n";
    else{
        cont = 0;
        while(i != 1){
            if (i%2 == 0)
                i /= 2;
            else
                i = i*3+1;
            cont++;
        }
        cout<<cont<<"\n";
    }
}