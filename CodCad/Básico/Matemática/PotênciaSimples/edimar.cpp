#include <iostream>
#include <cmath>

using namespace std;

int main(){

    double a, b;

    cout.precision(4);
    cout.setf(ios::fixed);

    cin >>a>>b;

    cout<<pow(a, b)<<"\n";

}