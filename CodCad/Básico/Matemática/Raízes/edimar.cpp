#include <iostream>
#include <cmath>
//#include <stdio.h>

using namespace std;

int main(){

    int n;
    double x;

    cin >>n;

    while(n--){
        cin>>x;
        cout.precision(4);
        cout.setf(ios::fixed);
        cout<<(double)sqrt(x)<<"\n";
    }

}