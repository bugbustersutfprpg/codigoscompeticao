#include <iostream>
#include <cmath>

using namespace std;

int main(){

    double b;
    int l, a, p, r;
    cin>>l>>a>>p>>r;

    b = sqrt(l*l + a*a + p*p);
    r *= 2;

    if (b <= r)
        cout<<"S\n";
    else
        cout<<"N\n";

}