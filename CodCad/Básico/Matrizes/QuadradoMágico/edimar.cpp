#include <stdio.h>
#include <iostream>

using namespace std;

int main(){

    int m[10][10];
    int n, i, j, t, total, bo;

    cin >>n;

    total = 0;
    bo = 0;
    i = 0;
    while(i < n){
        j = 0;
        t = 0;
        while(j < n){
            cin >>m[i][j];
            t += m[i][j++];
        }
        if (i == 0)
            total = t;
        else if (total != t)
            bo = -1;
        i++;
    }

    if (bo == -1)
        cout <<bo<<"\n";
    else{
        bo = total;
        for (i = 0; i < n; i++){
            t = 0;
            for (j = 0; j < n; j++){
                t += m[j][i];
            }
            if (t != total){
                bo = -1;
                break;
            }
        }

        if (bo == -1)
            cout <<bo<<"\n";
        else{
            j = 0;
            t = 0;
            for (i = 0; i < n; i++){
                t += m[i][j++];
            }
            if (t != total)
                cout <<-1<<"\n";
            else{
                j = n;
                t = 0;
                for (i = 0; i < n; i++){
                    t += m[i][--j];
                }
                if (t != total)
                    cout <<-1<<"\n";
                else
                    cout <<t<<"\n";
            }
        }
    }

}