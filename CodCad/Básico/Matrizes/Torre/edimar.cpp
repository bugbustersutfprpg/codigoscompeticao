#include <stdio.h>
#include <iostream>

#define T 1000
using namespace std;


int main(){

    int i, j, n, maior, cont, linha[T], coluna[T];
    short int m[T][T];

    cin >>n;

    maior = 0;
    for (i = 0; i < n; i++){
        linha[i] = 0;
        for (j = 0; j < n; j++){
            cin >>m[i][j];
            linha[i] += m[i][j];
        }
        //printf("linha %d = %d\n", i, linha[i]);
    }

    for (i = 0; i < n; i++){
        coluna[i] = 0;
        for (j = 0; j < n; j++){
            coluna[i] += m[j][i];
        }
        //printf("coluna %d = %d\n", i, coluna[i]);
    }

    for (i = 0; i < n; i++){
        for (j = 0; j < n; j++){
            cont = linha[i] + coluna[j] - (m[i][j] * 2);
            //printf("%d %d - %d = %d\n", linha[i], coluna[j], m[i][j]*2, cont);
            if (cont > maior)
                maior = cont;
        }
    }

    cout << maior<<"\n";
}