#include <iostream>
#include <algorithm>

using namespace std;

struct Pais{

    int ouro;
    int prata;
    int bronze;
    int id;

};

bool compara(Pais a, Pais b){

    if (a.ouro > b.ouro)
        return true;
    else if (a.ouro < b.ouro)
        return false;
    else{
        if (a.prata > b.prata)
            return true;
        else if (a.prata < b.prata)
            return false;
        else{
            if (a.bronze > b.bronze)
                return true;
            else if (a.bronze < b.bronze)
                return false;
            else{
                if (a.id < b.id)
                    return true;
                else
                    return false;
            }
        }
    }
}

int main(){

    int i, n, m, o, p, b;
    Pais pais[100];

    cin >>n>>m;
    for (i = 0; i < n; i++){
        pais[i].id = i+1;
        pais[i].ouro = 0;
        pais[i].prata = 0;
        pais[i].bronze = 0;
    }

    for (i = 0; i < m; i++){
        cin>>o>>p>>b;
        pais[o-1].ouro++;
        pais[p-1].prata++;
        pais[b-1].bronze++;
    }

    sort(pais, pais+n, compara);

    n--;
    for (i = 0; i < n; i++){
        cout<<pais[i].id<<" ";
    }
    cout<<pais[i].id<<"\n";
}