#include <iostream>
#include <algorithm>

using namespace std;

int m;

int compara(const void *a, const void *b){

    int x, y, i, j;
    i = *(int*)a;
    j = *(int*)b;
    x = i%m;
    y = j%m;


    if (x > y)
        return 1;
    if (x < y)
        return -1;
    if (x == y){
        if (i > j)
            return 1;
        else if (i < j)
            return -1;
        else
            return 0;
    }

}

int main(){

    int i, j, k, n, vetor[100000], cop[100000];

    cin>>n>>m;

    for(i = 0; i < n; i++){
        cin>>vetor[i];
    }

    qsort(vetor, n, sizeof(int), compara);

    for(i = n-1; i > 0; i--){
        cout<<vetor[i]<<" ";
    }
    cout<<vetor[0]<<"\n";

}