#include <iostream>
#include <algorithm>

using namespace std;

int main(){

    int i, cont, n, vetor[100000], cop[100000];

    cin>>n;

    for(i = 0; i < n; i++){
        cin>>vetor[i];
        cop[i] = vetor[i];
    }
    sort(cop, cop+i);

    cont = 0;
    for(i = 0; i < n; i++){
        if (cop[i] != vetor[i]){
            cop[cont++] = cop[i];
        }
    }

    if (cont == 0){
        cout<<"0\n";
        return 0;
    }

    cout<<cont<<"\n"<<cop[0];
    for(i = 1 ;i < cont; i++){
        cout<<" "<<cop[i];
    }
    cout<<"\n";
}