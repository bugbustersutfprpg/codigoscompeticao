#include <iostream>
#include <algorithm>

using namespace std;

int main(){

    int i, v[100000];
    cin>>i;

    for(int j= 0; j<i; j++){
        cin>>v[j];
    }

    sort(v, v+i);

    cout<<v[0];
    for(int j=1; j<i; j++){
        cout<<" "<<v[j];
    }
    cout<<"\n";
}