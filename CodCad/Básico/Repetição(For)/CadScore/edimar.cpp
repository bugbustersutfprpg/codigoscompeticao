#include<iostream>

using namespace std;

int main(){
  int n, x, p;
  
    cin >>p>>n;
    
    while(n--){
        cin >>x;
        p += x;
        if (p < 0)
            p = 0;
        if (p > 100)
            p = 100;
    }
    cout <<p<<"\n";
}