#include<iostream>
#define T 1000000


using namespace std;

int main(){
  int x, n, total, i, cont, stop;
  
    cin >>n;
    
    stop = 1;
    i = 0;
    total = 0;
    while(i++ < n){
        cin >>x;
        total += x;
        
        if (total >= T && stop){
            cont = i;
            stop = 0;
        }
    }
    cout <<cont<<"\n";
}