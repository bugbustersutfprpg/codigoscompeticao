#include <iostream>

using namespace std;

int main(){
	
	int n, s, menor, i;
	
	cin>>n>>s;
	menor = s;
	
	while(n--){
		cin>>i;
		s += i;
		if (s < menor)
			menor = s;
	}
	
	cout <<menor<<endl;
}