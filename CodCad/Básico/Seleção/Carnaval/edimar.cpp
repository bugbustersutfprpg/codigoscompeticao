#include <iostream>
#include <algorithm>

using namespace std;

int main(){
	
	float n[5], total;
	total = 0;
	
	cin>>n[0]>>n[1]>>n[2]>>n[3]>>n[4];
	
	sort(n, n+5);
	total = n[1] + n[2] + n[3];
	
	cout.precision(1);
	cout.setf(ios::fixed);
	cout<<total<<endl;
}