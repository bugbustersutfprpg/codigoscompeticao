#include <stdio.h>
#include <string.h>

int main() {

    char s[64];

    scanf("%s", s);

    int t = strlen(s)-1;
    int i = 0;
    while(i < t){

        while (s[i] != 'a' && s[i] != 'e' && s[i] != 'i' && s[i] != 'o' && s[i] != 'u'){
            i++;
            if (!s[i])
                break;
        }

        while (s[t] != 'a' && s[t] != 'e' && s[t] != 'i' && s[t] != 'o' && s[t] != 'u'){
            t--;
            if (t < 0)
                break;
        }
        //printf("%c %c\n", s[i], s[t]);
        if (s[i] != s[t])
            break;
        if (i == t)
            break;
        i++;
        t--;
    }

    if (i < t)
        puts("N");
    else
        puts("S");

    return 0;
}