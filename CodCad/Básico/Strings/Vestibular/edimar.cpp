#include <stdio.h>

int main(){

    int n, cont = 0;
    char g[128], p[128];

    scanf("%d %s %s", &n, g, p);

    while(n--){
        if (g[n] == p[n])
            cont++;
    }

    printf("%d\n", cont);

}