#include <iostream>
#include <algorithm>

using namespace std;

int n, b, v[92];

bool da(int i);

//##########################################################

int main(){
	
	int i;
	bool bo;
	
	while(1){
		cin>>n>>b;
		if (n == 0 && b == 0)
			break;
		
		i = 0;
		while(i < b){
			cin>>v[i];
			i++;
		}
		
		sort(v, v+b);
				
		bo = true;
		for (i = 1; i <= n; i++){
			bo = da(i);
			if (!bo){
				//cout<<i<<endl;
				break;
			}
		}
		
		if (bo)
			cout<<"Y\n";
		else
			cout<<"N\n";
	}
}

//##########################################################

bool da(int i){
	int l = 1;
	int m;
	for (int j = v[b-l]; (b-l) > 0 ; j = v[b-l]){
		m = l+1;
		for (int k = v[b-m]; ; k = v[b-m]){
			if ( (j - k) == i ){
				//cout<<j<<" "<<k<<endl;
				return true;
			}
			if ( (j - k) > i){
				//cout<<"Nao vai mais "<<j<<" "<<k<<endl;
				break;
			}
			m++;
			if ( (b-m) < 0)
				break;
		}
		l++;
	}
	//cout<<"False\n";
	return false;
}