#include <iostream>

using namespace std;

int v[1000], v1[1000];

int main(){

    int i, m, n;
    bool flag = 0, last = 1;

    cin >> m >> n;

    for (i = 0; i < m; i++){
        cin >> v[i];
    }
    for (i = 0; i < n; i++){
        cin >> v1[i];
    }

    if (m < n){
        int aux = m;
        m = n;
        n = aux;
    }

    for (i = m-1; i >= 0; i--){
        v[i] += v1[i] + (int)flag;
        if (v[i] == 2){
            v[i] = 0;
            flag = 1;
        }else if (v[i] == 3){
            v[i] = 1;
            flag = 1;
        }else{
            flag = 0;
        }
        if (last && v[i] == 0){
            m--;
        }else{
            last = 0;
        }
    }

    if (m == 1){
        cout << v[0] << endl;
    }else{
        m--;
        for (i = 0; i < m; i++){
            cout << v[i] << " ";
        }
        cout << v[i] << endl;
    }
}