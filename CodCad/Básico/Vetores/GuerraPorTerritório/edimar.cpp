#include <stdio.h>
#include <iostream>

using namespace std;

int main(){

    int n, i, total, valor, vetor[100000];
    float metade;

    cin >>n;
    total = 0;

    i = 0;
    while(i < n){
        cin >>vetor[i];
        total += vetor[i++];
    }

    metade = (float)total/2;
    valor = 0;

    i = 0;
    while(valor < metade){
        valor += vetor[i++];
    }

    cout <<i<<"\n";
}