#include <stdio.h>
#include <iostream>

using namespace std;

int main(){

    int b, i, n, banco[21], d, c, v;

    while(1){
        cin >>b>>n;
        if (!b && !n)
            break;

        for (i = 1; i <= b; i++){
            cin >>banco[i];
        }
        for (i = 0; i < n; i++){
            cin >>d>>c>>v;
            banco[d] -= v;
            banco[c] += v;
        }

        for(i = 1; i <= b; i++){
            if (banco[i] < 0){
                cout << "N\n";
                break;
            }
        }
        if (i > b)
            cout << "S\n";
    }
}