#include <bits/stdc++.h>

using namespace std;
#define T 301
#define F first
#define S second
typedef pair<int, int> ii;
typedef pair<ii, ii> iii;

struct compara{
    bool operator()(iii a, iii b){
        if (a.F.F == b.F.F){
            return a.F.S > b.F.S;
        }
        return a.F.F > b.F.F;
    }
};

priority_queue<iii, vector<iii>, compara > fila;
int m[T][T], n, ans[T][T], len[T][T];

void update(iii v, int a, int b){

    if (v.F.F < ans[a][b])
        ans[a][b] = max(v.F.F, m[a][b]);
    if (v.F.S+1 < len[a][b])
        len[a][b] = v.F.S+1;

    fila.push(iii( ii(max(v.F.F, m[a][b]), v.F.S+1), ii(a, b) ));
}

void inicializa(){

    for (int i = 1; i <= n; i++){
        for (int j = 1; j <= n; j++){
            ans[i][j] = INT_MAX;
            len[i][j] = INT_MAX;
        }
    }
    ans[1][1] = m[1][1];
    len[1][1] = 1;
    fila.push(iii(ii(m[1][1], 1), ii(1, 1) ));
}

void djkstra(){

    inicializa();
    int x, y;
    iii v;
    while(!fila.empty()){
        v = fila.top();
        fila.pop();
        x = v.S.F;
        y = v.S.S;
        if (x == n && y == n) {
            ans[n][n] = v.F.F;
            len[n][n] = v.F.S;
            return;
        }
        if (x-1 > 0 && (v.F.F < ans[x-1][y] || v.F.S < len[x-1][y]) )
            update(v, x-1, y);
        if (y+1 <= n && (v.F.F < ans[x][y+1] || v.F.S+1 < len[x][y+1]) )
            update(v, x, y+1);
        if (x+1 <= n && (v.F.F < ans[x+1][y] || v.F.S+1 < len[x+1][y]) )
            update(v, x+1, y);
        if (y-1 > 0 && (v.F.F < ans[x][y-1] || v.F.S+1 < len[x][y-1]) )
            update(v, x, y-1);
    }
}

int main(){

    cin >> n;

    for (int i = 1; i <= n; i++){
        for (int j = 1; j <= n; j++){
            scanf("%d", &m[i][j]);
        }
    }

    djkstra();

    cout << ans[n][n] << " " << len[n][n] << endl;
}
