#include <bits/stdc++.h>

using namespace std;
const double menor = -2e30;

long long uniao(vector<double> &v){

    if (v.size() == 1) return 0;

    long long t = 0;
    vector<double> v1, v2;
    int metade = v.size() / 2;

    for (int i = 0; i < metade; i++){
        v1.push_back(v[i]);
    }
    for (int i = metade; i < v.size(); i++){
        v2.push_back(v[i]);
    }

    t += uniao(v1);
    t += uniao(v2);

    v1.push_back(menor);
    v2.push_back(menor);

    int j = 0, k = 0;
    for(int i = 0; i < v.size(); i++){
        if (v1[j] > v2[k]){
            v[i] = v1[j++];
        }else{
            v[i] = v2[k++];
            t += v1.size() - j - 1;
        }
    }

    return t;
}

int main(){

    long long x, y, n;
    vector<double> v;
    cin >> n;

    for (int i = 0; i < n; i++){
        cin >> x >> y;
        v.push_back(sqrt(x*x + y*y));
    }

    cout << uniao(v) << endl;
}