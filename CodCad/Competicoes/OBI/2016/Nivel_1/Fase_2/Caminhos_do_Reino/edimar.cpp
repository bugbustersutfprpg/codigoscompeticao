#include <bits/stdc++.h>

using namespace std;
#define T 100001

int v[T], chega[T];
int n, x, ciclo, dist[T], grupo[T], gru;
map<int, int> maxDist;
map<int, int> destino;

int busca(int a, int b){

    if (grupo[a] == grupo[b]) {
        int m = maxDist[grupo[a]];
        //se estiverem no ciclo entra no if
        if (grupo[a] == gru) return min(abs(dist[a]-dist[b]),
                                        dist[a] < dist[b] ? dist[a] + m - dist[b] : dist[b] + m - dist[a]);//calcula a diferença
                                                                                                        //completando a volta
        return abs(dist[a] - dist[b]);
    }

    int da = 0, db = 0, destA = a, destB = b;
    if (grupo[a] != gru){   //se entrar no if, a não faz parte do ciclo
        da = maxDist[grupo[a]] - dist[a];   //calcula distância percorrida até chegar no ciclo
        destA = destino[grupo[a]];  //nó do ciclo que o a chegou
    }
    if (grupo[b] != gru){
        db = maxDist[grupo[b]] - dist[b];
        destB = destino[grupo[b]];
    }

    int xa, xb;
    if (dist[destA] < dist[destB]){
        xa = dist[destB] - dist[destA]; //distancia de a para chegar em b dentro do ciclo
        xb = (dist[destA] + maxDist[grupo[destA]]) - dist[destB];
    }else{
        xb = dist[destA] - dist[destB];
        xa = (dist[destB] + maxDist[grupo[destA]]) - dist[destA];
    }

    xa = max(xa+da, db);
    xb = max(xb+db, da);

    return min(xa, xb);
}

int main(){

    scanf("%d", &n);
    for (int i=1; i <= n; i++){
        scanf("%d", &v[i]);
        chega[v[i]]++;
        if (chega[v[i]] == 2) ciclo = v[i];
    }

    for (int i=1; i <= n;i++){
        if (!chega[i]){
            int j, k = i;
            for (j = 0; chega[i] != 2; j++){
                dist[i] = j;
                grupo[i] = gru;
                i = v[i];
            }
            maxDist[gru] = j;
            destino[gru] = i;
            gru++;
            i = k;
        }
    }

    int ci = v[ciclo];
    dist[ciclo] = 0;
    grupo[ciclo] = gru;
    int k = 1;
    while(ci != ciclo){
        dist[ci] = k++;
        grupo[ci] = gru;
        ci = v[ci];
    }
    maxDist[gru] = k;

    int q, a, b;
    scanf("%d", &q);
    for (int i=0; i < q; i++){
        scanf("%d %d", &a, &b);
        cout << busca(a, b) << endl;
    }

}
