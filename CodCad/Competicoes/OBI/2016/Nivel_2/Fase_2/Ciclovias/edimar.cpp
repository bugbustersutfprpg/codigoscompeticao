#include <bits/stdc++.h>

using namespace std;
#define T 100010
#define PB push_back

vector<int> v[T];
int resp[T], maior[T];

int main(){

    int a, b, n, m;

    cin >> n >> m;

    for (int i = 0; i < m; i++){
        cin >> a >> b;
        v[a].PB(b);
        v[b].PB(a);
    }

    for (int i = 1; i <= n; i++) sort(v[i].begin(), v[i].end());

    fill_n(resp, T, 1);
    for (int i = n; i > 0; i--){
        maior[v[i].size()] = 0;
        for (int j = v[i].size()-1; j > 0; j--){
            int u = v[i][j];
            maior[j] = max(resp[u], maior[j+1]);
        }
        for (int j = 0; j < v[i].size(); j++){
            int u = v[i][j];
            resp[u] = max(resp[u], 2+maior[j+1]);
        }
    }

    for (int i = 1; i < n; i++){
        cout << resp[i] << " ";
    }
    cout << resp[n] << endl;
}