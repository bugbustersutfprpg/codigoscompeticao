#include <bits/stdc++.h>

using namespace std;
#define T 41

int primo[T], n, k;

long long repete(long long base, int i, int alt){

    if (base > n) return 0;

    long long total = 0;
    for (int j = i; j < k; j++)
        total += repete(base * primo[j], j+1, alt+1);

    if (base != primo[i-1]){
        if (alt%2)
            total += n/base;
        else
            total -= n/base;
    }

    return total;
}

int main(){

    long long total = 0;
    cin >> n >> k;

    for (int i=0; i < k; i++){
        scanf("%d", &primo[i]);
        total += n / primo[i];
    }

    //sort(primo, primo+k);
    for (int i=0; i < k; i++)
        total -= repete(primo[i], i+1, 0);

    cout << n - total << endl;
}
