#include <iostream>

using namespace std;

int n, m, v[10000];

long long int contar(int i, int ant){

    if (i == n)
        return 0;

    int menor = m - v[i];
    if (menor > v[i]){
        int aux = menor;
        menor = v[i];
        v[i] = aux;
    }

    if (v[i] < ant) return -1;

    long long int cont;
    if (menor >= ant){
        cont = contar(i+1, menor);
        if (cont == -1){
            cont = contar(i+1, v[i]);
            if (cont == -1) return -1;
            return cont + v[i];
        }
        return cont + menor;
    }else{
        cont = contar(i+1, v[i]);
        if (cont == -1) return -1;
        return cont + v[i];
    }
}

int main(){

    cin >> n >> m;

    for (int i = 0; i < n; i++){
        cin >> v[i];
    }

    cout << contar(0, 0) << endl;
}