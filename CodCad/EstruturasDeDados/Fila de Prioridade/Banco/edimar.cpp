#include <iostream>
#include <queue>

using namespace std;

int main(){
	
	long long int caixa, cliente, tempo, demora, total = 0, i = 0, t;
	priority_queue<long long int> fila;
	
	cin >> caixa >> cliente;
	
	while(cliente--){
		cin >> tempo >> demora;
		
		if (i < caixa){
			demora += tempo;
			fila.push(-demora);
		}else{
			t = -fila.top();
			fila.pop();
			
			if (tempo < t){
				if ( (t - tempo) > 20){
					total++;
				}	
				demora += t;	
			}else{
				demora += tempo;
			}
			
			fila.push(-demora);
		}
		i++;
	}
	
	cout << total << endl;
}