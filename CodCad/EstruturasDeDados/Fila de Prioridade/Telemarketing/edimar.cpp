#include <iostream>
#include <queue>

using namespace std;

int v[1001];
typedef pair<int, int> P;
priority_queue< P, vector<P>, greater<P> > fila;


int main(){

    int vendedor, ligacao, tempo;
    //priority_queue< pair<int, int> > fila;
    pair<int, int> top;

    cin >> vendedor >> ligacao;

    for (int i = 1; i <= ligacao; i++){
        cin >> tempo;

        if (i <= vendedor){
            fila.push(make_pair(tempo, i) );
            v[i] = 1;
        }else{
            top = fila.top();
            //cout << "Retirou o vendedor " << top.second << " com " << top.first << " minutos" << endl;
            v[top.second]++;
            fila.pop();
            fila.push(make_pair(tempo + top.first, top.second));
        }
    }

    for (int i = 1; i <= vendedor; i++){
        cout << i << " " << v[i] << endl;
    }

}