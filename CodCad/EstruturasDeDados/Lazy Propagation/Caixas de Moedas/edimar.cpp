#include <iostream>
#define T 270000

using namespace std;
int lazy[T], arvore[T];

int acharMax(int n){
    int x;
    for (x = 1 ; x < n; x <<= 1){    }
    return x;
}

void atualiza(int no, int i, int j, int a, int b, int valor){

    int esq = no * 2;
    int dir = no * 2 + 1;
    int meio = (i + j) / 2;

    if (lazy[no]){
        arvore[no] = lazy[no] * (j - i + 1);
        if (i != j){
            lazy[esq] = lazy[no];
            lazy[dir] = lazy[no];
        }
        lazy[no] = 0;
    }

    if (i > j || a > j || b < i) return;

    if (a <= i && b >= j){
        arvore[no] = valor * (j - i + 1);
        if (i != j){
            lazy[esq] = valor;
            lazy[dir] = valor;
        }
    }else{
        atualiza(esq, i, meio, a, b, valor);
        atualiza(dir, meio+1, j, a, b, valor);

        arvore[no] = arvore[esq] + arvore[dir];
    }
}

int consultar(int no, int i, int j, int a, int b){

    int esq = no * 2;
    int dir = no * 2 + 1;
    int meio = (i + j) / 2;

    if (lazy[no]){
        arvore[no] = lazy[no] * (j - i + 1);
        if (i != j){
            lazy[esq] = lazy[no];
            lazy[dir] = lazy[no];
        }
        lazy[no] = 0;
    }

    if (i > j || a > j || b < i) return 0;

    if (a <= i && b >= j){
        return arvore[no];
    }else{
        int x = consultar(esq, i, meio, a, b);
        int y = consultar(dir, meio+1, j, a, b);

        return x + y;
    }
}

void imprimir(int n){

    int j = 1, k = 1;
    for (int i = 1; i <= n; i++){
        cout << arvore[i] << " ";
        if (j == k++){
            cout << endl;
            j <<= 1;
            k = 1;
        }
    }
    cout << endl;
}

int main(){

    int n, q, v, inicio, fim, op, a, b;

    cin >> n >> q;

    inicio = acharMax(n);
    fim = inicio+n-1;
    //cout << inicio << " " << fim << endl;

    for (int i = 0; i < n; i++){
        cin >> v;
        atualiza(1, inicio, fim, inicio+i, inicio+i, v);
        //imprimir(fim);
    }


    for (int i = 0; i < q; i++){
        cin >> op >> a >> b;
        a += inicio - 1;
        b += inicio - 1;
        if (op == 1){
            cin >> v;
            atualiza(1, inicio, fim, a, b, v);
        }else{
            v = consultar(1, inicio, fim, a, b);
            cout << v << endl;
        }
    }
}