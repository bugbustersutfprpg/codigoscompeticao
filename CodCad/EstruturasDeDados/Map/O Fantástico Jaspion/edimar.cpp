#include <iostream>
#include <map>

using namespace std;

int main(){

    map<string, string> word;
    map<string, string>::iterator it;
    int inst, i, j, n, musica;
    string japa, port, s;

    cin >> inst;

    while(inst--){
        cin >> n >> musica;

        while(n--){
            cin >> japa;
            getline(cin, port);
            getline(cin, port);

            word[japa] = port;
        }

        while(musica--){
            getline(cin, japa);
            while(1){
                j = japa.find(' ');
                if (j < japa.size()){
                    s = japa.substr(0, j);
                    it = word.find(s);
                    if (it != word.end()){
                        cout << it->second << " ";
                    }else{
                        cout << s << " ";
                    }
                    japa = japa.substr(j+1);
                }else{
                    break;
                }
            }
            it = word.find(japa);
            if (it != word.end()){
                cout << it->second << endl;
            }else{
                cout << japa << endl;
            }
        }

        if (musica){
            word.clear();
            cout << endl;
        }
    }
}