#include <iostream>
#include <map>
#include <vector>

using namespace std;

int main(){

    int i, a, b, x, y;
    map<int, int> empresa, atualiza;
    map<int, int>::iterator it, it2;

    cin>>a>>b;

    for(i = 0; i < a; i++){
        cin>>x>>y;
        empresa[x] = y;
    }

    for(i = 0; i < b; i++){
        cin>>x>>y;
        it = empresa.find(x);
        if (it != empresa.end()){
			it2 = atualiza.find(x);
			if(it2 != atualiza.end()){
				if (it2->second < y){
					it2->second = y;
				}
			}else{
				if (it->second < y)
					atualiza[x] = y;
			}
		}else{
			atualiza[x] = y;
			empresa[x] = y;
		}
	}

    for(it2 = atualiza.begin(); it2 != atualiza.end(); it2++){
        cout<<it2->first<<" "<<it2->second<<endl;
    }
}