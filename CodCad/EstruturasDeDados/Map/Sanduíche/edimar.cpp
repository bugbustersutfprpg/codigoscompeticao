#include <iostream>

using namespace std;

int v[1000001];

int main(){

    int total, soma, n, d, first;

    cin >> n >> d;

    soma = 0;
    first = 0;
    total = 0;
    for (int i = 0; i < n; i++){
        cin >> v[i];
        soma += v[i];
        if (soma == d){
            total++;
            soma -= v[first++];
        }else if (soma > d){
            while(soma > d){
                soma -= v[first++];
            }
            if (soma == d){
                total++;
                soma -= v[first++];
            }
        }
    }
    for (int i = 0; first < n && first > i; i++){
        soma += v[i];
        if (soma == d){
            total++;
            soma -= v[first++];
        }else if (soma > d){
            while(soma > d && first < n){
                soma -= v[first++];
            }
            if (first >= n) break;
            if (soma == d){
                total++;
                soma -= v[first++];
            }
        }
    }

    cout << total << endl;
}