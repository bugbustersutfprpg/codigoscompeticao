#include <iostream>

using namespace std;
#define T 10001

int main(){

    int p, s, i, ini, fim, vi[T], vf[T];

    cin>>p>>s;
    for(i = 0; i < T; i++){
        vi[i] = 0;
        vf[i] = 0;
    }
    for(i = 0; i < s; i++){
        cin>>ini>>fim;
        vi[ini]++;
        vf[fim]++;
    }

    ini = 0;
    fim = 0;
    for (i = 0; i < T; i++){
        if (vi[i]){
            if (fim == ini)
                cout<<i<<" ";
            ini += vi[i];
        }
        if (vf[i]){
            fim += vf[i];
            if (fim == ini)
                cout<<i<<"\n";
        }
    }
    cout<<"\n";
}