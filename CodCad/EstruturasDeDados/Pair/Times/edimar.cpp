#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

int main(){

    int n, i, j, t, a, cont;
    string b;

    vector<string> s;
    vector< pair<int, string> > v;

    cin>>n>>t;

    for(i = 0; i < n; i++){
        cin>>b>>a;
        v.push_back(make_pair(a, b));
    }
    sort(v.begin(), v.end());

    for (i = 0; ; i++){
        cont  = 0;
        for (j = n-1-i; j >= 0; j-=t){
            s.push_back(v[j].second);
            //cout<<s[cont]<<"\n";
            cont++;
        }
        sort(s.begin(), s.end());
        cout<<"Time "<< i+1 <<"\n";
        for (j = 0; j < cont; j++){
            cout<<s[j]<<"\n";
        }

        if ((i+1) == t)
            break;
        cout<<"\n";
        s.clear();
    }
}