#include <iostream>
#include <set>

using namespace std;

int main(){

    set<int> taco;
    int n, tac, t;

    cin >> n;

    t = 0;
    while(n--){
        cin >> tac;
        if (taco.find(tac) != taco.end()){
            taco.erase(tac);
        }else{
            taco.insert(tac);
            t++;
        }
    }

    cout << t*2 << endl;
}