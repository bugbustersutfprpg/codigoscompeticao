#include <iostream>
#include <set>

using namespace std;

int main(){

    int a, b, i, x, alice, beatriz;
    set<int> al;
    set<int> be;

    cin>>a>>b;

    for(i = 0; i < a; i++){
        cin>>x;
        al.insert(x);
    }
    for(i = 0; i < b; i++){
        cin>>x;
        be.insert(x);
    }

    alice = 0;
    beatriz = 0;
    for (set<int>::iterator it = al.begin(); it != al.end(); ++it){
        if (be.find(*it) == be.end())
            alice++;
    }
    for (set<int>::iterator it = be.begin(); it != be.end(); ++it){
        if (al.find(*it) == al.end())
            beatriz++;
    }

    if (beatriz < alice)
        cout<<beatriz<<"\n";
    else
        cout<<alice<<"\n";
}