#include <iostream>
#include <stack>

using namespace std;
#define T 100001

int main(){

	int a, b, i;
	char c, st[T];
	stack<char> s;

	while(1){
		cin >> a >> b;
		if (a == 0)
			break;

		cin >> c;
		s.push(c);
		a--;
		while(a--){
			cin >> c;
			if (b){
                if (c > s.top()){
                    s.pop();
                    b--;
                    while(!s.empty() && c > s.top() && b){
                        s.pop();
                        b--;
                    }
                    s.push(c);
                }else{
                    s.push(c);
                }
			}else{
                s.push(c);
			}
		}

        for ( ; b; b--){
            s.pop();
        }

        int t = s.size();
		for (i = 0; i < t; i++){
            st[i] = s.top();
            s.pop();
		}

		for (i-- ; i >= 0; i--){
            cout << st[i];
		}
		cout << endl;
	}
}