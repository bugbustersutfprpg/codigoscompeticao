#include <iostream>
#include <stack>

using namespace std;

int main(){

    int i, j, n;
    string s;
    bool b = true;
    stack<char> c;

    cin>>n;

    getline(cin, s);

    for(i = 0; i < n; i++){
        getline(cin, s);
        if (b){
            for (j = 0; s[j]; j++){
                if (s[j] == '{')
                    c.push('{');
                else if (s[j] == '}'){
                    if (c.empty()){
                        b = false;
                        break;
                    }else
                        c.pop();
                }
            }
        }
    }
    if (b && c.empty())
        cout<<"S\n";
    else
        cout<<"N\n";
}