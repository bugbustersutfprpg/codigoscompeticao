#include <iostream>
#include <stack>

using namespace std;

int main(){

    int t, i, j;
    bool b;
    string s;
    stack<char> c;

    cin>>t;

    for (i = 0; i < t; i++){
        b = true;
        cin>>s;
        for(j = 0; s[j]; j++){
            if (s[j] == '(' || s[j] == '[' || s[j] == '{')
                c.push(s[j]);
            else if (s[j] == ')'){
                if (c.empty() || c.top() != '('){
                    b = false;
                    break;
                }else
                    c.pop();
            }else if (s[j] == ']'){
                if (c.empty() || c.top() != '['){
                    b = false;
                    break;
                }else
                    c.pop();
            }else if (s[j] == '}'){
                if (c.empty() || c.top() != '{'){
                    b = false;
                    break;
                }else
                    c.pop();
            }
        }
        if (b && c.empty())
            cout<<"S\n";
        else
            cout<<"N\n";

        while(!c.empty()){
            c.pop();
        }
    }
}