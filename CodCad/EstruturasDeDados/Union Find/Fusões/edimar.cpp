#include <iostream>

using namespace std;
#define MAX 100001
int peso[MAX];
int banco[MAX];

int find(int x){
	
	if (banco[x] == x){
		return x;
	}
	
	return banco[x] = find(banco[x]);
}

void join(int x, int y){
	
	int a, b;
	
	a = find(x);
	b = find(y);
	
	if (a == b){
		return;
	}
	
	if (peso[a] < peso[b]){
		banco[a] = b;
	}else if (peso[a] > peso[b]){
		banco[b] = a;
	}else{
		banco[a] = b;
		peso[b]++;	
	}
}

int main(){
	
	int b, o, x, y;
	char op;
	
	cin >> b >> o;
	
	for (int i = 1; i < MAX; i++){
		banco[i] = i;
	}
	
	while(o--){
		
		cin >> op >> x >> y;	
		
		if (op == 'F'){
			join(x, y);
		}else{
			if (find(x) == find(y)){
				cout << "S\n";
			}else{
				cout << "N\n";
			}
		}
	}
}