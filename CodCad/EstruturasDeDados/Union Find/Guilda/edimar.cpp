#include <iostream>

using namespace std;
#define MAX 100001
int peso[MAX];
int banco[MAX];
int qtde[MAX];

int find(int x){
	
	if (banco[x] == x){
		return x;
	}
	
	return banco[x] = find(banco[x]);
}

void join(int x, int y){
	
	int a, b;
	
	a = find(x);
	b = find(y);
	
	if (a == b){
		return;
	}
	
	if (peso[a] < peso[b]){
		banco[a] = b;
		qtde[b] += qtde[a];
	}else if (peso[a] > peso[b]){
		banco[b] = a;
		qtde[a] += qtde[b];
	}else{
		banco[a] = b;
		peso[b]++;	
		qtde[b] += qtde[a];
	}
}

int main(){
	
	int b, o, x, y, vitoria, w, z, k;
	char op;
	
	
	
	cin >> b >> o;
	
	for (int i = 1; i <= b; i++){
		banco[i] = i;
	}
	
	while(1){
		
		for (int i = 1; i <= b; i++){
			cin >> qtde[i];
		}
		
		//cout << "Entrou no laco\n";
		vitoria = 0;
		while(o--){
			
			cin >> op >> x >> y;	
			
			if (op == '1'){
				join(x, y);
			}else{
				
				w = find(x);
				z = find(y);
				k = find(1);
				
				if (k == w){
					if (qtde[w] > qtde[z]){
						vitoria++;
					}
				}else if (k == z){
					if (qtde[z] > qtde[w]){
						vitoria++;
					}
				}
			}
		}
		
		cout << vitoria << endl;
		
		cin >> b >> o;
		if (b == 0)
			break;
			
		for (int i = 1; i <= b; i++){
			peso[i] = 0;
			banco[i] = i;
		}
	}
}