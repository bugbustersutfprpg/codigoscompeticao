#include <iostream>
#include <vector>

using namespace std;

int proximo(int j, int p, int dir);
int maiorCarta(vector<pair<int, char> > p, pair<int, char> topo);
int naipe(char c);

//########################################################################################################

int main(){

	int i, j, p, m, n, indice;
	int dir;
	vector<pair<int, char> > jog[11];
	pair<int, char> baralho[300];
	pair<int, char> topo;
	pair<int, char> carta;

	while(1){
		cin >> p >> m >> n;
		if (p == 0)
			break;

        for (j = 1; j <= p; j++){
            for (i = 0 ;i < m ;i++ ){
                cin >> carta.first >> carta.second;
                jog[j].push_back(carta);
                n--;
            }
        }
		cin >> topo.first >> topo.second;
		n--;
		if (topo.first == 12){
            dir = -1;
		}else{
            dir = 1;
		}

		for (i = 0; i < n; i++){
            cin >> baralho[i].first >> baralho[i].second;
		}

		i = 0;  //indice do baralho
		j = 1;  //indice do jogador
		bool aplicado = false;

		while (1){
            if (aplicado == false){
                switch (topo.first){
                    case 7:
                        jog[j].push_back( baralho[i++] );
                        jog[j].push_back( baralho[i++] );
                        j = proximo(j, p, dir);
                        aplicado = true;
                        continue;
                    case 1:
                        jog[j].push_back( baralho[i++] );
                        j = proximo(j, p, dir);
                        aplicado = true;
                        continue;
                    case 11:
                        j = proximo(j, p, dir);
                        aplicado = true;
                        continue;
                    default:
                        break;
                }
            }

            indice = maiorCarta(jog[j], topo);
            if (indice == -1){
                if (baralho[i].first == topo.first || baralho[i].second == topo.second){
                    topo = baralho[i++];
                    if (topo.first == 12)
                        dir *= -1;
                    aplicado = false;
                }else{
                    jog[j].push_back(baralho[i++]);
                }
            }else{
                topo = jog[j][indice];
                if (topo.first == 12)
                    dir *= -1;
                jog[j].erase(jog[j].begin() + indice);
                if (jog[j].empty()){
                    break;
                }
                aplicado = false;
            }
            j = proximo(j, p, dir);
		}

		cout << j << endl;
		for (i = 1; i <= p; i++)
			jog[i].clear();
	}
}

//########################################################################################################

int proximo (int j, int p, int dir){
	j += dir;
	if (j > p)
		return 1;
	if (j == 0)
		return p;
	return j;
}

int naipe(char c){
	if (c == 'C')
		return 1;
	else if (c == 'D')
		return 2;
	else if (c == 'H')
		return 3;
	return 4;
}

int maiorCarta(vector<pair<int, char> > p, pair<int, char> topo){
	int i, maior = 0, j = -1;

	for (i = 0; i < p.size(); i++){
		if (p[i].second == topo.second){
			if (maior < p[i].first){
				maior = p[i].first;
				j = i;
			}else if (maior == p[i].first){
                if (naipe(p[i].second) > naipe(p[j].second)){
                    j = i;
                }
			}
		}else if (p[i].first == topo.first){
            if (maior < p[i].first){
                maior = p[i].first;
                j = i;
            }else if (maior == p[i].first){
                if (naipe(p[i].second) > naipe(p[j].second)){
                    j = i;
                }
            }
		}
	}
	return j;
}