#include <iostream>
#include <vector>

using namespace std;
#define T 100000
#define t 50000

int main(){

    int i, j, n, m, x, mapa[T], v[t];

    cin>>n;
    for (i = 0; i < n; i++){
        cin>>v[i];
        mapa[v[i]] = i;
    }

    cin>>m;
    for (i = 0; i < m; i++){
        cin>>x;
        v[mapa[x]] = -1;
    }

    i = 0;
    while(v[i] == -1)
        i++;

    cout<<v[i];
    for(i++; i < n; i++){
        if (v[i] != -1)
            cout<<" "<<v[i];
    }
    cout<<"\n";
}