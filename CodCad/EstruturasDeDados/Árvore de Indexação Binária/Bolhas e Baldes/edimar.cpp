#include <iostream>
#define T 100001
using namespace std;

int index[T], mapa[T], n;

void atualiza(int x, int v){ // adicionar v frutas a caixa x

	while(x <= n){ // nosso teto, que e quando vamos parar de rodar o algoritmo
		index[x] += v; // adicionamos v frutas a arvore[x], como devemos
		x += (x & -x);  // atualizamos o valor de x adicionado ele ao seu bit menos significante
	}
}

int soma(int x){

	int s = 0;
	while(x > 0){ // vamos reduzindo x ate acabarmos (quando chegamos a zero)
		s += index[x]; // adicionamos o pedaco de arvore atual a soma
		x -= (x & -x);  // removemos o bit menos significante
	}
	return s;
}

int main(){

    int t, k;

    cin >> n;
    while(n){
        for (int i = 1; i <= n; i++){
            cin >> k;
            mapa[k] = i;
            atualiza(i, 1);
        }

        t = 0;
        for (int i = 1; i <= n; i++){
            t += soma (mapa[i]) - 1;
            atualiza(mapa[i], -1);
        }
        
        if (t%2 == 0)
            cout << "Carlos\n";
        else
            cout << "Marcelo\n";

        cin >> n;
    }
}