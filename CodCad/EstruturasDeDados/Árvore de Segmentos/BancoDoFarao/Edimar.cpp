#include <bits/stdc++.h>

using namespace std;
#define T 270000
#define LAST (T-1)
#define INF (1 << 30)

typedef pair<int, int> ii;

typedef struct node{

    ii esquerda;
    ii direita;
    ii melhor;
    ii total;

}No;

No arv[T];
int v[100100];

No inserirBase(int x){

    No no;
    ii aux = {v[x], 1};
    no.direita = aux;
    no.esquerda = aux;
    no.melhor = aux;
    no.total = aux;

    return no;
}

ii maior(ii a, ii b){

    if (a.first > b.first)
        return a;
    else if (a.first < b.first)
        return b;
    if (a.second > b.second)
        return a;
    return b;
}

No atualizaPai(No esq, No dir){

    if (dir.melhor.first == INF) return esq;
    if (esq.melhor.first == INF) return dir;

    No aux;
    aux.esquerda = maior(esq.esquerda, ii(esq.total.first + dir.esquerda.first, esq.total.second + dir.esquerda.second));

    aux.direita = maior(ii(esq.direita.first + dir.total.first, esq.direita.second + dir.total.second), dir.direita);

    aux.total = ii(esq.total.first + dir.total.first, esq.total.second + dir.total.second);

    aux.melhor = maior(ii(esq.direita.first + dir.esquerda.first, esq.direita.second + dir.esquerda.second),
                       maior(aux.esquerda, maior(aux.direita, maior(esq.melhor, dir.melhor) ) ) );

    return aux;
}

No createArv(int no, int inicio, int fim){

    if (inicio == fim) arv[no] = inserirBase(inicio);
    else{
        int meio = (inicio + fim) / 2;
        arv[no] = atualizaPai(createArv(no*2, inicio, meio), createArv(no*2+1, meio+1, fim));
    }

    return arv[no];
}

No consulta(int no, int i, int j, int a, int b){

    if(a <= i && j <= b) return arv[no];
	if(i > b || a > j) return arv[LAST];

	int meio = (i + j)/2;

	return atualizaPai(consulta(no*2, i, meio, a, b), consulta(no*2+1, meio+1, j, a, b));
}

int main(){

    arv[LAST].melhor.first = INF;
    int n, m, caso, x;

    cin >> caso;

    while(caso--){

        cin >> n;

        for (int i = 1; i <= n; i++){
            cin >> v[i];
        }

        createArv(1, 1, n);

        cin >> m;
        int a,b;
        No res;
        for (int i = 0; i < m; i++){
            cin >> a >> b;
            res = consulta(1, 1, n, a, b);
            cout << res.melhor.first << " " << res.melhor.second << endl;
        }
    }
}
