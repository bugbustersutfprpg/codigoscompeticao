#include <stdio.h>
#include <vector>
#include <algorithm>
using namespace std;

typedef pair<int, int> ii;
typedef struct {
	ii left, right;
	ii total;
	ii best;
	bool invalid;
} node;

int n;
int v[100010];
node tree[400010];

node consulta(int index, int from, int to, int query_from, int query_to);

node build_tree(int index, int from, int to);
node single(int value);
node merge(node value1, node value2);
node invalid();
ii best(ii value1, ii value2);
int get_mid(int from, int to);

int main() {
	int t;
	scanf("%d", &t);
	while(t--) {
		scanf("%d", &n);
		for(int i=0; i<n; i++) {
			scanf("%d", &v[i]);
		}

		build_tree(0, 0, n-1);
		// for(int i=0; i<15; i++) {
		// 	printf("%d\n", i);
		// 	printf("left\t%d,%d\n", tree[i].left.first, tree[i].left.second);
		// 	printf("right\t%d,%d\n", tree[i].right.first, tree[i].right.second);
		// 	printf("total\t%d,%d\n", tree[i].total.first, tree[i].total.second);
		// 	printf("best\t%d,%d\n", tree[i].best.first, tree[i].best.second);
		// 	printf("\n");
		// }

		int q;
		scanf("%d", &q);
		while(q--) {
			int a, b;
			scanf("%d %d", &a, &b);

			node resultado = consulta(0, 0, n-1, a-1, b-1);
			printf("%d %d\n", resultado.best.first, resultado.best.second);
		}
	}
}

node consulta(int index, int from, int to, int query_from, int query_to) {
	if(query_from <= from and to <= query_to) {
		return tree[index];
	}

	if(to < query_from or from > query_to) {
		return invalid();
	}

	int mid = get_mid(from, to);
	return merge(
		consulta(index * 2 + 1, from, mid, query_from, query_to),
		consulta(index * 2 + 2, mid + 1, to, query_from, query_to)
	);
}

node build_tree(int index, int from, int to) {
	if(from == to) {
		return tree[index] = single(v[from]);
	}

	int mid = get_mid(from, to);
	return tree[index] = merge(
		build_tree(index * 2 + 1, from, mid),
		build_tree(index * 2 + 2, mid + 1, to)
	);
}

node single(int value) {
	node result;

	result.left = ii(value, 1);
	result.right = ii(value, 1);
	result.total = ii(value, 1);
	result.best = ii(value, 1);
	result.invalid = false;

	return result;
}

node merge(node value1, node value2) {
	if(value1.invalid) {
		return value2;
	} else if(value2.invalid) {
		return value1;
	}

	node result;
	result.invalid = false;

	result.left = best(
		value1.left, 
		ii(
			value1.total.first + value2.left.first, 
			value1.total.second + value2.left.second
		)
	);
	result.right = best(
		value2.right,
		ii(
			value2.total.first + value1.right.first,
			value2.total.second + value1.right.second
		)
	);
	result.total = ii(
		value1.total.first + value2.total.first,
		value1.total.second + value2.total.second
	);

	result.best = result.total;
	result.best = best(result.best, result.left);
	result.best = best(result.best, result.right);
	result.best = best(result.best, value1.best);
	result.best = best(result.best, value2.best);
	result.best = best(result.best, ii(
		value1.right.first + value2.left.first,
		value1.right.second + value2.left.second
	));

	return result;
}

node invalid() {
	node result;

	result.left = ii(0, 0);
	result.right = ii(0, 0);
	result.total = ii(0, 0);
	result.best = ii(0, 0);
	result.invalid = true;

	return result;
}

ii best(ii value1, ii value2) {
	if(value1.first == value2.first) {
		if(value1.second > value2.second) {
			return value1;
		} else {
			return value2;
		}
	} else if(value1.first > value2.first) {
		return value1;
	} else {
		return value2;
	}
}

int get_mid(int from, int to) {
	return from + (to - from) / 2;
}