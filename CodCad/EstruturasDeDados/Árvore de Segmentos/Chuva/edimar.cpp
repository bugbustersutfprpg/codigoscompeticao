#include <iostream>
#include <stack>

using namespace std;

int main(){

    int height, total, n;
    stack <int> pilha;
    total = 0;

    cin >> n;
    cin >> height;
    pilha.push(height);

    for (int i = 1; i < n; i++){
        cin >> height;
        if (height > pilha.top()){
            int q = 0;
            while(height > pilha.top()){
                pilha.pop();
                if (pilha.empty()){
                    break;
                }
                q++;
                if (pilha.size() == 1){
                    if (pilha.top() <= height){
                        pilha.pop();
                    }
                    break;
                }
            }
            total += q;
        }else if (height == pilha.top() && pilha.size() == 1){
            continue;
        }
        pilha.push(height);
    }

    cout << total << endl;
}