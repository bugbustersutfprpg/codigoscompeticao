#include <iostream>
#include <stdio.h>

using namespace std;

char arvore[270000];
int fim;

int acharInicio(int x){
    int i;
    for (i = 1 ;i < x ;i <<= 1 ){}
    return i;
}

char cria(int no, int i, int j){

    if (i >= fim){
        return ' ';
    }
	if (i == j){
        return arvore[no];
	}

    int meio = (i+j)/2;
    int esq = no*2;
    int dir = no*2+1;

    char c = cria(esq, i, meio);
    char d = cria(dir, meio+1, j);

    if (d == ' '){
        arvore[no] = c;
    }else if (c == '0' || d == '0'){
        arvore[no] = '0';
    }else if (c == d){
        arvore[no] = '+';
    }else{
        arvore[no] = '-';
    }
    return arvore[no];
}

void atualiza(int no, int i, int j, int posicao){

	if (i == j)
        return;

    int meio = (i+j)/2;
    int esq = no*2;
    int dir = no*2+1;

    if (posicao <= meio) atualiza(esq, i, meio, posicao);
    else atualiza(dir, meio+1, j, posicao);

    if (arvore[dir] == ' '){
        arvore[no] = arvore[esq];
    }else if (arvore[esq] == '0' || arvore[dir] == '0'){
        arvore[no] = '0';
    }else if (arvore[esq] == arvore[dir]){
        arvore[no] = '+';
    }else{
        arvore[no] = '-';
    }
}

char consulta(int no, int i, int j, int a, int b){

   //totalmente dentro
    if (i >= a && j <= b){
        return arvore[no];
    }
    //totalmente fora
    if (j < a || i > b) {
        return ' ';
    }

    int meio = (i+j)/2;
    int esq = no*2;
    int dir = no*2+1;

    char c = consulta(esq, i, meio, a, b);
    char d = consulta(dir, meio+1, j, a, b);

    if (c == ' '){
        return d;
    }
    if (d == ' ')
        return c;
    if (c == '0' || d == '0')
        return '0';
    if (c == d){
        return '+';
    }
    return '-';
}

int main(){

    int v, k, x, y, inicio, initial;
    char c, r;

    while(scanf("%d %d", &v, &k) != -1){

        initial = acharInicio(v);
        inicio = initial;

        for (int i = 1; i <= v; i++){
            cin >> x;
            if (x == 0){
                arvore[initial++] = '0';
            }else if (x < 0){
                arvore[initial++] = '-';
            }else{
                arvore[initial++] = '+';
            }
        }
        fim = initial;
        initial = inicio*2-1;

        cria(1, inicio, initial);
        for (int i = 1; i <= k; i++){
            cin >> c >> x >> y;

            if (c == 'C'){
                c = arvore[x+inicio-1];
                if (y < 0)
                    r = '-';
                else if (y > 0)
                    r = '+';
                else
                    r = '0';

                if (c != r){
                    arvore[x+inicio-1] = r;
                    atualiza(1, inicio, initial, x+inicio-1);
                }
            }else{
                cout << consulta(1, inicio, initial, x+inicio-1, y+inicio-1);
            }
        }
        cout << endl;
    }
}