#include <bits/stdc++.h>

using namespace std;
#define T 501

vector<int> v[T];
int idade[T], mapa[T], node[T], menor;
bool visit[T];

void consulta(int x){

    if (visit[x]) return;
    //printf("Olhando %d\n", x);

    visit[x] = 1;
    for (int i = 0; i < v[x].size(); i++){
        if (idade[node[v[x][i]]] < menor) {
            menor = idade[node[v[x][i]]];
            //printf("Atualizou menor para %d no no %d que representa %d\n", menor, x, node[v[x][i]]);
        }
        consulta(v[x][i]);
    }
}

int main(){

    int n, m, I, a, b;

    cin >> n >> m >> I;

    for (int i = 1; i <= n; i++){
        cin >> idade[i];
    }

    for (int i = 0; i < m; i++){
        cin >> a >> b;
        v[b].push_back(a);
    }
    for (int i = 1; i <= n; i++){
        mapa[i] = i;
        node[i] = i;
    }

    char c;
    for (int i = 0; i < I; i++){
        cin >> c >> a;
        if (c == 'T'){
            cin >> b;
            swap(mapa[a], mapa[b]);
            swap(node[mapa[a]], node[mapa[b]]);
        }else{
            menor = 3000;
            b = mapa[a];
            if (v[b].empty()) cout << "*\n";
            else {
                consulta(b);
                cout << menor << endl;
                memset(visit, 0, sizeof(visit));
            }
        }
    }
}
