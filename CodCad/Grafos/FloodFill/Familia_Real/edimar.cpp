#include <bits/stdc++.h>

using namespace std;
#define T 10010

int festa[T], altura[T], maior;
vector<int> v[T];
int qtd[T], qtdFesta[T];

void contar(int x, int alt){

    altura[x] = alt;
    if (alt > maior) maior = alt;
    for (int i = 0; i < v[x].size(); i++){
        contar(v[x][i], alt+1);
    }
}

int main(){

    int n, m, x;

    cin >> n >> m;

    for (int i=1; i <=n; i++){
        cin >> x;
        v[x].push_back(i);
    }
    for (int i=1; i <=m; i++){
        cin >> x;
        festa[x] = 1;
    }

    contar(0, 0);

    for (int i = 1; i <= n; i++){
        qtd[altura[i]]++;
    }
    for (int i=1; i <= n; i++){
        if (festa[i]){
            qtdFesta[altura[i]]++;
        }
    }
    for (int i=1; i< maior; i++){
        //printf("%d %d\n", qtdFesta[i], qtd[i]);
        printf("%.2lf ", ((double)qtdFesta[i] / qtd[i])*100 );
    }
    printf("%.2lf\n", ((double)qtdFesta[maior]/qtd[maior])*100);
}