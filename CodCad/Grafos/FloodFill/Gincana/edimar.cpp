#include <bits/stdc++.h>

using namespace std;
#define T 1001

vector<int> v[T];
bool agrupado[T];

void dfs(int x){

    agrupado[x] = true;
    for (int i = 0; i < v[x].size(); i++){
        int y = v[x][i];
        if (agrupado[y] == false){
            dfs(y);
        }
    }
}

int main(){

    int n, m, x, y;

    cin >> n >> m;

    for (int i = 0; i < m; i++){
        cin >> x >> y;
        v[x].push_back(y);
        v[y].push_back(x);
    }

    int cont = 0;
    for (int i = 1; i <= n; i++){
        if (agrupado[i] == false){
            dfs(i);
            cont++;
        }
    }

    cout << cont << endl;
}
