#include <iostream>
#include <vector>

using namespace std;
#define T 1000001

vector<int> v[T];

int buscar(int i);


//###############################################################################################


int main(){

	int i, j, k, n;

	cin>>n;

	k = 0;
	while(k++ < n){
		cin>>i>>j;
		v[j].push_back(i);
	}

	i = buscar(0);

	if (i > 0)
		cout<<"bem\n";
	else
		cout<<"mal\n";
}


//###############################################################################################


int buscar(int i){
	if (v[i].empty())
		return 1;

	int k, filho;
	filho = T;
	vector<int>::iterator it;
	for (it = v[i].begin(); it != v[i].end(); it++) {
		k = buscar(*it);
		if (k > 0){
			if (filho == T)
				filho = k;
			else{
				if (k != filho){
					return 0;
				}
			}
		}else{
			return 0;
		}
	}
	return k*v[i].size()+1;
}