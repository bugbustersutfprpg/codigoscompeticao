#include <bits/stdc++.h>
using namespace std;
#define T 1001
int n, m, matriz[T][T]; //global para pegar valor posteriormente

bool dfs(int x, int y, int cont){
	if (matriz[x][y] == 3) {
		cout << cont << endl; //imprime a resposta
		return true; //pois é o valor da casa 3
	}
	
	matriz [x][y] = 0;//evita que se perca durante o caminho, para não ficar subindo e descendo toda hora

	if (x-1 >=0 && matriz[x-1][y]){//verifica os limites da matriz 
		if (dfs (x-1, y, cont+1) == -1)
			return true;//retorna -1 para encerrar a busca ou a procura
	}

	if (y+1 < m && matriz[x][y+1]){
		if (dfs (x, y+1, cont+1))
			return true;//retorna verdadeiro para continuar
	}

	if (x+1 < n && matriz[x+1][y])
		if (dfs (x+1, y, cont+1))
			return true;

	if (y-1 >= 0 && matriz[x][y-1])
		if (dfs (x, y-1, cont+1))
			return true;

	return false;//para encerrar o programa
}


int main (){
	int x, y; //salvar os valores local para se atualizar a cada iteração
	cin >> n >> m; //entrada
	for (int i=0; i<n; i++){ //leitura dentro do for
		for (int j=0; j<m; j++){
			cin >> matriz[i][j];
			if (matriz[i][j]==2){
				x=i;
				y=j;
			}

		}
	}

	dfs (x, y, 1); //começo minha busca aqui (2)

}