#include <bits/stdc++.h>

using namespace std;
#define T 1001

int n, m, matriz[T][T];
list<pair<int, pair<int, int> > > lista;

void bfs(int x, int y, int cont){

    if (matriz[x][y] == 3){
        cout << cont << endl;
        return;
    }
    matriz[x][y] = 0;

    if (x-1 >= 0 && matriz[x-1][y])
        lista.push_back(make_pair(cont+1, make_pair(x-1, y)));

    if (y+1 < m && matriz[x][y+1])
        lista.push_back(make_pair(cont+1, make_pair(x, y+1)));

    if (x+1 < n && matriz[x+1][y])
        lista.push_back(make_pair(cont+1, make_pair(x+1, y)));

    if (y-1 >= 0 && matriz[x][y-1])
        lista.push_back(make_pair(cont+1, make_pair(x, y-1)));

    pair<int, pair<int, int> > p = lista.front();
    lista.pop_front();

    bfs(p.second.first, p.second.second, p.first);
}

int main(){

    int x, y;

    cin >> n >> m;

    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
            cin >> matriz[i][j];
            if (matriz[i][j] == 2){
                x = i;
                y = j;
            }
        }
    }

    bfs(x, y, 1);
}
