#include <bits/stdc++.h>

using namespace std;
#define T 251
const int INF = (1 << 29);

int v[T][T];
int n, m, a, b, c;

void floyd(){
    for (int k=1;k<=n;k++)
        for(int i=1;i<=n;i++)
            for(int j=1;j<=n;j++)
                v[i][j] = v[j][i] = min(v[i][j], v[i][k]+v[k][j]);
}

int main(){

    cin >> n>>m;

    for (int i=1; i<=n;i++)
        for(int j=1; j<=n;j++)
            if (i==j) v[i][j] = 0;
            else v[i][j] = v[j][i] = INF;

    for (int i=0; i < m; i++){
        cin >>a>>b>>c;
        v[a][b] = v[b][a] = c;
    }

    floyd();

    int maior, menor = INF;
    for (int i=1; i<=n;i++){
        maior = 0;
        for (int j=1; j<=n;j++){
            maior = max(maior, v[i][j]);
        }
        menor = min(menor, maior);
    }

    cout << menor << endl;
}
