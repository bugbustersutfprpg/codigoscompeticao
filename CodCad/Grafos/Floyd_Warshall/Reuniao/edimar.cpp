#include <bits/stdc++.h>

using namespace std;
#define T 101
#define INF (1 << 29)

int v[T][T];
int n, m,a,b,c;

void floyd(){

    for (int i=0; i <n; i++)
        for (int j=0; j<n; j++)
            for (int k=0; k<n; k++)
                v[k][j] = v[j][k] = min(v[k][j], v[k][i] + v[i][j]);
}

int main(){

    cin >>n>>m;

    for (int i=0; i <n;i++)
        for (int j=0; j<n; j++)
            if (i==j) v[i][j]=0;
            else v[i][j] = v[j][i] = INF;

    for (int i=0; i <m; i++){
        cin >>a>>b>>c;
        v[a][b] = v[b][a] = min(v[a][b], c);
    }

    floyd();

    int maior, menor = INF;
    for (int i = 0; i<n; i++){
        maior = 0;
        for (int j=0; j<n; j++){
            maior = max(maior, v[i][j]);
        }
        menor = min(menor, maior);
    }
    cout << menor << endl;
}
