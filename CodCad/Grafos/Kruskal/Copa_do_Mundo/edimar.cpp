#include <bits/stdc++.h>

using namespace std;
#define T 101
typedef pair<int,int> ii;
typedef pair<int, ii> iii;

iii x;
priority_queue<iii, vector<iii>, greater<iii> > f, r;
int n, fe, ro, ans = 0, cont, pai[T], peso[T];

int find(int x){
    if(pai[x] == x) return x;
    return pai[x] = find(pai[x]);
}

void join(int a, int b){

    a = find(a);
    b = find(b);

    if(peso[a] < peso[b]) pai[a] = b;
    else if(peso[b] < peso[a]) pai[b] = a;
    else{
        pai[a] = b;
        peso[b]++;
    }
}

void kruskal(){

    n--;
    while(!f.empty()){
        x = f.top();
        f.pop();

        if (find(x.second.first) != find(x.second.second)){
            join(x.second.first, x.second.second);
            ans += x.first;
            cont++;
        }
    }

    while(!r.empty() && cont != n){
        x = r.top();
        r.pop();

        if (find(x.second.first) != find(x.second.second)){
            join(x.second.first, x.second.second);
            ans += x.first;
            cont++;
        }
    }
}

int main(){

    cin >>n>>fe>>ro;

    for (int i=0; i < fe; i++){
        cin >> x.second.first >> x.second.second >> x.first;
        f.push(x);
    }
    for (int i=0; i < ro; i++){
        cin >> x.second.first >> x.second.second >> x.first;
        r.push(x);
    }
    for (int i=1; i <= n; i++)
        pai[i] = i;

    kruskal();

    cout << ans << endl;
}
