#include <bits/stdc++.h>

using namespace std;
#define T 100001
#define MAXLOG 17
typedef pair<int,long long> il;

il ancestral[T][MAXLOG];
int nivel[T], n, L;

long long LCA(int u, int v){

	if(nivel[u] < nivel[v]) swap(u, v);

	long long soma = 0;
	for(int i = L-1; i >= 0; i--){
		if(nivel[u] - (1 << i) >= nivel[v]){
			soma += ancestral[u][i].second;
			u = ancestral[u][i].first;
		}
    }
	if (u == v) return soma;

	int i;
	for(i = L-1; i >= 0; i--){
		if(ancestral[u][i].first != -1 && ancestral[u][i].first != ancestral[v][i].first){
			soma += ancestral[u][i].second + ancestral[v][i].second;
			u = ancestral[u][i].first;
			v = ancestral[v][i].first;
		}
	}
	soma += ancestral[u][0].second + ancestral[v][0].second;

	return soma;
}

void construirTabela(){

    il x = il(-1,0);
    long long peso;
    for(int j = 1; j < L; j++){
        for(int i = 1; i < n; i++){
            if (ancestral[i][j-1].first != -1){
                peso = ancestral[i][j-1].second + ancestral[ ancestral[i][j-1].first ][j-1].second;
                ancestral[i][j] = il(ancestral[ ancestral[i][j-1].first ][j-1].first, peso);
            }else ancestral[i][j] = x;
        }
    }
}

int main(){

    int a, b, q;
    long long dist;
    scanf("%d", &n);

    nivel[0] = 0;
    il x = il(-1, 0);
    for (int i = 0; i < MAXLOG; i++)
        ancestral[0][i] = x;

    while(n){
        for (int i = 1; i < n; i++){
            scanf("%d %lld", &a, &dist);
            ancestral[i][0] = il(a, dist);
            nivel[i] = nivel[a]+1;
        }

        L = ceil(log2(n));
        construirTabela();

        scanf("%d", &q);
        for (int i = 1; i < q; i++){
            scanf("%d %d", &a, &b);
            printf("%lld ", LCA(a, b));
        }
        scanf("%d %d", &a, &b);
        printf("%lld\n", LCA(a, b));

        scanf("%d", &n);
    }
}

