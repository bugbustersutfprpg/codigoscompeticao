#include <bits/stdc++.h>

using namespace std;
#define T 10010
const int INF = (1 << 20);
typedef pair<int, int> ii;
vector<ii> v[T];
int dis[T];
bool visit[T];
int n, m, a, b, c;
priority_queue<ii, vector<ii>, greater<ii> > fila;

void caminho(int x){

    if (x == n+1){
        cout << dis[x] << endl;
        exit(0);
    }
    if (visit[x]) return;

    visit[x] = 1;
    ii u;
    for (int i = 0; i < v[x].size(); i++){
        u = v[x][i];
        if (dis[u.second] > dis[x] + u.first){
            dis[u.second] = u.first = dis[x] + u.first;
            fila.push(u);
        }
    }
    while(!fila.empty()){
        x = fila.top().second;
        fila.pop();
        caminho(x);
    }
}

int main(){

    cin >> n >> m;

    for (int i = 0; i < m; i++){
        cin >> a >> b >> c;
        v[a].push_back(make_pair(c, b));
        v[b].push_back(make_pair(c, a));
    }

    for (int i = 1;i <= n+1; i++)
        dis[i] = INF;

    caminho(0);
}