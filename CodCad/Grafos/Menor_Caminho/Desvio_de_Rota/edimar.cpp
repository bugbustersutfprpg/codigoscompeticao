#include <bits/stdc++.h>

using namespace std;
#define T 251
#define INF (1 << 30);
typedef pair<int, int> ii;

vector<ii> v[T];
priority_queue<ii, vector<ii>, greater<ii> > fila;
int dis[T], rota[T];
int n, m, c, k, a, b, d;
bool visit[T];

void djk(int x){

    if (visit[x]) return;
    visit[x] = 1;

    ii u;
    for (int i = 0; i < v[x].size(); i++){
        u = v[x][i];
        if (u.second < c){
            if (u.second == c-1){
                if (dis[c-1] > dis[x] + u.first){
                    dis[c-1] = dis[x] + u.first;
                    fila.push(ii(dis[c-1], c-1));
                }
            }
            else if (dis[c-1] > dis[u.second] + dis[x] + u.first){
                dis[c-1] = dis[u.second] + dis[x] + u.first;
                fila.push(ii(dis[c-1], c-1));
            }
        }
        else if (dis[u.second] > dis[x]+u.first){
            dis[u.second] = u.first = dis[x]+u.first;
            fila.push(u);
        }
    }

    while(!fila.empty()){
        x = fila.top().second;
        fila.pop();
        djk(x);
    }
}

void calculaRota(){

    dis[c-2] = rota[c-2];
    for (int i=c-3; i >= 0; i--){
        dis[i] = rota[i] + dis[i+1];
    }
}

int main(){

    while(1){
        cin >>n>>m>>c>>k;

        if (n==0) break;
        v[0].clear();
        for (int i = 1; i <=n; i++) {
            dis[i] = INF;
            visit[i] = 0;
            v[i].clear();
        }
        while(!fila.empty()) fila.pop();

        for (int i=0; i<m; i++){
            cin >>a>>b>>d;
            v[a].push_back(ii(d, b));
            v[b].push_back(ii(d, a));
            if (a+1 == b) rota[a] = d;
            if (a-1 == b) rota[b] = d;
        }
        calculaRota();
        dis[k] = 0;
        djk(k);
        cout << dis[c-1] << endl;
    }
}
