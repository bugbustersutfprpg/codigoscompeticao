#include <bits/stdc++.h>

using namespace std;
#define T 100
#define x first
#define y second
#define INF 2e9
#define p pair<int, int>
#define pa pair<int, p>

int n, N;
int dist[T][T];
bool mat[T][T], visit[T][T];


void djikstra(){

    priority_queue <pa, vector<pa>, greater<pa> > fila;

    pair<int, int> par = make_pair(0, 0);
    fila.push(make_pair(0, par));
    dist[0][0] = 0;

    n = N-1;
    while(!fila.empty()){

        do {
            par = fila.top().second;
            fila.pop();
        } while(visit[par.x][par.y]);

        int x = par.x;
        int y = par.y;
        visit[x][y] = true;

        if (x == n and y == n) break;

        int d = dist[x][y] + mat[x][y+1];
        if (y < n and dist[x][y+1] > d){
            par = make_pair(x, y+1);
            fila.push(make_pair(d, par));
            dist[x][y+1] = d;
        }

        d = dist[x][y] + mat[x+1][y];
        if (x < n and dist[x+1][y] > d){
            par = make_pair(x+1, y);
            fila.push(make_pair(d, par));
            dist[x+1][y] = d;
        }

        d = dist[x][y] + mat[x][y-1];
        if (y > 0 and dist[x][y-1] > d){
            par = make_pair(x, y-1);
            fila.push(make_pair(d, par));
            dist[x][y-1] = d;
        }

        d = dist[x][y] + mat[x-1][y];
        if (x > 0 and dist[x-1][y] > d){
            par = make_pair(x-1, y);
            fila.push(make_pair(d, par));
            dist[x-1][y] = d;
        }
    }

}

int main(){

    cin >> N;

    for (int i = 0; i < N; i++){
        for (int j = 0; j < N; j++){
            cin >> mat[i][j];
            dist[i][j] = INF;
        }
    }

    djikstra();

    cout << dist[n][n] << endl;
}