#include <bits/stdc++.h>

using namespace std;
#define T 50010
vector<int> v[T];
int ans[T], dep[T], cont;
bool visit[T];
priority_queue<int, vector<int>, greater<int> > lista;

void escalonar(int x){

    lista.pop();

    ans[cont++] = x;
    for (int i = 0; i < v[x].size(); i++){
        int u = v[x][i];
        dep[u]--;
        if (dep[u] == 0)
            lista.push(u);
    }
    if (!lista.empty())
        escalonar(lista.top());
}

int main(){

    int n, m, a, b;

    cin >> n >> m;

    for (int i = 0; i < m; i++){
        cin >> a >> b;
        v[a].push_back(b);
        dep[b]++;
    }

    for (int i = 0; i < n; i++){
        if (dep[i] == 0){
            lista.push(i);
        }
    }

    if (!lista.empty())
        escalonar(lista.top());

    if (cont == n){
        for (int i = 0; i < n; i++){
            cout << ans[i] << endl;
        }
    }else cout << "*\n";
}