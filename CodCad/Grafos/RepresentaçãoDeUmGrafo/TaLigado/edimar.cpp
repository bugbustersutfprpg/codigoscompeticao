#include <iostream>
#include <vector>

using namespace std;
#define T 100001

int main(){

    int n, m, a, b, i, j;
    bool bo;
    vector<int> grafo[T];
    vector<int>::iterator it;

    cin>>n>>m;

    while(m--){
        cin>>bo>>a>>b;
        if (bo){
            grafo[a].push_back(b);
            grafo[b].push_back(a);
        }else{
            if (grafo[a].empty()){
                cout<<"0\n";
            }else{
                if (grafo[a].size() > grafo[b].size()){
                    it = grafo[b].begin();
                    i = b;
                    j = a;
                }else{
                    it = grafo[a].begin();
                    i = a;
                    j = b;
                }
                for(a = a ; it != grafo[i].end(); it++){
                    if (*it == j)
                        break;
                }
                if (it != grafo[i].end()){
                    cout<<"1\n";
                }else{
                    cout<<"0\n";
                }
            }
        }
    }
}