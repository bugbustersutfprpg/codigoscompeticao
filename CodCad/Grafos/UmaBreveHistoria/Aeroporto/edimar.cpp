#include <iostream>
#include <stdio.h>

using namespace std;
#define T 101

int main(){

    int a, v, i, j, o, d, aeroporto[T], maior;

    j = 1;
    while(1){
        scanf("%d %d", &a, &v);
        if (!a && !v) break;

        for (i = 0; i <= a; i++)
            aeroporto[i] = 0;

        maior = 0;
        while(v--){
            cin>>o>>d;
            if (++aeroporto[o] > maior)
                maior++;
            if (++aeroporto[d] > maior)
                maior++;
        }

        cout<<"Teste "<<j++<<endl;
        for (i = 0; i <= a; i++){
            if (aeroporto[i] == maior){
                cout<<i<<" ";
            }
        }
        puts("\n");
    }
}