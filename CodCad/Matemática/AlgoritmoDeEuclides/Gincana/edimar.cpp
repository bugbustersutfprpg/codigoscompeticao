#include <iostream>

using namespace std;
#define T 2000000000

long long int mdc(long long int a, long long int b){

    if (b == 0)
        return a;

    if (a > T){
        return mdc(b, (a - (int)(a / b) * b));
    }else{
        return mdc(b, a%b);
    }
}

int main(){

    long long int n, m, i;

    cin >> n >> m;

    for (i = m; mdc(n, i) != 1; i--){}

    cout << i << endl;
}