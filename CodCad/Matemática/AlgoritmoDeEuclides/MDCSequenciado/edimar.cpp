#include <iostream>

using namespace std;

int mdc(int a, int b){
    if (b == 0)
        return a;

    return mdc(b, a%b);
}

int main(){

    int n, a, b;

    cin >> n >> a;
    n--;

    while(n--){
        cin >> b;
        a = mdc(a, b);
    }

    cout << a << endl;
}