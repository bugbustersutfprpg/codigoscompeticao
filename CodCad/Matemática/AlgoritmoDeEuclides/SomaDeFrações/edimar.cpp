#include <iostream>

using namespace std;

int mdc(long long int a, long long int b){

    if (b == 0)
        return a;
    return mdc(b, a%b);
}

int main(){

    long long int a, b, c, d, m;

    cin >> a >> b >> c >> d;

    a = a * d + b * c;
    b = b * d;

    m = mdc(a,b);
    a /= m;
    b /= m;

    cout << a << " " << b << endl;
}