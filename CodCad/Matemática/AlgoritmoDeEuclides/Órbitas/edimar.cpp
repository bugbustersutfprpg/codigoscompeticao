#include <iostream>

using namespace std;
#define T 2000000000

long long int mdc(long long int a, long long int b){

    if (b == 0)
        return a;

    if (a > T){
        return mdc(b, (a - (int)(a / b) * b));
    }else{
        return mdc(b, a%b);
    }
}

long long int mmc(long long int a, long long int b){
    long long int r;
    r = mdc(a, b);
    a /= r;
    b /= r;

    return a * b * r;
}

int main(){

    long long int a, b, aux;

    cin >> a >> b;

    if (a < b){
        aux = a;
        a = b;
        b = aux;
    }

    aux = mmc(a, b);
    cout << aux << endl;
}