#include <iostream>

using namespace std;
#define T 1000001
#define TT 12000000

bool composto[TT];
int primos[T];

void crivo(int n){

    int k = 2;
    primos[1] = 2;
    for (int i = 3; k <= n; i += 2){
        if (!composto[i]){
            primos[k++] = i;
            for (int j = i * 2; j <= TT; j += i){
                composto[j] = 1;
            }
        }
    }
}

int main(){

    int n;

    cin >> n;
    crivo(n);

    cout << primos[n] << endl;
}