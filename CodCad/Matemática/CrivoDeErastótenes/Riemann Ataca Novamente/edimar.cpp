#include <iostream>

using namespace std;
#define T 13000001

bool composto[T];
int mapa[T];

void crivo(){
    int k = 2;
    mapa[2] = 1;
    for (int i = 3; i <= T; i += 2){
        if (!composto[i]){
            mapa[i] = k++;
            for (int j = i * 2; j <= T; j += i){
                composto[j] = 1;
            }
        }
    }
}

int main(){

    int q, n;

    cin >> q;
    crivo();

    while(q--){
        cin >> n;
        cout << mapa[n] << endl;
    }
}