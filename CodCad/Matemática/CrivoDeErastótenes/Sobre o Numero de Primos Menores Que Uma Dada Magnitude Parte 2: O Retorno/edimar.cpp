#include <iostream>

using namespace std;
#define T 10000001

bool composto[T];

void crivo(int n){

    int m = n / 2;
    for (int i = 3; i <= m; i += 2){
        if (!composto[i]){
            for (int j = i * 2; j <= n; j += i){
                composto[j] = 1;
            }
        }
    }

}

int main(){

    int n;

    cin >> n;
    crivo(n);

    cout << "2";
    for (int i = 3; i <= n; i += 2){
        if (!composto[i]){
            cout << " " << i;
        }
    }

    cout << endl;
}