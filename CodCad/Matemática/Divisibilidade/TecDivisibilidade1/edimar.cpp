#include <iostream>

using namespace std;

int main(){
	
	int i, j, t;
	string n;
	
	cin>>n;
	i = n.length() - 1;
	j = n[i] - 48;
	
	for(t = 0 ; i >= 0; i--){
		t += n[i] - 48;
	}
	
	if (j%2 == 0)
		cout<<"S\n";
	else
		cout<<"N\n";
		
	if (t%3 == 0)
		cout<<"S\n";
	else
		cout<<"N\n";
		
	if (j == 0 || j == 5)
		cout<<"S\n";
	else
		cout<<"N\n";
	
}