#include <iostream>

using namespace std;

int main(){
	
	int i, j, t;
	string n;
	
	cin>>n;
	i = n.length() - 1;
	j = n[i] - 48;
	if (i > 0)
		j += 10 * (n[i-1] - 48);
	
	for(t = 0 ; i >= 0; i--){
		t += n[i] - 48;
	}
	
	if (j%4 == 0)
		cout<<"S\n";
	else
		cout<<"N\n";
		
	if (t%9 == 0)
		cout<<"S\n";
	else
		cout<<"N\n";
		
	if (j == 0 || j == 25 || j == 50 || j == 75)
		cout<<"S\n";
	else
		cout<<"N\n";
	
}