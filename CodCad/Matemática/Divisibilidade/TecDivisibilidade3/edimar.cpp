#include <iostream>

using namespace std;

int main(){
	
	int i, j, t;
	string n;
	
	cin>>n;
	i = n.length();
	
	t = n[0] - 48;
	for(j = 1 ; j < i; j++){
		if (j%2 == 0)
			t += n[j] - 48;
		else
			t -= n[j] - 48;
	}
	
	if (t%11 == 0)
		cout<<"S\n";
	else
		cout<<"N\n";
	
}