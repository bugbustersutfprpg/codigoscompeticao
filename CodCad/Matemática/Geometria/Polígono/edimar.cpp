#include <iostream>
#include <algorithm>

using namespace std;
#define T 100000

int v[T];

int main(){

    int n, total;

    cin >> n;

    total = 0;
    for (int i = 0; i < n; i++){
        cin >> v[i];
        total += v[i];
    }

    sort(v, v + n);

    if ((total - v[n-1]) <= v[n-1]){
        n--;
        total -= v[n--];
        while( (total - v[n]) <= v[n] && n > 1){
            total -= v[n--];
        }
        if (n < 2){
            cout << "0\n";
        }else
            cout << n+1 << endl;
    }else{
        cout << n << endl;
    }
}