#include <iostream>

using namespace std;

int main(){

    int aux, a, b, c, d, a2, b2, c2, d2;

    cin >> a >> b >> c >> d;
    cin >> a2 >> b2 >> c2 >> d2;

    if (a > c){
        aux = a;
        a = c;
        c = aux;
    }
    if (b > d){
        aux = b;
        b = d;
        d = aux;
    }
    if (a2 > c2){
        aux = a2;
        a2 = c2;
        c2 = aux;
    }
    if (b2 > d2){
        aux = b2;
        b2 = d2;
        d2 = aux;
    }

    if (c < a2 || a > c2){
        cout << "0\n";
    }else{
        if (b > d2 || b2 > d){
            cout << "0\n";
        }else{
            cout << "1\n";
        }
    }

}