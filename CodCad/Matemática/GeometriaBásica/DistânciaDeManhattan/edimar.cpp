#include <iostream>

using namespace std;

int main(){

    int aux, d, x, y, x1, y1;

    cin >> x >> y >> x1 >> y1;
    
    if (x > x1){
        aux = x;
        x = x1;
        x1 = aux;
    }
    if (y > y1){
        aux = y;
        y = y1;
        y1 = aux;
    }

    d = x1 - x + y1 - y;

    cout << d << endl;
}