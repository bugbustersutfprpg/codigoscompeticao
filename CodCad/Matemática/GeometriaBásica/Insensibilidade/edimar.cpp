#include <iostream>

using namespace std;

int main(){

    int n, x, x1, y, y1, d;

    cin >> n;

    d = 0;
    while(n--){
        cin >> x >> y >> x1 >> y1;
        d += (x-x1)*(x-x1) + (y-y1)*(y-y1);
    }

    cout << d << endl;
}