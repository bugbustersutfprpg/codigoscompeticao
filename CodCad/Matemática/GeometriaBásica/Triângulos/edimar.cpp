#include <iostream>

using namespace std;

int main(){

    int aux, a, b, c;

    cin >> a >> b >> c;

    if (a > c){
        aux = a;
        a = c;
        c = aux;
    }
    if (b > c){
        aux = b;
        b = c;
        c = aux;
    }else if (a > b){
        aux = a;
        a = b;
        b = aux;
    }

    if ((a+b) <= c){
        cout << "n\n";
    }else{
        if ( (a*a + b*b) == (c*c)){
            cout << "r\n";
        }else{
            if ((a*a + b*b) < (c*c)){
                cout << "o\n";
            }else{
                cout << "a\n";
            }
        }
    }

}