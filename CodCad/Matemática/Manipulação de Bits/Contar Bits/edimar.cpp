#include <iostream>

using namespace std;

int qtd(long long int n){

    int r = 0;
    while(n != 0){
        r++;
        n -= n&-n;
    }
    return r;
}

int main(){

    int n;

    cin >> n;

    cout << qtd(n) << endl;

}