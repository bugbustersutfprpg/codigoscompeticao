#include <iostream>

using namespace std;

bool like(long long int x){
    if (x == 0) return 0;
    return ((x&(x + 1)) == 0);
}

int main(){

    long long int n;

    cin >> n;

    if (like(n)){
        cout << "S\n";
    }else{
        cout << "N\n";
    }
}