#include <iostream>

using namespace std;
#define T 100001

int main(){

    int n, maior, i;

    cin >> n;

    cin >> maior;
    while(--n){
        cin >> i;
        maior |= i;
    }

    cout << maior << endl;
}