#include <iostream>
#include <cmath>

using namespace std;
#define T 2000000000

int main(){

	int i, a, b, c, d, m, oter, menor = T;
	bool k = false;

	cin>>a>>b>>c>>d;

    if (c%a == 0){
        m = sqrt(c);

        for(i = 1; i <= m; i++){
            if (c%i == 0){
                oter = c/i;
                if (i%a == 0){
                    if (b > i || i%b != 0){
                        if (d < i || d%i != 0){
                            if (i < menor){
                                menor = i;
                            }
                        }
                    }
                }
                if (oter%a == 0){
                    if (b > oter || oter%b != 0){
                        if (d < oter || d%oter != 0){
                            if (oter < menor){
                                menor = oter;
                            }
                        }
                    }
                }
            }
        }
    }
    if (menor == T)
        menor = -1;

    cout<<menor<<endl;
}