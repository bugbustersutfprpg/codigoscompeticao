#include <cmath>
#include <iostream>

using namespace std;

int primos[10000];

void eh_primo(int x){

	if (x > 1)
        cout << "2";

    bool primo;
    int k = 0;
	for (int i = 3; i <= x; i += 2){
	    primo = true;
	    for (int j = 0; j < k; j++){
            if (i % primos[j] == 0){
                primo = false;
                break;
            }
	    }
	    if (primo){
            cout << " " << i;
            primos[k++] = i;
	    }
	}

	cout << endl;
}

int main(){
	int x;

	cin >> x;

	eh_primo(x);
}