#include <cmath>
#include <iostream>

using namespace std;

bool eh_primo(long long int x){

	if (x == 2)
	    return true;
	if (x%2 == 0 || x == 1)
		return false;

	int a = (int)sqrt(x);
	for (int i = 3; i <= a; i+=2){
	    if (x%i == 0)
	        return false;
	}
	return true;
}

int main(){
	long long int x;

	cin >> x;

	if (eh_primo(x)){
		cout << "S\n";
	}else{
		cout << "N\n";
	}
}