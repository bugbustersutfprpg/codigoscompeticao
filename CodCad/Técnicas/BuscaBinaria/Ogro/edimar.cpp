#include <iostream>

using namespace std;

#define T 100000
#define Tm 1000000000

struct Ponto{
    int posicao;
    int ponto;
};

int busca(Ponto v[], int x, int t){

    int meio, ini = 0, resp;

    while(ini <= t){
        meio = (ini + t) / 2;
        if (v[meio].posicao > x){
            resp = v[meio].ponto;
            t = meio-1;
        }else{
            ini = meio+1;
        }
    }
    return resp;
}

int main(){

    int i, j, n, m, x;
    Ponto v[T];

    cin>>n>>m;

    j = n-1;
    for (i = 0; i < j; i++){
        cin>>v[i].posicao;
    }
    v[i].posicao = Tm;

    for (i = 0; i < n; i++){
        cin>>v[i].ponto;
    }

    cin>>x;
    j = busca(v, x, n-1);
    cout<<j;

    for (i = 1; i < m; i++){
        cin>>x;
        j = busca(v, x, n-1);
        cout<<" "<<j;
    }
    cout<<"\n";
}