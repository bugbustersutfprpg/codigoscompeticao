#include <iostream>

using namespace std;
#define T 100000

int encontre(int v[], int n, int x){

    int meio, ini;
    ini = 0;

    while(ini <= n){
        meio = (ini+n)/2;
        if (v[meio] == x)
            return meio;

        if (v[meio] < x)
            ini = meio+1;
        else
            n = meio-1;
    }
    return -1;
}

int main(){

    int i, n, v[T], x, y;

    cin>>n;

    for (i = 0; i < n; i++){
        cin>>v[i];
    }
    cin>>x;

    for(i = 0; i < n; i++){
        y = encontre(v, n-1, x - v[i]);
        if (y != -1)
            break;
    }
    cout<<v[i]<<" "<<v[y]<<"\n";
}