#include <iostream>
#include <vector>

using namespace std;
#define infinito 200000000

int uniao(vector<int> &v){

    if (v.size() == 1) return 0;

    int t = 0;
    vector<int> v1, v2;
    int metade = v.size() / 2;

    for (int i = 0; i < metade; i++){
        v1.push_back(v[i]);
    }
    for (int i = metade; i < v.size(); i++){
        v2.push_back(v[i]);
    }

    t += uniao(v1);
    t += uniao(v2);

    v1.push_back(infinito);
    v2.push_back(infinito);

    int j = 0, k = 0;
    for(int i = 0; i < v.size(); i++){
        if (v1[j] < v2[k]){
            v[i] = v1[j++];
        }else{
            t += v1.size() - j - 1;
            v[i] = v2[k++];
        }
    }

    return t;
}

int main(){

    int j, n;
    vector<int> v;
    cin >> n;

    for (int i = 0; i < n; i++){
        cin >> j;
        v.push_back(j);
    }

    cout << uniao(v) << endl;
}