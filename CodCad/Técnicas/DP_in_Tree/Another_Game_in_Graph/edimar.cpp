#include <bits/stdc++.h>

using namespace std;
#define N 100100

vector<int> v[N];
int peso[N];
int pd[N][2];
int pai[N];

int buscaPD(int x, bool pai_incluso){

    if (pd[x][pai_incluso] != -1) return pd[x][pai_incluso];

    int caso1 = peso[x], caso2 = 0;
    for (int i = 0; i < v[x].size(); i++){
        int u = v[x][i];
        if (pai[x] == u) continue;
        pai[u] = x;

        caso1 += buscaPD(u, 1);
        caso2 += buscaPD(u, 0);
    }

    if (pai_incluso) pd[x][pai_incluso] = caso2;
    else pd[x][pai_incluso] = max(caso1, caso2);

    return pd[x][pai_incluso];
}


int main(){

    int n, x, y;

    cin >> n;

    for (int i = 1; i < n; i++){
        cin >> x >> y;
        v[x].push_back(y);
        v[y].push_back(x);
    }

    for (int i = 1; i <= n; i++){
        cin >> peso[i];
        pd[i][0] = -1;
        pd[i][1] = -1;
    }

    int ans = buscaPD(1, 0);

    cout << ans << endl;
}