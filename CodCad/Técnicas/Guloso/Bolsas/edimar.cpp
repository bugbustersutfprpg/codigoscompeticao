#include <iostream>
#include <algorithm>

using namespace std;

struct Bolsa{

    int t;
    int d;

};

bool compara(Bolsa a, Bolsa b){
    return a.d > b.d;
}

int main(){

    int i, n, cont, aux, tempo, soma;
    Bolsa vetor[10000];

    cin>>n;
    cont = 0;
    soma = 0;

    for(i = 0; i < n; i++){
        cin>>vetor[i].t>>vetor[i].d;
        soma += vetor[i].t;
    }

    sort(vetor, vetor+n, compara);

    for(i = 0; i < n; i++){
        //cout<<vetor[i].d<<"\n";
        aux = soma - vetor[i].d;
        if (aux > cont)
            cont = aux;

        soma -= vetor[i].t;
    }

    cout<<cont<<"\n";
}