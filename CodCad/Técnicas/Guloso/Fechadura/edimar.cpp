#include <iostream>
#include <cmath>

using namespace std;

int main(){

    int n, m, soma, i, sobra, aux, cont;

    cin>>n>>m;
    cont = 0;
    sobra = 0;

    for(i = 0; i < n; i++){
        cin >>aux;

        if (soma)
            aux += sobra;
        else
            aux -= sobra;

        if (aux < m){
            soma = 1;
            sobra = m - aux;
        }else{
            soma = 0;
            sobra = aux - m;
        }

        cont += sobra;
    }
    cout <<cont<<"\n";
}