#include <iostream>

using namespace std;

int main(){

    int n, cont;

    cin>>n;
    cont = 0;

    while(n >= 100){
        cont++;
        n -= 100;
    }
    if(n >= 50){
        cont++;
        n -= 50;
    }
    if(n >= 25){
        cont++;
        n -= 25;
    }
    while(n >= 10){
        cont++;
        n -= 10;
    }
    if(n >= 5){
        cont++;
        n -= 5;
    }
    while(n >= 1){
        cont++;
        n -= 1;
    }

    cout<<cont<<"\n";
}