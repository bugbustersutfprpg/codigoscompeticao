#include <bits/stdc++.h>

using namespace std;
#define PB push_back
vector<int> v;

int lis(){

    vector<int> pilha;
    vector<int>::iterator it;
    for (int i=0; i < v.size(); i++){
        it = upper_bound(pilha.begin(), pilha.end(), v[i]);

        if (it == pilha.end())
            pilha.PB(v[i]);
        else
            *it = v[i];
    }

    return pilha.size();
}

int main(){

    int n, x;
    cin >> n;

    for (int i=0; i <n; i++){
        cin >> x;
        v.PB(x);
    }

    cout << lis() << endl;
}
