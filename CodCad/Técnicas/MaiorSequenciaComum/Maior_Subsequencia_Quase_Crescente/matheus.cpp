#include <bits/stdc++.h>

using namespace std;

vector<int> v;

int subseq(){
	int i;
	vector<int> vet;
	vector<int>::iterator it;
	for(i = 0; i < v.size(); i++){
		it = upper_bound(vet.begin(), vet.end(), v[i]);

		if(it == vet.end())
			vet.push_back(v[i]);
		else
			*it = v[i];
	}
	
	return vet.size();
}

int main(){
	int n, i;
	cin >> n;
	for(i = 0; i < n; i++){
		int aux;
		cin >> aux;
		v.push_back(aux);
	}

	cout << subseq() << endl;
	return 0;
}