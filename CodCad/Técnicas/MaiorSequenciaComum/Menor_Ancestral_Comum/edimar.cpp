#include <bits/stdc++.h>

using namespace std;
#define T 1001

int dp[T][T], v[T], v2[T];

int lcs(int a, int b){

    if (dp[a][b] >= 0) return dp[a][b];
    if (!a or !b) return dp[a][b] = 0;

    if (v[a] == v2[b]) return dp[a][b] = 1 + lcs(a-1, b-1);

    return dp[a][b] = max(lcs(a-1, b), lcs(a, b-1));
}

int main(){

    int n, m;

    scanf("%d %d", &n, &m);

    for (int i = 1; i <= n; i++){
        scanf("%d", &v[i]);
    }
    for (int i = 1; i <= m; i++){
        scanf("%d", &v2[i]);
    }
    for (int i = 0; i <= n; i++){
        for (int j = 0; j <= m; j++){
            dp[i][j] = -1;
        }
    }

    int ans = lcs(n, m);

    printf("%d %d\n", n-ans, m-ans);
}
