#include <bits/stdc++.h>

using namespace std;
#define PB push_back
vector<char> v;

int lis(){

    vector<char> pilha;
    vector<char>::iterator it;
    for (int i=0; i < v.size(); i++){
        it = upper_bound(pilha.begin(), pilha.end(), v[i]);

        if (it == pilha.end())
            pilha.PB(v[i]);
        else
            *it = v[i];
    }

    return pilha.size();
}

int main(){

    int n, x;
    char c;
    while(!feof(stdin)){
        scanf("%c", &c);
        if (c == '\n') break;
        v.PB(c);
    }

    cout << lis() << endl;
}
