#include <bits/stdc++.h>

using namespace std;
#define PB push_back
unordered_map<int, int> mapa;
unordered_map<int,int>::iterator it_map;
vector<int> v;
vector<int>::iterator it;

int main(){

    int n, m, x;
    cin >>n>>m;
    for (int i=1; i <= n; i++){
        scanf("%d", &x);
        mapa[x] = i;
    }
    for (int i=1; i <= m; i++){
        scanf("%d", &x);
        it_map = mapa.find(x);
        if (it_map != mapa.end()){
            x = it_map->second;
            it = lower_bound(v.begin(), v.end(), x);
            if (it == v.end())
                v.PB(x);
            else
                *it = x;
        }
    }

    cout << v.size() << endl;
}
