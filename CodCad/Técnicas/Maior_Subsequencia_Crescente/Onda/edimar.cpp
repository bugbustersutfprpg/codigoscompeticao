#include <bits/stdc++.h>

using namespace std;
#define PB push_back
#define T 100010

vector<int> v, cre, decr, pos_c, pos_d;
vector<int>::iterator it, it2;

int onda(){

    for (int i=0; i < v.size(); i++){
        it = lower_bound(cre.begin(), cre.end(), v[i]);
        if (it == cre.end()){
            cre.PB(v[i]);
            pos_c.PB(i);
        }else{
            *it = v[i];
        }
    }
    for (int i=v.size()-1; i >= 0; i--){
        it = lower_bound(decr.begin(), decr.end(), v[i]);
        if (it == decr.end()){
            decr.PB(v[i]);
            pos_d.PB(i);
        }else{
            *it = v[i];
        }
    }

    while(cre.size() > decr.size()){
        cre.pop_back();
        pos_c.pop_back();
    }
    while(cre.size() < decr.size()){
        decr.pop_back();
        pos_d.pop_back();
    }

    int x = pos_c.back();
    int y = pos_d.back();
    while(x > y){
        pos_c.pop_back();
        x = pos_c.back();
        pos_d.pop_back();
        y = pos_d.back();
    }

    return pos_c.size() * 2 - 1;
}

int main(){

    int n, x;

    cin >> n;

    for (int i=0; i <n; i++){
        cin >> x;
        v.PB(x);
    }

    cout << onda() << endl;
}
