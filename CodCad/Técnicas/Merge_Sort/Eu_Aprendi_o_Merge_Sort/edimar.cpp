#include <iostream>
#include <algorithm>

using namespace std;
#define T 100000

bool compara(int a, int b){
    return a > b;
}

int main(){

    int n;
    int v[T];

    cin >> n;

    for (int i = 0; i < n; i++){
        cin >> v[i];
    }

    sort(v, v+n, compara);
    n--;

    for (int i = 0; i < n; i++){
        cout << v[i] << " ";
    }
    cout << v[n] << endl;
}