#include <bits/stdc++.h>

using namespace std;
#define T 2001

int v[T], dp[T][T], pro[T], n;

long long fdp(int p, int peso){

    if (dp[p][peso] != -1) return dp[p][peso];

    if (p == n or !peso) return dp[p][peso] = 0;

    long long nao_coloca = fdp(p+1, peso);

    if (peso >= v[p]){
        long long coloca = pro[p] + fdp(p+1, peso-v[p]);

        return dp[p][peso] = max(coloca, nao_coloca);
    }

    return dp[p][peso] = nao_coloca;
}


int main(){

    int p;

    memset(dp, -1, sizeof dp);

    scanf("%d %d", &p, &n);

    for (int i = 0; i < n; i++){
        scanf("%d %d", &v[i], &pro[i]);
    }

    cout << fdp(0,p) << endl;
}
