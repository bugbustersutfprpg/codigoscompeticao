#include <bits/stdc++.h>

using namespace std;
#define T 1001

int dp[T][T], v[T], qtd[T], n;

int fdp(int p, int c){

    if (dp[p][c] != -1) return dp[p][c];
    if (p == n or !c) return dp[p][c] = 0;

    int nao_coloca = fdp(p+1, c);

    if (v[p] <= c){
        int coloca = qtd[p] + fdp(p+1, c-v[p]);

        return dp[p][c] = max(coloca, nao_coloca);
    }

    return dp[p][c] = nao_coloca;
}

int main(){

    int c, i = 1;

    scanf("%d %d", &c, &n);
    do{
        memset(dp, -1, sizeof dp);
        for (int i = 0; i < n; i++){
            scanf("%d %d", &v[i], &qtd[i]);
        }

        printf("Teste %d\n", i++);
        printf("%d\n", fdp(0,c));

        scanf("%d %d", &c, &n);
        if (!c)
            break;
        puts("");

    }while(1);
}
