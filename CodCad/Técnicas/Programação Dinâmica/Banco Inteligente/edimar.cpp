#include <bits/stdc++.h>

using namespace std;
long long pd[5001][7];
int nota[7], valor[7];

long long contar(int resta, int j = 0){

    if (resta == 0) return 1;
    if (j > 5) return 0;

    if (pd[resta][j] != -1) return pd[resta][j];

    long long total = 0;
    for (int i = 0; i <= nota[j]; i++){
        if (resta < i*valor[j]) break;

        total += contar(resta - i * valor[j], j+1);
    }

    return pd[resta][j] = total;
}

int main(){

    int s;
    memset(pd, -1, sizeof(pd));

    cin >> s;

    valor[0] = 2;
    valor[1] = 5;
    valor[2] = 10;
    valor[3] = 20;
    valor[4] = 50;
    valor[5] = 100;
    valor[6] = 0;

    for (int i = 0 ; i < 6; i++){
        cin >> nota[i];
    }

    cout << contar(s) << endl;
}
