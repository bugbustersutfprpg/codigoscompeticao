#include <iostream>
#include <cstring>

using namespace std;
#define T 1010
#define inteiro 2000000000

long long n, m;
long long a[T], f[T];

long long rec(int k){

    if (f[k] != -1) return f[k];

    long long r = 0;
    for (int i = 1; i <= n; i++){
        r += (a[i] * rec(k-i)) % m;
    }

    f[k] = r % m;
    return f[k];
}

int main(){

    long long k, j;

    cin >> n >> k >> m;

    memset(f, -1, sizeof(f));

    for (int i = 1; i <= n; i++)
        cin >> a[i];

    for (int i = 1; i <= n; i++){
        cin >> j;
        f[i] = j % m;
    }

    cout << rec(k) << endl;
}