#include <bits/stdc++.h>

using namespace std;

int main(){

    long long maior = 0, soma = 0;
    int n, x;

    cin >> n;

    while(n--){
        cin >> x;
        soma = max((long long)0, soma+x);

        maior = max(maior, soma);
    }

    cout << maior << endl;
}