#include <bits/stdc++.h>

using namespace std;

int main(){

    int maior = 0, soma = 0;
    int n, x, j = 0, b = 0;
    vector<int> v;

    cin >> n;

    for(int i = 0; i < n; i++){
        cin >> x;
        soma = max(0, soma+x);
        v.push_back(x);
        if (!soma) j = i+1;

        maior = max(maior, soma);
    }

    for(int i = 0; i < v.size(); i++){
        if (!soma) break;
        if (i == j) {
            b = 1;
            break;
        }
        soma = max(0, soma+v[i]);
        maior = max(maior, soma);
    }

    if (b){
        int menor = 0;
        int som = 0;
        for (int i = j+1; i != j; i = (i+1)%v.size()){
            som = min(0, som+v[i]);
            menor = min(som, menor);
        }
        maior = max(maior, soma-menor);
    }

    cout << maior << endl;
}