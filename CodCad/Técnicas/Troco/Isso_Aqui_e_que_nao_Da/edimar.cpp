#include <bits/stdc++.h>

using namespace std;
#define T 1001

int v[T], dp[T], n, m, total;

int solve(int x){

    if (x >= m) return 0;
    if (dp[x]) return 1;

    dp[x] = 1;
    total--;

    for (int i = 0; i < n; i++){
        solve(x + v[i]);
    }

    return 0;
}

int main(){

    cin >> n >> m;
    total = m-1;

    for (int i = 0; i < n; i++){
        cin >> v[i];
    }

    for (int i = 0; i < n; i++)
        solve(v[i]);

    cout << total << endl;
}