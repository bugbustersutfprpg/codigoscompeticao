#include <bits/stdc++.h>

using namespace std;
#define T 1001

bool pd[T];
int m, v[T];

int moedas(int valor, int i, int qtd){

	if (qtd > 9 or valor > m or pd[valor] == 1) return 1;
	if (valor == m) return 2;

	for ( ; i >= 0; i--){
		if (moedas(valor + v[i], i, qtd+1) == 2){
			return 2;		
		}
	}
	
	return pd[valor] = 1;
}

int main(){

	int n;

	cin >> n >> m;

	for (int i = 0; i < n; i++){
		cin >> v[i];
	}
	sort(v, v+n);

	if (moedas(0, n-1, 0) == 2)
		cout << "S\n";
	else
		cout << "N\n";

}