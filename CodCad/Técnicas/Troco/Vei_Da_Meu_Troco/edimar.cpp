#include <bits/stdc++.h>

using namespace std;
#define T 1001

int pd[T], troco, v[T];

int trocado(int valor, int i){
    if (valor > troco) return 0;
    if (valor == troco) return 1;
    if (pd[valor]) return pd[valor];

    for ( ;i >=0 ;i--){
        if (trocado(valor+v[i], i) == 1)
            return 1;
    }

    return pd[valor] = -1;
}

int main(){

    int n;

    cin >> n >> troco;

    for (int i = 0; i < n; i++){
        cin >> v[i];
    }

    sort(v, v+n);

    if (trocado(0, n-1) == 1)
        cout << "S\n";
    else
        cout << "N\n";
}