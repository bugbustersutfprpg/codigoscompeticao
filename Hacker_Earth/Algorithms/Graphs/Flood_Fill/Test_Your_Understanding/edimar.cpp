#include <bits/stdc++.h>
using namespace std;

int n, m;
bool ma[11][11];

bool bfs(int x, int y){
    if (x > n or y > m) return false;
    if (x < 0 or y < 0) return false;
    if (!ma[x][y]) return false;
    
    if (x == n and y == m) return true;
    ma[x][y] = false;
    
    if (bfs(x+1, y) == true) return true;
    if (bfs(x-1, y) == true) return true;
    if (bfs(x, y+1) == true) return true;
    if (bfs(x, y-1) == true) return true;
    
    return false;
}

int main()
{
    cin >> n >> m;
    
    for (int i = 1; i <= n; i++){
        for (int j = 1; j <= m; j++){
            cin >> ma[i][j];
        }
    }
    
    if (bfs(1, 1))
        cout << "Yes" << endl;
    else
        cout << "No\n";
    return 0;
}
