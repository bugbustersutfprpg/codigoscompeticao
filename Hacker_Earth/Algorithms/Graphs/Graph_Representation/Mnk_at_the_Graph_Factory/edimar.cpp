#include <iostream>
using namespace std;

int main(){
    
    int n, value, total = 0;
    
    cin >> n;
    
    for (int i = 0; i < n; i++){
        cin >> value;
        total += value;
    }
    
    if ( (total % 2) == 0 && (total / 2) == (n - 1) )
        cout << "Yes\n";
    else
        cout << "No\n";
}
