#include <bits/stdc++.h>

using namespace std;

int main()
{
    int n, e, x, y;
    set<int> cidade;
    
    cin >> n;
    
    for (int i = 0; i < n; i++){
        cin >> e;
        
        for (int j = 0; j < e; j++){
            cin >> x >> y;
            cidade.insert(x);
            cidade.insert(y);
        }
        
        cout << cidade.size() << endl;
        cidade.clear();
    }
}
