#include <bits/stdc++.h>
using namespace std;
#define T 1001

bool v[T][T];

int main()
{
    int n, m, a, b, query;
    
    cin >> n >> m;
    
    for (int i = 0; i < m; i++){
        cin >> a >> b;
        v[a][b] = 1;
        v[b][a] = 1;
    }
    
    cin >> query;
    for (int i = 0; i < query; i++){
        cin >> a >> b;
        if (v[a][b])
            cout << "YES\n";
        else
            cout << "NO\n";
    }
}
