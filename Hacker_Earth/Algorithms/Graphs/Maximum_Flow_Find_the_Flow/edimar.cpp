#include <bits/stdc++.h>
using namespace std;
#define T 21
#define INF 2e9

int matriz[T][T];
int level[T];
int pai[T];

bool bfs(){

    for (int i = 0; i < T; i++){
        level[i] = INF;
    }

    list<int> q;
    int s = 'S' - 'A';
    level[s] = 0;
    q.push_back(s);

    while(q.size()){
        int x = q.front();
        if (level[x] == level['T'-'A']) break;
        q.pop_front();

        for (int i = 0; i < T; i++){
            if (matriz[x][i] and level[i] == INF){
                level[i] = level[x] + 1;
                pai[i] = x;
                q.push_back(i);
            }
        }
    }

    return level['T' - 'A'] == INF ? false : true;
}

int maxFlow(int x, int recebido){

    if (x == 'T' - 'A'){
        return recebido;
    }

    for (int i = 0; i < T; i++){
        int saida = matriz[x][i];

        if (saida > 0 and level[x] == (level[i] - 1) and (level[i] < level['T'-'A'] or i == 'T'-'A') ){
            saida = min(recebido, saida);

            int fluxo = maxFlow(i, saida);

            if (fluxo){
                matriz[i][x] += fluxo;
                matriz[x][i] -= fluxo;
                return fluxo;
            }
        }
    }
    return 0;
}

int main(){

    int e, x, t;
    char a, b;

    cin >> e;

    for (int i = 0; i < e; i++){
        cin >> a >> b >> x;
        matriz[a-'A'][b-'A'] = x;
    }

    int resposta = 0;
    while(bfs() == true){
        /*while (t = maxFlow('S' - 'A', INF)){
            resposta += t;
        }
        */
        int f = 'T' - 'A';
        int fluxo = INF;

        while(f != 'S' - 'A'){
            int p = pai[f];
            fluxo = min(fluxo, matriz[p][f]);
            f = p;
        }

        resposta += fluxo;
        f = 'T' - 'A';
        while(f != 'S' - 'A'){
            int p = pai[f];
            matriz[p][f] -= fluxo;
            matriz[f][p] += fluxo;
            f = p;
        }
        
    }

    cout << resposta << endl;
}
