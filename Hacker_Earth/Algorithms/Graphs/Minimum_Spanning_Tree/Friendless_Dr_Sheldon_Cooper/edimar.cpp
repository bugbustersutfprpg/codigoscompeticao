#include <iostream>
using namespace std;

int main()
{
    int n, m, a, b, t;
    
    cin >> t;
    
    while(t--){
        cin >> n >> m;
        
        for(int i = 0; i < m; i++){
            cin >> a >> b;
        }
        
        cout << n-1 << endl;
    }    
    return 0;
}
