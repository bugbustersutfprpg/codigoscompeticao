#include <bits/stdc++.h>
using namespace std;
#define T 10001

typedef pair<int, int> PII;
vector<PII> v[T];
bool marked[T];
int cont, n;

long long prim(){
    
    long long total = 0;
    priority_queue<PII, vector<PII>, greater<PII>> fila;
    fila.push(make_pair(0, 1));
    
    while (!fila.empty()){
        PII p = fila.top();
        fila.pop();
        int x = p.second;
        
        if (marked[x]) continue;
        marked[x] = 1;
        total += p.first;
        
        for (int j = 0; j < v[x].size(); j++){
            if (marked[v[x][j].second] == false)
                fila.push(v[x][j]);
        }
    }
    
    return total;
}

int main()
{
    int m, a, b, peso;
    
    cin >> n >> m;
    
    for (int i = 0; i < m; i++){
        cin >> a >> b >> peso;
        v[a].push_back(make_pair(peso, b));
        v[b].push_back(make_pair(peso, a));
    }
    
    long long total = prim();
    
    cout << total << endl;
    
    return 0;
}
