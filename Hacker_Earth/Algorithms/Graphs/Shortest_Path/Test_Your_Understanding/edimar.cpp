#include <bits/stdc++.h>

using namespace std;
vector <long long> v [1000000 + 10];

int main()
{
    long long n, m, from , next, weight;
    long long dis [10000 + 10];
    
    cin >> n >> m;

    for(int i = 2; i <= n; i++){
        dis[i] = 10e9;
    }

   for(int i = 1; i <= m; i++){

        cin >> from >> next >> weight;

        v[i].push_back(from);
        v[i].push_back(next);
        v[i].push_back(weight);
   }

    bool flag;
    dis[1] = 0;
    for(int i = 1; i < n; i++){
        flag = true;
        int j = 1;
        while(v[j].size() != 0){
            if(dis[ v[j][0]  ] + v[j][2] < dis[ v[j][1] ] ){
                dis[ v[j][1] ] = dis[ v[j][0]  ] + v[j][2];
                flag = false;
            }
            j++;
        }
        
        if (flag) break;
    }
    
    for (int i = 2; i < n; i++){
        cout << dis[i] << " ";
    }
    cout << dis[n] << endl;
}
