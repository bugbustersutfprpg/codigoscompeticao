#include <bits/stdc++.h>

using namespace std;
#define T 100100

int main(){

    int t, n, c[T], l[T];

    cin >> t;

    while(t--){
        cin >> n;

        for (int i =0; i < n; i++){
            cin >> c[i];
        }
        for (int i =0; i < n; i++){
            cin >> l[i];
        }

        long long menor = c[0];
        long long total = menor*l[0];
        for (int i =1; i < n;i++){
            if (c[i] < menor) menor = c[i];
            total += menor*l[i];
        }

        cout << total << endl;
    }
}
