#include <iostream>
#include <algorithm>

using namespace std;
#define T 100000

int main()
{
    int n, v[T];
    int query, value;
    
    cin >> n;
    
    for (int i = 0; i < n; i++){
        cin >> v[i];
    }
    sort(v, v+n);
    
    cin >> query;
    
    for (int i = 0; i < query; i++){
        cin >> value;
        cout << lower_bound(v, v+n, value) - v + 1 << endl;
    }
}
