#include <iostream>
#include <algorithm>

using namespace std;
#define T 100005

int main()
{
    int transaction[T];
    int n, query, value, k;
    
    cin >> n;
    transaction[0] = 0;
    for (int i = 1; i <= n; i++){
        cin >> value;
        transaction[i] = value + transaction[i-1];
    }
    
    cin >> query;
    for(int i = 0; i < query; i++){
        cin >> value;
        k = lower_bound(transaction, transaction + n, value) - &transaction[0];
        if (k == n and transaction[n] < value)
            k = -1;
        cout << k << endl;
    }
}
