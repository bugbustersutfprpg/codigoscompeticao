#include <iostream>
using namespace std;

int main()
{
    int v, n, res, m;
    
    cin >> n >> m;
    
    res = -1;
    for (int i = 1; i <= n; i++){
        cin >> v;
        if (v == m) res = i;
    }
    
    cout << res << endl;
}
