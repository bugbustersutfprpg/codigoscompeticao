#include <bits/stdc++.h>

using namespace std;

int main(){

    int N, n[200001];
    set<int> dif, aux;
    map<int, int> mapa;

    cin >> N;

    for (int i = 0; i < N; i++){
        cin >> n[i];
        dif.insert(n[i]);
    }

    int j = 0;
    long long ans = 0;
    for (int i = 0; i < N; i++){
        j = max(i, j);
        while (j < N and dif.size() > aux.size()){
            mapa[n[j]]++;
            if (mapa[n[j]] == 1){
                aux.insert(n[j]);
            }
            j++;
        }
        if (dif.size() == aux.size()){
            ans += N-j+1;
        }
        mapa[n[i]]--;
        if (mapa[n[i]] == 0){
            aux.erase(n[i]);
        }
    }

    cout << ans << endl;
}
