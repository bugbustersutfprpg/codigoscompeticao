#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n;
    string s, ans;
    char c;
    
    cin >> n;
    
    while(n--){
        cin >> s;
        
        ans = "";
        int j = s.length()-1;
        for (int i= 0; i < s.length(); i++){
            c = (s[i] - 'a' + 1) + (s[j--] - 'a' + 1) ;
            c %= 26;
            c += 'a' - 1;
            if (c < 'a') c = 'z';
            
            cout << c;
        }
        puts("");
    }
    
}