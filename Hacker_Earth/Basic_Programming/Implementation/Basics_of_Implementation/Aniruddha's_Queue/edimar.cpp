#include <bits/stdc++.h>
using namespace std;

int main(){
    
    long long t, n, v[100100], m, total, ans, last;
    
    cin >> t;
    
    while(t--){
        cin >> n;
        total = 0;
        for (int i = 1; i <= n; i++){
            cin >> v[i];
            total += v[i];
            if (v[i])
                last = i;
        }
        cin >> m;
        
        m %= total;
        
        if (m == 0){
            cout << last << endl;
        }else{
            int i;
            for (i = 1; m > 0; i++)
                m -= v[i];
                
            cout << i-1 << endl;
        }
    }
    
}