#include <bits/stdc++.h>

using namespace std;
#define T 100100

long long v[T];
int n, arr[T];

long long soma(int pos){

    long long ans = 0;
    while(pos > 0){
        ans += v[pos];
        pos -= (pos & -pos);
    }
    return ans;
}

void update(int pos, int x){

    while(pos <= n){
        v[pos] += x;
        pos += (pos & -pos);
    }
}

void printar(){
    for (int i = 1; i <= n; i++){
        cout << v[i] << " ";
    }
    puts("");
}

int main(){

    int q, type, a, b;

    cin >> n >> q;

    for (int i = 1; i <= n; i++){
        cin >> arr[i];
        update(i, arr[i]);
    }

    //printar();
    for (int i = 0; i < q; i++){
        cin >> type >> a >> b;
        a++;

        if (type == 1){
            update(a, b - arr[a]);
            arr[a] = b;
            //printar();
        }else{
            if (++b <= n)
                cout << soma(b) - soma(a-1) << endl;
        }
    }
}
