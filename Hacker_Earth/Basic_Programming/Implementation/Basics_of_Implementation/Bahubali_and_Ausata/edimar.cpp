#include <bits/stdc++.h>

using namespace std;
#define T 1001000

long long v[T];

int main(){
    
    int n, q, l, r, x;
    
    cin >> n >> q;
    
    v[0] = 0;
    for (int i = 1; i <= n; i++){
        cin >> x;
        v[i] = v[i-1] + x;
    }
    
    while(q--){
        cin >> l >> r;
        
        cout << (v[r] - v[l-1]) / (r-l+1) << endl;
    }
    
}