#include <bits/stdc++.h>

using namespace std;
char m[4][4];
string s[3] = {".xx", "x.x", "xx."};

bool testaColuna(char m[][4]){
    
    string ans = "";
    for (int i = 0; i < 4; i++){
        ans = "";
        for (int j = 0; j < 4; j++){
            ans += m[j][i];
            if (j == 3) ans = ans.substr(1);
            if (j > 1){
                if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
                    return true;
            }
        }
    }
    return false;
}

bool testaDiagonal(char m[][4]){
    string ans = "";
    ans += m[0][0]; ans += m[1][1]; ans += m[2][2];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
    
    ans = "";
    ans += m[1][1]; ans += m[2][2]; ans += m[3][3];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
        
    ans = "";
    ans += m[0][1]; ans += m[1][2]; ans += m[2][3];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
        
    ans = "";
    ans += m[1][0]; ans += m[2][1]; ans += m[3][2];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
        
    ans = "";
    ans += m[0][3]; ans += m[1][2]; ans += m[2][1];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
        
    ans = "";
    ans += m[1][2]; ans += m[2][1]; ans += m[3][0];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
    
    ans = "";
    ans += m[0][2]; ans += m[1][1]; ans += m[2][0];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
    
    ans = "";
    ans += m[1][3]; ans += m[2][2]; ans += m[3][1];
    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
        return true;
    
    return false;
}

int main(){
    
    int n;
    char x;
    string ans = "";
    
    cin >> n;

    while(n--){
        bool victory = false;
        
        for (int i = 0; i < 4; i++){
            ans = "";
            for (int j = 0; j < 4; j++){
                cin >> x;
                m[i][j] = x;
                ans += x;
                
                if (j == 3) ans = ans.substr(1);
                if (j > 1)
                    if (!ans.compare(s[0]) or !ans.compare(s[1]) or !ans.compare(s[2]))
                        victory = true;
            }
        }
        
        if (!victory){
            if (testaColuna(m)){
                victory = true;
            }else if (testaDiagonal(m)){
                victory = true;
            }
        }
        
        if (victory)
            cout << "YES\n";
        else
            cout << "NO\n";
    }    
}