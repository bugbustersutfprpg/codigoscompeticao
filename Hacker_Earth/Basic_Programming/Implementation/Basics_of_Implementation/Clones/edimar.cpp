#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n, z;
    
    cin >> n >> z;
    
    int sum;
    if (n == 1) sum = 1;
    else if (n == 2) sum = 2;
    else if (n == 3 || n == 4) sum = 6;
    else if (n == 5) sum = 3;
    else sum = 9;
    
    double aux = 0;
    for (int i = 2; i <= n; i++)
        aux += log10(i);
        
    int len = aux + 1;
    
    cout << len + sum << endl;
}