#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t, k, x, j;
    int v[7];
    string s[7] = {"MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY"};
    
    cin >> t;
    
    while(t--){
        cin >> k;
        
        long long sum = 0;
        for (int i = 0; i < 7; i++){
            cin >> v[i];
            sum += v[i];
        }
        
        j = 0;
        k %= sum;
        if (k == 0){
            j = 6;
            while(!v[j]) j--;
        }else{
            do {
                k -= v[j];
                if (k > 0){
                    j++;
                    j %= 7;
                }else break;
            }while(1);
        }
        
        cout << s[j] << endl;
    }
    
}