#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t, n, j;
    string s;
    
    cin >> t;
    
    while(t--){
        cin >> n >> s;
    
        int cont = 0;
        for (int i =0; i < s.length(); i++){
            if (s[i] >= '0' && s[i] <= '9'){
                for (j = i+1; j < s.length() && s[j] >= '0' && s[j] <= '9'; j++);
                cont++;
                i = j;
            }
        }
        
        cout << cont << endl;
    }
    
}