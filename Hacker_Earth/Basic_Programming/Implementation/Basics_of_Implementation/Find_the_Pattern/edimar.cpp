#include <bits/stdc++.h>

using namespace std;
#define INF 2e18+5

int main(){
    
    long long n, x, menor, maior;
    
    cin >> n;
    
    maior = -INF;
    menor = INF;
    for (int i= 0; i < n; i++){
        cin >> x;
        
        maior = max(maior, x);
        menor = min(menor, x);
    }
    
    cout << maior + menor << endl;
}