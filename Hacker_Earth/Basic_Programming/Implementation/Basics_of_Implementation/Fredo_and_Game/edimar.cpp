#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t;
    
    cin >> t;
    
    while(t--){
        int n, gas;
        cin >> gas >> n;
        
        bool b, left = false;
        for(int i = 1; i <= n; i++){
            cin >> b;
            
            if (left) continue;
            
            if (b)
                gas += 3;
                
            gas--;
            
            if (gas == 0 and i < n){
                cout << "No " << i << endl;
                left = true;
            }
        }
        
        if (!left) cout << "Yes " << gas << endl;
    }
    
}