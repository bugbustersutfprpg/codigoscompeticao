#include <bits/stdc++.h>

using namespace std;

int main(){
    
    long long t, x, n, som;
    long long ans;
    
    cin >> t;
    
    while(t--){
        cin >> n;
        if (n == 1){
            cout << "1\n";
            continue;
        }
        
        x = (-3 + (int)sqrt(9 + 8*n)) / 2;
        ans = (n - x) * 2;
    
        cout << ans << endl;
    }
    
}