#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n, x, y, p, dif;
    bool zero;
    
    cin >> n >> x >> y >> p;
    
    for (int i = 0; i < n; i++){
        
        if ( abs(i - x) >= p ){
            for (int j=0; j < n; j++)
                cout << "0 ";
        }else{
            //int dif = abs(abs(i-y) - p);
            for (int j=0; j < n; j++){
                dif = p - max(abs(i-x), abs(j-y));
                
                if (dif < 0) dif = 0;
                cout << dif << " ";
            }
        }
        
        puts("");
    }
    
}