#include <bits/stdc++.h>

using namespace std;

int v[100100];
int main(){
    
    int t, n, x;
    
    cin >> t;
    
    while(t--){
        cin >> n;
        
        bool equal = true;
        for (int i = 1; i <= n; i++){
            cin >> v[i];
        }
        
        for (int i = 1; i <= n; i++){
            if (v[v[i]] != i){
                equal = false;
                break;
            }
        }
        
        if (equal) cout << "inverse\n";
        else cout << "not inverse\n";
    }
    
}