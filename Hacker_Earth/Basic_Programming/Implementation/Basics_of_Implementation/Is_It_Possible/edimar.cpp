#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t;
    string s;
    
    cin >> t;
    
    while(t--){
        cin >> s;
        
        if ( (s[ s.length()-1 ] - '0') % 2)
            cout << "NO\n";
        else
            cout << "YES\n";
    }
    
}