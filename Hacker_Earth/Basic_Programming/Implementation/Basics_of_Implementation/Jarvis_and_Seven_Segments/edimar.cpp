#include <bits/stdc++.h>

using namespace std;
#define INF 2e9+7

int main(){
    
    string s, ans;
    int t, n, menor, k, x;
    int v[10] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
    
    cin >> t;
    
    while(t--){
        cin >> n;

        menor = INF;
        for (int i=0; i < n; i++){
            cin >> s;
            
            x = 0;
            for (int j = 0; j < s.length(); j++){
                x += v[s[j] - '0'];  
            }
            
            if (x < menor){
                menor = x;
                ans = s;
            }
        }
        
        cout << ans << endl;
    }
    
}