#include <bits/stdc++.h>

using namespace std;
const int T = 4e5+5;

vector<int> v[T], ans;
bool visit[T];


void busca(int x){
    
    ans.push_back(x);
    visit[x] = 1;
    
    for (auto i : v[x]){
        if (!visit[i]){
            busca(i);
            ans.push_back(x);
        }
    }
}

int main(){
    
    int m, n;
    
    cin >> n >> m;
    
    int x,y;
    for (int i=1; i <= m; i++){
        cin >> x >> y;
        
        v[x].push_back(y);
        v[y].push_back(x);
    }
    
    busca(1);
    
    cout << ans.size() << endl;
    for (auto i: ans){
        cout << i << " ";
    }
    return 0;
}