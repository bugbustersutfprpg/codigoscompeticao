#include <bits/stdc++.h>

using namespace std;

int main(){
    
    string s;
    
    cin >> s;
    
    int um = 0, zero = 0;
    bool sorte = true;
    for (int i=0; i <s.length();i++){
        if (s[i] == '1'){
            zero = 0;
            um++;
            if (um == 6){
                sorte = false;
                break;
            }
        }else{
            um = 0;
            zero++;
            if (zero == 6){
                sorte = false;
                break;
            }
        }
    }
    
    if (sorte){
        cout << "Good luck!\n";
    }else{
        cout << "Sorry, sorry!\n";
    }
}