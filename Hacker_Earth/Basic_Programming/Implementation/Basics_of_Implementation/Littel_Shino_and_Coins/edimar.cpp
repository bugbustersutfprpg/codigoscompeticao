#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int k;
    string s;
    map<char, int> le;
    
    cin >> k >> s;
    
    long long cont = 0;
    int c = 0;
    for (int i = 0; i < s.length(); i++){
        if (le.count(s[i]))
            le[s[i]]++;
        else
            le[s[i]] = 1;
            
        if (le.size() == k){
            int x = 1;
            for (int j = i+1; j < s.length() && le.count(s[j]); j++)
                x++;
            
            cont += x;
            if (le[s[c]] == 1){
                le.erase(s[c++]);
            }else{
                do{
                    le[s[c]]--;
                    cont += x;
                }while(le[s[++c]] > 1);
                le.erase(s[c++]);
            }
        }
    }
    
    cout << cont << endl;
}