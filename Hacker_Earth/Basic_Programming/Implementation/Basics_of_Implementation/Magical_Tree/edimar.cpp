#include <bits/stdc++.h>

using namespace std;
#define INF 2e9

int main(){
    
    int n, value, maior;
    string s;
    
    cin >> n;
    
    maior = -INF;
    while(n--){
        cin >> s;
        
        value = s[0] - '0';
        
        for (int i = 1; i < s.length(); i+=2){
            if (s[i] == '-'){
                value -= s[i+1] - '0';
            }else{
                value += s[i+1] - '0';
            }
        }
        
        maior = max(maior, value);
    }
    
    cout << maior << endl;
}