#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n;
    string s, s1;
    
    cin >> n >> s1;
    
    s = "";
    for (int i = 0; i < s1.length(); i++)
        if (s1[i] == 'a' || s1[i] == 'b')
            s += s1[i];
    
    list<int> l;
    for (int i = 1; i <= n; i++){
        l.push_back(i);
    }
    
    int i = 0;
    auto it = l.begin();
    while(l.size() > 1){
        if (s[i] == 'a')
            it++;
        else
            it = l.erase(it);
        
        if (it == l.end()) it = l.begin();
        if (++i == s.size()) i = 0;
    }
    
    cout << *it << endl;
}