#include <bits/stdc++.h>

using namespace std;
int v[27];

int main(){
    
    int n;
    string s;
    
    cin >> n >> s;
    
    for (int i=0; i < s.length(); i++){
        v[s[i]-'a']++;        
    }
    
    int mi = 200000000;
    mi = min(mi, v['a'-'a']/2);
    mi = min(mi, v['c'-'a']);
    mi = min(mi, v['h'-'a']/2);
    mi = min(mi, v['k'-'a']);
    mi = min(mi, v['e'-'a']/2);
    mi = min(mi, v['r'-'a']/2);
    mi = min(mi, v['t'-'a']);
    
    
    cout << mi << endl;
}