#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n, x, y, a;
    
    cin >> n >> x >> y;
    
    bool flag = true;
    for (int i =0; i < n; i++){
        cin >> a;
    
        if (a < x || a > y)
            flag = false;
    }
        
    if (flag)
        cout << "YES\n";
    else
        cout << "NO\n";
}