#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t, n;
    
    cin >> t;
    
    while(t--){
        cin >> n;
        
        int k = 1;
        for (int i=n-1; i >=0; i--){
            for (int j = i; j; j--)
                cout << " ";
            for (int j = 0; j < k; j++)
                cout << "*";
                
            k += 2;
            puts("");
        }
    }
    
}