#include <bits/stdc++.h>

using namespace std;
bool killed[1010];

int main(){
    
    int n, k, i, x, kill;
    
    cin >> n >> k >> x;
    
    int total = n++;
    while(1){
        kill = x%k;
        if (total <= kill)
            break;
        
        for (i = (x+1)%n; kill; i = (i+1)%n ){
            if (i == 0 or killed[i]) continue;
            killed[i] = 1;
            kill--;
            total--;
        }
        while(killed[i] or i == 0)
            i = (i+1)%n;
        
        x = i;
    }
    
    cout << x << endl;
}