#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t, n, m;
    long long ans;
    
    cin >> t;
    
    while(t--){
        cin >> n >> m;
        
        long long pen = 0, cont = 0, pc = 1;
        for (int i = 1; i < n; ){
            i += pen;
            if (pen != m){
                pen += pc;
                if (pen > m) pen = m;
            }
            pc = i;
            cont++;
        }
        
        cout << cont << endl;
    }
    
}