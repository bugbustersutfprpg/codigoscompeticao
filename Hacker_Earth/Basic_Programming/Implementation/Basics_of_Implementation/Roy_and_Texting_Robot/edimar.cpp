#include <bits/stdc++.h>

using namespace std;

void initial(int v[], int t[]){
v['.'] = v['a'] = v['d'] = v['g'] = v['j'] = v['m'] = v['p'] = v['t'] = v['w'] = v['_'] = 1;
v[','] = v['b'] = v['e'] = v['h'] = v['k'] = v['n'] = v['q'] = v['u'] = v['x'] = v['0'] = 2;
v['?'] = v['c'] = v['f'] = v['i'] = v['l'] = v['o'] = v['r'] = v['v'] = v['y'] = 3;
v['!'] = v['2'] = v['3'] = v['4'] = v['5'] = v['6'] = v['s'] = v['8'] = v['z'] = 4;
v['1'] =                                              v['7'] =          v['9'] = 5;

t['0'] = t['_'] = 0;
t['.'] = t[','] = t['?'] = t['!'] = t['1'] = 1;
t['a'] = t['b'] = t['c'] = t['2'] = 2;
t['d'] = t['e'] = t['f'] = t['3'] = 3;
t['g'] = t['h'] = t['i'] = t['4'] = 4;
t['j'] = t['k'] = t['l'] = t['5'] = 5;
t['m'] = t['n'] = t['o'] = t['6'] = 6;
t['p'] = t['q'] = t['r'] = t['s'] = t['7'] = 7;
t['t'] = t['u'] = t['v'] = t['8'] = 8;
t['w'] = t['x'] = t['y'] = t['z'] = t['9'] = 9;
}

int main(){
    
    int t, v[200], tecla[200], sum, pos;
    string s;
        
    cin >> t;
    
    initial(v, tecla);
    while(t--){
        cin >> s;
        
        sum = 0;
        pos = 1;
        for (int i = 0 ; i < s.length(); i++){
            sum += v[s[i]];
            if (tecla[s[i]] != pos){
                sum++;
                pos = tecla[s[i]];
            }
        }
        
        cout << sum << endl;
    }
    
}