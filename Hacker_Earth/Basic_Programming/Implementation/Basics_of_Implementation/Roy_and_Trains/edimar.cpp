#include <bits/stdc++.h>

using namespace std;

int main(){
    
    long long t, t0, t1, t2, v1, v2, d;
    
    cin >> t;
    
    while(t--){
        cin >> t0 >> t1 >> t2 >> v1 >> v2 >> d;
        
        if (t0 > t1){
            if (t0 > t2)
                cout << "-1\n";
            else{
                t2 += ceil((d/(double)v2)*60.0);
                cout << t2 << endl;
            }
        }else{
            if (t0 > t2){
                t1 += ceil((d/(double)v1)*60.0);
                cout << t1 << endl;
            }else{
                t1 += ceil((d/(double)v1)*60.0);
                t2 += ceil((d/(double)v2)*60.0);
            
                cout << min(t1, t2) << endl;
            }
        }
    }
}