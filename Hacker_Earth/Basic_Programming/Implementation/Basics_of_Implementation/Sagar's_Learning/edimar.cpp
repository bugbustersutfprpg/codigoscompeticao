#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t;
    long long x, y;
    
    cin >> t;
    
    while(t--){
        cin >> x;
        
        if (x < 3)
            cout << "-1\n";
        else{
            while(x%3) x--;
            
            y = x/3;
            cout << y << " " << y*2 << " " << x << endl;
        }
            
    }
    
}