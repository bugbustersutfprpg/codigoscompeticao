#include <bits/stdc++.h>

using namespace std;
#define T 100100

int main(){
    
    long long t, n, m, k, x=1, y=1, cont, a, b;
    
    cin >> t;
    
    while(t--){
        cin >> n >> m >> k;
        
        x=y=1;
        cont = 0;
        for (int i = 0; i < k; i++){
            cin >> a >> b;
            
            while(x+a > 0 and x+a <= n and y+b > 0 and y+b <= m){
                x += a;
                y += b;
                cont++;
            }
        }
        
        cout << cont << endl;
    }
    
}