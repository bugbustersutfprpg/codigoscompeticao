#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t, day, year;
    string mes;
    
    cin >> t;
    
    while(t--){
        cin >> day >> mes >> year;
        
        if (day == 1){
            if (mes[0] == 'J' and mes[1] == 'a'){
                cout << "31 December " << year-1 << endl;
            }else if (mes[0] == 'F'){
                cout << "31 January " << year << endl;
            }else if (mes.length() == 3){
                cout << "30 April " << year << endl;
            }else if (mes[0] == 'M'){
                if (year%100 == 0 or year%4)
                    cout << "28 February " << year << endl;
                else
                    cout << "29 February " << year << endl;
            }else if (mes[1] == 'p'){
                cout << "31 March " << year << endl;
            }else if (mes[2] == 'n'){
                cout << "31 May " << year << endl;
            }else if (mes[0] == 'J'){
                cout << "30 June " << year << endl;
            }else if (mes[0] == 'A'){
                cout << "31 July " << year << endl;
            }else if (mes[0] == 'S'){
                cout << "31 August " << year << endl;
            }else if (mes[0] == 'O'){
                cout << "30 September " << year << endl;
            }else if (mes[0] == 'N'){
                cout << "31 October " << year << endl;
            }else{
                cout << "30 November " << year << endl;
            }
        }else
            cout << day -1 << " " << mes << " " << year << endl;
    }
    
}
