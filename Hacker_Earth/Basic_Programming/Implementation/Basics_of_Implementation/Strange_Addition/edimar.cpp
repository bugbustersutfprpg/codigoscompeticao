#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t, carry, i;
    string x, y, s;
    
    cin >> t;
    
    while(t--){
        cin >> x >> y;
        
        for (i = x.length()-1; i > 0 && x[i] == '0'; i--)
            x.pop_back();
        for (i = y.length()-1; i > 0 && y[i] == '0'; i--)
            y.pop_back();
            
        if (x.length() > y.length())
            swap(x, y);
            
        carry = 0;
        s = "";
        
        for (i = 0; i < x.length(); i++){
        
            s += x[i] + y[i]-'0' + carry;
            if (s[i] > '9'){
                s[i] -= 10;
                carry = 1;
            }else
                carry = 0;
        }
        
        for (i = x.length(); i < y.length(); i++){
            s += y[i] + carry;
            if (s[i] > '9'){
                s[i] -= 10;
                carry = 1;
            }else carry = 0;
        }
        if (carry)
            s += '1';
            
        for (i = 0; i < s.length() && s[i] == '0'; i++);
        
        cout << s.substr(i) << endl;
    }
    
}