#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t;
    string n, m;
    
    cin >> t;
    
    while(t--){
        cin >> n >> m;
        
        if (n == m){
            cout << "YES\n";
        }else{
            if (n.length() != 1 || m.length() != 1)
                cout << "NO\n";
            else{
                if (n[0] > m[0])
                    swap(n, m);
                
                if (n[0] == '2' and m[0] == '4')
                    cout << "YES\n";
                else
                    cout << "NO\n";
            }
        }
    }
    
}