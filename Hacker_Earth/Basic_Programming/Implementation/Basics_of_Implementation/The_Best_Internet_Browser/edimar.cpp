#include <bits/stdc++.h>

using namespace std;

bool isvogal(char c){
    return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'; 
}

int main(){
    
    int t;
    string s;
    
    cin >> t;
    
    while(t--){
        cin >> s;
        
        int cont = 3;
        for (int i = 4; i < s.length(); i++){
            if (isvogal(s[i]))
                cont++;
        }
        
        cout << s.length() - cont << "/" << s.length() << endl;
    }
    
}