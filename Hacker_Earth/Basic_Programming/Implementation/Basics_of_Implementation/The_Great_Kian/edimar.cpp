#include <bits/stdc++.h>

using namespace std;
#define T 100100
int v[T];

int main(){
    
    int n;
    cin >> n;
    for (int i= 1; i <= n; i++){
        cin >> v[i];
    }    
    
    long long t1 = 0,t2=0,t3=0;
    for (int i = 1; i <=n; i+=3){
        t1+=v[i];
    }
    for (int i = 2; i <=n; i+=3){
        t2+=v[i];
    }
    for (int i = 3; i <=n; i+=3){
        t3+=v[i];
    }
    cout << t1 << " " << t2 << " " << t3 << endl;
}