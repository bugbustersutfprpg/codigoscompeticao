#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t;
    int va[26], vb[26];
    string a, b;
    
    cin >> t;
    
    while(t--){
        cin >> a >> b;
        
        for (int i = 0; i < 26; i++){
            va[i] = vb[i] = 0;
        }
        
        for (int i = 0; i < a.length(); i++){
            va[a[i]-'a']++;
        }
        for (int i = 0; i < b.length(); i++){
            vb[b[i]-'a']++;
        }
        
        int total = 0;
        for (int i =0; i < 26; i++){
            total += abs(va[i] - vb[i]);
        }
        
        cout << total << endl;
    }
    
}