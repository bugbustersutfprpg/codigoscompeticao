#include <bits/stdc++.h>

using namespace std;

char cipher(char c, int k){

    if (c >= '0' and c <= '9'){
        k %= 10;
        for (int i = 0; i < k; i++){
            c++;
            if (c > '9')
                c = '0';
        }
    }else if (c >= 'A' and c <= 'Z'){
        k %= 26;
        for (int i = 0; i < k; i++){
            c++;
            if (c > 'Z')
                c = 'A';
        }
    }else if (c >= 'a' and c <= 'z'){
        k %= 26;
        for (int i = 0; i < k; i++){
            c++;
            if (c > 'z')
                c = 'a';
        }
    }

    return c;
}

int main(){

    string s;
    int k;
    
    cin >> s >> k;
    
    for (int i = 0; i < s.length(); i++){
        cout << cipher(s[i], k);
    }
    cout << endl;

}
