#include <bits/stdc++.h>
using namespace std;

int main()
{
    int l, r, k;
    
    cin >> l >> r >> k;
    
    if (l % k == 0 and r % k == 0)
        cout << (r - l) / k + 1 << endl;
    else
        cout << ceil((r - l) / (float)k) << endl;
    
    return 0;
}
