#include <iostream>
using namespace std;
#define MOD 1000000007

int main()
{
    long long res, divisao;
    int n, a;
    
    cin >> n;
    
    res = 1;
    for (int i = 0; i < n; i++){
        cin >> a;
        res *= a;
        if (res >= MOD){
            divisao = res / MOD;
            res -= divisao * MOD;
        }
    }
    
    cout << res << endl;
    
    return 0;
}
