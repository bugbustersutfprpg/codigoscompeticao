#include <iostream>
using namespace std;

int main()
{
    int a;
    bool flag = true;
    
    cin >> a;
    do {
        if (a == 42)
            return 0;
        
        if (flag)
            cout << a << endl;
            
        cin >> a;
    }while(!feof(stdin));
    
    return 0;
}
