#include <bits/stdc++.h>

using namespace std;;
#define T 1000
bool primo[T];
int near[T];

void primos(int n){

    for (int i = 2; i < 50; i++){
        if (!primo[i]){
            for (int j = i*2; j < T; j += i){
                primo[j] = 1;
            }
        }
    }
    for (int i = (int)'Z'+1; i < (int)'a'; i++)
        primo[i] = 1;
}

int distancia(int c){
    if (c > (int)'z') return (int)'q';
    if (c < (int)'A') return (int)'C';
    int i, menor, maior;

    for (i = c; i >= 0 and primo[i]; i--){}
    menor = c - i;

    for (i = c; i < T and primo[i]; i++){}
    maior = i - c;

    int letterA = c - menor, letterB = maior + c;
    if (letterB > 'z' or (letterB > 'Z' and letterB < 'a')) maior = T;
    if (letterA < 'A' or (letterA < 'a' and letterA > 'Z')) menor = T;

    if (menor <= maior)
        return letterA;
    return letterB;
}

void nearest(int n){

    for (int i = 1; i < n; i++){
        //if (primo[i]){
            near[i] = distancia(i);
    }
}

int main(){

    char c;
    int n, t;

    cin >> t;
    primos(T);
    nearest(T);
    /*
    for (int i = 1; i < T; i++){
        if (!primo[i])
            cout << i << " ";
    }
    cout << endl;
    
    for (int i = 10; i <= 127; i++){
        cout << (char)i << ":" << (char)near[i] << " ";
    }
    cout << endl;
    */

    while(t--){
        cin >> n;

        while(n--){
            cin >> c;

            cout << (char)near[c];
        }

        cout << endl;
    }

}
