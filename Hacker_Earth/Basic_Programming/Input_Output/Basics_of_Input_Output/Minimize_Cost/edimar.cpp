#include <bits/stdc++.h>

using namespace std;
#define T 100100
int arr[T];

int main(){

    int n, k;

    cin >> n >> k;

    for (int i = 0; i < n; i++){
        cin >> arr[i];
    }

    int j = 0;
    for (int i = 0; i < n; i++){
        if (arr[i] > 0){
            while(i - j > k) j++;

            while(arr[i] && k >= abs(i-j)){
                if (arr[j] < 0){
                    int x = min(arr[i], -arr[j]);
                    arr[i] -= x;
                    arr[j] += x;
                }else
                    j++;
            }
        }
    }

    long long ans = 0;
    for (int i = 0; i < n; i++){
        ans += abs(arr[i]);
    }

    cout << ans << endl;
}
