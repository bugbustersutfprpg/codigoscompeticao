#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n, q, l, r, x;
    vector<int> v;
    vector<long long> soma;
    
    cin >> n >> q;
    v.push_back(0);
    soma.push_back(0);
    
    for (int i = 0; i < n; i++){
        cin >> x;
        v.push_back(x);
        soma.push_back(soma[i] + x);
    }
    
    for (int i = 0; i < q; i++){
        cin >> l >> r;
        
        cout << (soma[r]-soma[l-1]) / (r-l+1) << endl;
    }
    
}