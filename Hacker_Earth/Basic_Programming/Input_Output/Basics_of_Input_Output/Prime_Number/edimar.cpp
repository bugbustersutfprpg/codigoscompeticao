#include <bits/stdc++.h>

using namespace std;
bool v[1001];

int main(){
    
    int n;
    
    cin >> n;
    
    if (n == 1) return 0;
    
    for (int i = 3; i < 32; i+=2){
        if (v[i] == 0){
            for (int j = i*2; j < 1001; j += i){
                v[j] = 1;
            }
        }
    }
    
    cout << "2";
    for (int i = 3; i < n; i+= 2){
        if (v[i] == 0)
            cout << " " << i;
    }
    cout << endl;
}