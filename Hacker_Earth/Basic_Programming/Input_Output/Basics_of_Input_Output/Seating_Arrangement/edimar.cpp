#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int n, t;
    int v[12];
    string s[12];
    
    s[0] = s[1] = s[6] = s[7] = "WS";
    s[2] = s[11] = s[5] = s[8] = "MS";
    s[3] = s[9] = s[4] = s[10] = "AS";
    
    v[1] = 12; v[0] = 1;
    v[2] = 11; v[11] = 2;
    v[3] = 10; v[10] = 3;
    v[4] = 9; v[9] = 4;
    v[5] = 8; v[8] = 5;
    v[6] = 7; v[7] = 6;
    
    cin >> t;
    
    while(t--){
        cin >> n;
        
        int mod = n%12;
        if (mod == 0) n--;
        
        cout << v[mod] + (12*(int)(n/12)) << " " << s[mod] << endl;
    }
    
}