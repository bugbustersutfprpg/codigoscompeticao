#include <bits/stdc++.h>

using namespace std;

int main(){
    
    int t;
    string s1, s2;
    int va[26], vb[26];
    
    cin >> t;
    
    while(t--){
        cin >> s1 >> s2;
        
        for (int i = 0; i < 26; i++)
            va[i] = vb[i] = 0;
            
        if (s1.length() == s2.length()){
            for (int i = 0; i < s1.length(); i++){
                va[s1[i]-'a']++;
                vb[s2[i]-'a']++;
            }
            
            for (int i = 0; i < 26; i++){
                if (va[i] - vb[i] != 0){
                    goto jump;
                }
            }
            cout << "YES\n";
        }else{
            jump:
            cout << "NO\n";
        }
    }
}