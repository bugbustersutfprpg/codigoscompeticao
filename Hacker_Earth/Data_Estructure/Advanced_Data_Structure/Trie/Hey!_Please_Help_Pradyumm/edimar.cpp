#include <bits/stdc++.h>

using namespace std;
#define T 26

struct Trie{
    bool end{};
    Trie *prox[T]{};
};
Trie *trie = new Trie;

void insert(string s){

    int k;
    Trie *t = trie;
    for (int i = 0; i < s.length(); i++){
        k = (s[i] < 'a') ? s[i] - 'A' : s[i] - 'a';
        if (!t->prox[k])
            t->prox[k] = new Trie;
        t = t->prox[k];
    }
    t->end = 1;
}

void sufix(Trie *t, string s){

    if (t->end)
        cout << s << endl;

    for (int i = 0; i < T; i++){
        if (t->prox[i]){
            sufix(t->prox[i], s + (char)(i+'a'));
        }
    }
}

void prefix(Trie *t, string s){

    int k;
    for (int i = 0; i < s.length(); i++){
        k = s[i] - 'a';
        if (!t->prox[k]){
            puts("No suggestions");
            insert(s);
            return;
        }
        t = t->prox[k];
    }

    sufix(t, s);
}

int main(){

    int n, q;
    string s;
    
    cin >> n;
    for (int i = 0; i < n; i++){
        cin >> s;
        insert(s);
    }

    cin >> q;
    for (int i = 0; i < q; i++){
        cin >> s;
        prefix(trie, s);
    }
}
