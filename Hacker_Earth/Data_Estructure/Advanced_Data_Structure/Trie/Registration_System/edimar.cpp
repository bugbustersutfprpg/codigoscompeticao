#include <bits/stdc++.h>

using namespace std;
#define T 36

typedef struct s{
    int cont;
    bool end;
    struct s* prox[T];
}Trie;

void insert(Trie *t, string s, bool flag);

bool isPossible(Trie *t, string s){

    int k;
    for (int i = 0; i < s.length(); i++){
        k = s[i] - '0';

        if (t->prox[k] == NULL) return true;
        else t = t->prox[k];
    }

    if (t->end == false) return true;
    return false;
}

string addNumber(Trie *t){

    string s;
    do {
        s = to_string(++(t->cont));
    }while(!isPossible(t, s));

    insert(t, s, 0);
    return s;
}

void insert(Trie *t, string s, bool flag){

    int k;
    for (int i = 0; i < s.length(); i++){
        if (s[i] <= '9')
            k = s[i] - '0';
        else
            k = s[i] - 'a' + 10;

        if (t->prox[k] == NULL){
            t->prox[k] = (Trie*)malloc(sizeof(Trie));
            t = t->prox[k];
            for (int i = 0;  i < T; i++) t->prox[i] = NULL;
            t->cont = -1;
            t->end = 0;
        }else
            t = t->prox[k];
    }

    if (t->end == 0){
        t->end = 1;
    }else{
        s += addNumber(t);
    }

    if (flag)
        cout << s << endl;
}

int main(){

    int n, k;
    string s;
    Trie *t = (Trie*)malloc(sizeof(Trie));
    t->end = false;
    for (int i = 0; i < T; i++) t->prox[i] = NULL;

    cin >> n;

    for (int i = 0; i < n; i++){
        cin >> s;
        insert(t, s, 1);
    }
}
