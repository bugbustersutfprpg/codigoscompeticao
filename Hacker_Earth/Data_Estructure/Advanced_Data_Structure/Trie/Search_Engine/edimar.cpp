#include <bits/stdc++.h>

using namespace std;

struct Trie{
    Trie *letra[26];
    int peso;
};


Trie *newNode(){

    Trie *aux = (Trie*)malloc(sizeof(Trie));
    if (!aux){
        printf("Err");
        exit(0);
    }
    for (int i = 0; i < 26; i++){
        aux->letra[i] = NULL;
    }
    aux->peso = 0;
    return aux;
}

int inserir(Trie *trie){

    int x;
    char c;
    scanf("%c", &c);
    if (c == ' ') {
        scanf("%d", &x);
        scanf("%c", &c);
    }else{
        x = c - 'a';
        if (trie->letra[x] == NULL)
            trie->letra[x] = newNode();
        x = inserir(trie->letra[x]);
    }

    //printf("Cabou ");
    return trie->peso = max(trie->peso, x);
}

void busca(Trie *trie){

    char c;
    int x;

    scanf("%c", &c);
    while(!feof(stdin) && c != '\n'){
        x = c - 'a';
        if (trie->letra[x] == NULL){
            puts("-1");
            while(!feof(stdin) && c != '\n')
                scanf("%c", &c);
            return;
        }
        trie = trie->letra[x];
        scanf("%c", &c);
    }

    printf("%d\n", trie->peso);
}

int main(){

    int n, q, x, peso;
    Trie *trie = newNode();
    char c;

    cin >> n >> q;

    scanf("%c", &c);
    for (int i = 0; i < n; i++){
        inserir(trie);
    }

    for (int i = 0; i < q; i++){
        busca(trie);
    }
}
