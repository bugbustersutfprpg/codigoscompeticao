#include <bits/stdc++.h>
using namespace std;

typedef struct no{
    char chave;
    int weight;
    struct no *prox[26];
}Trie;

Trie* insert(Trie *trie, string s, int j, int t){
    if (j == s.length()) {
        return NULL;
    }
    if (trie == NULL){
        trie = (Trie*)malloc(sizeof(Trie));
        for (int i = 0; i < 26; i++) trie->prox[i] = NULL;
        trie->chave = s[j];
        trie->weight = 0;
	//cout << "Criou:" << s[j] << endl;
    }

    j++;
    trie->prox[s[j] - 'a'] = insert(trie->prox[s[j] - 'a'], s, j, t);
    if (t > trie->weight)
        trie->weight = t;
    
    return trie;
}

int maxSufix(Trie *trie, string s, int j){
    
    if (trie == NULL) return -1;
    
    if (j+1 == s.length()){
        return trie->weight;
    }else{
        return maxSufix(trie->prox[s[j+1] - 'a'], s, j+1);
    }
}

int main(){
    
    int n, q, t;
    string s;
    Trie trie;

    trie.chave = '0';
    for (int i = 0; i < 26; i++){
        trie.prox[i] = NULL;
    }
    
    cin >> n >> q;
    
    for (int i = 0; i < n; i++){
        cin >> s >> t;
        
        trie.prox[s[0] - 'a'] = insert(trie.prox[s[0] - 'a'], s, 0, t);
    }
    
    for (int i = 0; i < q; i++){
        cin >> s;
        cout << maxSufix(trie.prox[s[0] - 'a'], s, 0) << endl;
    }
}
