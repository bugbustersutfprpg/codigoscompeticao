#include <bits/stdc++.h>

using namespace std;
#define T 100100

int main(){
    
    int n, x, q;
    string s[T], ss;
    
    cin >> n;
    
    for (int i = 0; i < n; i++){
        cin >> x >> ss;
        s[x] = ss;
    }
    
    cin >> q;
    
    for (int i = 0; i < q; i++){
        cin >> x;
        cout << s[x] << endl;
    }
}