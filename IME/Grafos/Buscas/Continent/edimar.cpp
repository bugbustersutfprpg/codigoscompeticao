#include <bits/stdc++.h>
using namespace std;
char land[21][21], C;
int n, m, x, y, mijid, l[4] = {0,0,1,-1}, c[4] = {1,-1,0,0};

bool isValid(int i, int j){
    return i >= 0 && j >= 0 && i < n && j < m && land[i][j] == C;
}

int dfs(int i, int j){

    land[i][j] = 0;
    if (i == x && j == y) mijid = 1;

    int total = 1;
    for (int k = 0; k < 4; k++){
        int li = i + l[k];
        int ci = j + c[k];
        if (isValid(li, ci) || (ci == m && land[li][0] == C) || (ci == -1 && land[li][m-1] == C)){
            if (!isValid(li, ci)){
                if (ci == m) ci = 0;
                else ci = m-1;
            }
            int aux = dfs(li, ci);
            if (aux == -1) return -1;
            total += aux;
        }
    }
    return total;
}


int main(){
    cin >> n >> m;
    do{
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                cin >> land[i][j];
            }
        }
        cin >> x >> y;
        C = land[x][y];
        int aux, maior = 0;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                if (land[i][j] == C){
                    aux = dfs(i, j);
                    if (mijid)
                        mijid = 0;
                    else
                        maior = max(maior, aux);
                }
            }
        }
        cout << maior << endl;
    }while(scanf(" %d %d", &n, &m) == 2);
}
