#include <bits/stdc++.h>
using namespace std;
#define F first
#define S second
#define T 1000010
typedef long long ll;
typedef pair<int, int> ii;

int f, s, g, u, d;
queue<ii> q;
bool visit[T];

long long bfs(){

    q.push(ii(s, 0));
    visit[s] = 1;

    while(q.size()){
        ii x = q.front();
        q.pop();
        if (x.F == g) return x.S;

        int low = x.F - d;
        if (low < 1) low = s;
        if (!visit[low]){
            visit[low] = 1;
            q.push(ii(low, x.S+1));
        }
        int high = x.F + u;
        if (high > f) high = s;
        if (!visit[high]){
            visit[high] = 1;
            q.push(ii(high, x.S+1));
        }
    }
    puts("use the stairs");
    exit(0);
}

int main(){

    cin >>f>>s>>g>>u>>d;

    cout << bfs() << endl;
}
