#include <bits/stdc++.h>
using namespace std;
#define T 100010
#define pb push_back

vector<int> adj[T];
int n, cont, alt[T];
bool visit[T];

bool dfs(int x){

    visit[x] = 1;
    for (int e : adj[x]){
        if (!visit[e]){
            alt[e] = alt[x]+1;
            if (int a = dfs(e) == 0) return 0;
        }else if ( abs(alt[e] - alt[x]) > 1)
            return 0;
    }
    cont++;
    return 1;
}

int main(){

    int m, a, b;
    cin >> n>>m;

    for (int i = 0; i < m; i++){
        cin >> a>> b;
        adj[a].pb(b);
        adj[b].pb(a);
    }

    alt[a] = 1;
    if (dfs(a) && cont == n)
        puts("YES");
    else
        puts("NO");
}
