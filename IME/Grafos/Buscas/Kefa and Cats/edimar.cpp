#include <bits/stdc++.h>
using namespace std;
#define T 100100
#define pb push_back

vector<int> adj[T];
int total, m;
bool visit[T], cat[T];

void dfs(int x, int alt){

    visit[x] = 1;
    if (cat[x]){
        if (++alt > m) return;
    }else
        alt = 0;
    if (adj[x].size() == 1 && x != 1)
        total++;

    for (int v : adj[x]){
        if (!visit[v]){
            dfs(v, alt);
        }
    }
}

int main(){

    int n, a, b;
    cin >> n >> m;

    for (int i = 1; i <= n; i++ ){
        cin >> cat[i];
    }
    for (int i = 1; i < n; i++){
        cin >> a >> b;
        adj[a].pb(b);
        adj[b].pb(a);
    }

    dfs(1, 0);
    cout << total << endl;
}
