#include <bits/stdc++.h>
using namespace std;
#define T 51
#define pb push_back

int n, m, k, total, mar, tempo = 1, visit[T][T];
char ber[T][T];
int l[4] = {0,0,1,-1}, c[4] = {1,-1,0,0};

vector<int> lake;

bool isMar(int x, int y){
    return x == 0 || x == n-1 || y == 0 || y == m-1;
}

bool isValid(int x, int y){
    return x >= 0 && x < n && y >= 0 && y < m && ber[x][y] == '.';
}

int dfs(int x, int y, char d){  //mede o tamanho do lago

    if (isMar(x, y)) mar = 1;

    visit[x][y] = tempo;
    ber[x][y] = d;
    int qtd = 1;
    for (int i = 0; i < 4; i++){
        int li = x + l[i];
        int ci = y + c[i];
        if (isValid(li, ci) && visit[li][ci] != tempo){
            qtd += dfs(li, ci, d);
        }
    }
    return qtd;
}

int main(){

    cin>>n>>m>>k;

    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
            cin >> ber[i][j];
        }
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
            if (ber[i][j] == '.' && !visit[i][j]){
                mar = 0;
                int a = dfs(i, j, '.');
                if (!mar && a){
                    total += a;
                    lake.pb(a);
                }
            }
        }
    }
    sort(lake.begin(), lake.end());
    int res = 0, res2 = -1;
    for (int i = 0; i < (lake.size() - k); i++){
        res += lake[i];
        res2 = i;
    }
    cout << res << endl;

    if (res2 != -1){
        tempo = 2;
        for (int i = 0; i < n; i++){
            for (int j = 0; j < m; j++){
                if (ber[i][j] == '.' && visit[i][j] < 2){
                    //conta o tamanho deste lago e salva em a
                    mar = 0;
                    int a = dfs(i, j, '.');
                    if (a <= lake[res2] && !mar){   //verifica se o tamanho do lago est� entre os menores e se n�o � mar
                        //preenche o lago com terra
                        tempo++;
                        dfs(i, j, '*');
                        if (--res2 == -1) break;
                        //retira do vector o lago preenchido
                        for (int ii = 0; ii < lake.size(); ii++){
                            if ((lake[ii] == a)){
                                lake.erase(lake.begin()+ii);
                                break;
                            }
                        }
                    }
                }
            }
            if (res2 == -1) break;
        }
    }
    //imprime a resposta
    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
            cout << ber[i][j];
        }
        puts("");
    }
}
