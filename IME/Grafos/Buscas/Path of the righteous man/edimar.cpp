#include <bits/stdc++.h>
using namespace std;
#define T 51
int mat[T][T], x1, y11, x2, y2, n, t, l[4] = {0, 0, 1, -1}, c[4] = {1, -1, 0, 0};

bool isValid(int x, int y){
    return x >= 0 && x < n && y >= 0 && y < n;
}

bool dfs(int b, int x, int y, vector<vector<bool> > &visit){

    if (x == x2 && y == y2) return true;

    visit[x][y] = 1;
    for (int i = 0; i < 4; i++){
        int li = x + l[i];
        int ci = y + c[i];
        if (isValid(li, ci) && (b & (1<<mat[li][ci]) ) && !visit[li][ci]){
            if (dfs(b, li, ci, visit)) return true;
        }
    }
    return false;
}

int bfs(){

    bool filado[1024];
    fill_n(filado, 1024, 0);
    queue<int> fila;
    int b = 1<<mat[x1][y11];
    fila.push(b);
    filado[b] = 1;

    while(fila.size()){
        b = fila.front();
        fila.pop();

        vector<vector<bool> > visit (T, vector<bool> (T, 0));
        if (dfs(b, x1, y11, visit)){
            int cont = 0;
            for (int i = 0; i < 10; i++)
                if (b & (1<<i) ) cont++;
            return cont;
        }
        for (int i = 0; i < 10; i++){
            int a = b + (1 << i);
            if ( !(b & (1<<i) ) && !filado[a]){
                fila.push(a);
                filado[a] = 1;
            }
        }
    }
    return -1;
}

int main(){

    cin >>t;
    while(t--){
        cin >> n;
        for (int i = 0; i < n; i++){
            for (int j=0; j < n; j++){
                cin >> mat[i][j];
            }
        }
        cin >> x1 >> y11 >> x2 >> y2;
        cout << bfs() << endl;
    }
}
