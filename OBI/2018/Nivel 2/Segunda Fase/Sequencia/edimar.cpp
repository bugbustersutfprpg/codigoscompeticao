#include <bits/stdc++.h>
using namespace std;
#define MAXN 100010
#define MAXH 21

int N, L, H;
int v[MAXN];
bool cor[MAXN];
int pd[MAXN][MAXH];

int main(){

    cin >> N >> L >> H;

    for (int i = 0; i < N; i++) cin >> v[i];
    for (int i = 0; i < N; i++) cin >> cor[i];

    for (int i = 0; i < MAXN; i++)
        for (int j = 0; j < MAXH; j++)
            pd[i][j] = INT_MIN;

    if (cor[0] == 1){
        pd[0][0] = 0;
        pd[0][1] = v[0];
    }else{
        pd[0][0] = max(0, v[0]);
    }

    int resp = INT_MIN;
    if ((L==0 && cor[0] == 0) || (L<=1 && cor[0] == 1 && H > 0)) resp = v[0];

    for (int i = 1; i < N; i++){
        for (int j = 0; j <= H; j++){
            if (cor[i] == 1){
                if (j == 0){
                    pd[i][j] = 0;
                }else{
                    if (pd[i-1][j-1] != INT_MIN){
                        pd[i][j] = pd[i-1][j-1] + v[i];
                    }
                }
            }else{
                if (j == 0){
                    pd[i][j] = max(0, pd[i-1][0] + v[i]);
                }else{
                    if (pd[i-1][j] != INT_MIN){
                        pd[i][j] = pd[i-1][j] + v[i];
                    }
                }
            }
            if (j >= L && j <= H){
                resp = max(resp, pd[i][j]);
            }
        }
    }

    cout << resp << endl;
}
