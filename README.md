### Para que ser este repositório? ###

* Este repositório é destinado a salvar arquivos que resolvem problemas de competição, disponibilizando códigos para que seja possível
* tirar dúvidas, consultas rápidas, etc.

### Do que preciso? ###

* Para testar os códigos basta usar as ferramentas do site e navegar entre os códigos fontes.
* Para baixar todos os códigos, use o comando:



```
#!Shell script

git clone https://SeuUsuario@bitbucket.org/bugbustersutfprpg/codigoscompeticao.git
```





### Como contribuir? ###

* Ainda será testado como será possível fazer contribuições, aguarde.

### Com quem posso conversar? ###

* Converse com um dos alunos que ministram o curso de Programação Competitiva na UTFPR PG ou com o professor responsável.