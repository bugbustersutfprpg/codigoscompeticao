#include <stdio.h>

int main(){

    int caso, n, k, i, j, a=0;
    scanf("%d", &caso);

    while(a++<caso){
        scanf("%d %d", &n, &k);
        n++;
        int vetor[n];

        for (i=1; i<n; i++){
            vetor[i] = i;
        }

        for (i=k ;n>2 ;i+=k){
            if (i>--n) {
                i -= n;
                while(i>n){
                    i -= n;
                }
            }

            for (j=i--; j<n; ){
                vetor[j++]= vetor[j];
            }
        }
        printf("Case %d: %d\n", a, vetor[1]);
    }

    return 0;
}
