#include <stdio.h>
#include <stdlib.h>
#include <math.h>


typedef struct no{
    int n;
    struct no *prox;

}NO;

NO* darVida(int n){

    NO* volta, *aux, *primeiro;
    int num = 1;

    primeiro = (NO*)malloc(sizeof(NO));
    volta = primeiro;
    volta->n = num++;

    while(--n){
        aux = (NO*)malloc(sizeof(NO));
        aux->n = num++;
        volta->prox = aux;
        volta = aux;
    }

    volta->prox = primeiro;
    return primeiro;
}


void listarPrimos(int primo[]){

    int i = 3500, j = 3, k = 1, l, yes, raiz;
    primo[0] = 2;
    while(i--){
        primo[k++] = j;

        while(j += 2){
            l = 1;
            yes = 0;
            raiz = pow(j, .5);

            while(raiz >= primo[l]){
                if (primo[l] == 0){
                    break;
                }
                if (j%primo[l++] == 0){
                    yes = 1;
                    break;
                }
            }
            if (yes == 0){
                break;
            }
        }
    }

}

//################################################################################################
//################################################################################################


void main(){

    int n, primo, primoAnt, pessoas, i, proxPrimo[3500], cont;
    NO* volta, *aux, *start;

    int j = 3500;
    i = 0;
    while(j--)
        proxPrimo[i++] = 0;
    listarPrimos(proxPrimo);

    scanf("%d", &n);

    while(n){

        j = 0;
        i = 1;
        primo = 2;
        volta = darVida(n);

        while(--n){
            if (j){
                j--;
                primo = primo%j;
                while(--primo){
                    aux = volta;
                    volta = volta->prox;
                }
            }else{
                cont = 0;
                start = volta;
                while(--primo){
                    aux = volta;
                    volta = volta->prox;
                    cont++;
                    if (start == volta){
                        primo = primo%cont;
                        j = cont;
                    }
                }
            }
            aux->prox = volta->prox;
            //printf("Primo= %d   Elimina %d\n", proxPrimo[i-1], volta->n);
            free(volta);
            volta = aux->prox;

            primo = proxPrimo[i++];
        }

        printf("%d\n", volta->n);
        scanf("%d", &n);
    }

}
