#include <stdio.h>


void fusao (int x[], int p, int meio, int u){

    int es=p, di=meio+1, i;
    int copia[u+1];
    for (i=p; i<=u; i++)
        copia[i] = x[i];

    for (i=p; i<=u; i++){
        if (es>meio) {
            x[i] = copia[di++];
        }else if (di>u) {
            x[i] = copia[es++];
        }else if (copia[es] < copia[di]) {
            x[i] = copia[es++];
        }else {
            x[i] = copia[di++];
        }
    }
}

void ordenacaoMerge(int x[], int p, int u){
	if (p>=u) return;

	int meio = (u+p)/2;
	ordenacaoMerge (x, p, meio);
	ordenacaoMerge (x, meio+1, u);
	fusao (x, p, meio, u);
}


int contarTabuas (int vetor[], int m, int n, int l, int k, int necessario){

    int total = 0;
    int i, j, tam = 0;
    for (i=0; i<necessario; i++){
        k--;
        while (vetor[k]==0) if (k--==0) return 0;
        tam = vetor[k];
        while (1){
            j = k-1;
            if (j<0) return 0;
            int dif = n - tam;
            while (vetor[j] != dif) {
                if (j==0) {
                    break;
                }
                j--;
            }
            if (vetor[j] == dif){
                total += 2;
                vetor[j] = 0;
                break;
            }
            else{
                while ( (vetor[k] == 0) || (vetor[k] == tam) ){
                    if (k==0) {
                        return 0;
                    }
                    k--;
                }
                tam = vetor[k];
            }
        }
    }
    return total;
}


int main(){

	int m, n, l, k, i, j, c, total, v, necessario, necessari;

	while(1){

        total = 0 ;
        v = 0;
        c = 0;
        j = 0;
		scanf("%d %d", &m, &n);
		if (m==0 && n==0) break;
		scanf("%d %d", &l, &k);
		int x[k], vetor[k];

		for (i=0; i<k; i++){
			scanf("%d", &x[j]);
			if (x[j] > m){
			}else if (x[j]==m){
                total++;
			}else vetor[c++] = x[j];

			if (x[j] > n){
			}else if (x[j]==n) {
			    v++;
			}else  j++;
		}

		if ( ((n*100)%l)==0) {
            necessario = (n*100/l);
            if (total >= necessario){
                printf("%d\n", necessario);
                continue;
            }
		}
		necessario -= total;
		if ( ((m*100)%l)==0) {
            necessari = (m*100/l);
            if (v >= necessari){
                printf("%d\n", necessari);
                continue;
            }
		}
		necessari -= v;

		//ordenacaoMerge(vetor, 0, c-1);
		//ordenacaoMerge(x, 0, j-1);

        int vo=0, to=0;
        if ( ((m*100)%l==0) && ((necessario*2) <= c) ) {
            to += contarTabuas (vetor, n, m, l, c, necessario);
		}
		if (to != 0) total += to;
        else total = 0;

		if ( ((n*100)%l==0) && (n!=m) && ((necessari*2) <= j) ){
            vo += contarTabuas (x, m, n, l, j, necessari);
		}
        if (vo != 0) v += vo;
        else v = 0;

        if ( (total == 0) && (v == 0) ){
            puts ("impossivel");
            continue;
        }
        if (total == 0) {
            total = v;
        }else if (v == 0) {
        }else if (total > v) total = v;

		printf("%d\n", total);
	}
	return 0;
}
