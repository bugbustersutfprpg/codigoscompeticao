#include <stdio.h>


void main(){

    int n, pico1, pico2, pico3, cont, aux, sobe, primeiro, segundo;

    scanf("%d", &n);
    while(n){

        sobe = 0;
        cont = 1;
        scanf("%d", &pico1);
        if (--n){
            scanf("%d", &pico2);

            cont++;
            if (pico2>pico1){
                sobe = 1;
            }
            primeiro = pico1;
            segundo = pico2;
            pico1 = pico2;

            while(--n){
                scanf("%d", &pico2);

                if (pico2>pico1){
                    if (!sobe){
                        cont++;
                        sobe = 1;
                    }
                }else{
                    if (sobe){
                        sobe = 0;
                        cont++;
                    }
                }
                pico1 = pico2;
            }

            pico2 = primeiro;
            if (pico2>pico1){
                if (sobe){
                    cont--;
                }else{
                    sobe = 1;
                }
            }else{
                if (!sobe){
                    cont--;
                }else{
                    sobe = 0;
                }
            }
            pico1 = pico2;
            pico2 = segundo;
            if (pico2>pico1){
                if (sobe)
                    cont--;
            }else{
                if (!sobe)
                    cont--;
            }
        }
        printf("%d\n", cont);
        scanf("%d", &n);
    }
}
