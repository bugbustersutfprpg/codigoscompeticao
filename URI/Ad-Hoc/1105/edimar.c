#include <stdio.h>


void main(){

    int b, d, i, n, c, v, r[21];

    while(1){
        scanf("%d %d", &b, &n);

        if (!b) break;

        i = 1;
        while(b--){
            scanf("%d", &r[i++]);
        }

        while(n--){
            scanf("%d %d %d", &d, &c, &v);
            r[d] -= v;
            r[c] += v;
        }

        while(--i){
            if (r[i] < 0){
                puts("N");
                break;
            }
        }

        if (!i)
            puts("S");
    }
}
