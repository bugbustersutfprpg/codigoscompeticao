#include <stdio.h>

void main(){

    int h, l, c, c1, vezes, sobe, extra;

    while(1){

        scanf("%d %d", &h, &l);
        if (!h) break;

        scanf("%d", &c);
        vezes = h-c;
        sobe = 0;
        extra = 0;

        while(--l){
            scanf("%d", &c1);

            if (sobe && c > c1)
                extra += vezes + c - h;

            if (c < c1)
                sobe = 1;
            else if (c > c1)
                sobe = 0;

            if (!sobe)
                vezes = h-c1;
            c = c1;
        }

        printf("%d\n", vezes + extra);
    }
}
