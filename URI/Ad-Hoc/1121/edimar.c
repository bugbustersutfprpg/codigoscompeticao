#include <stdio.h>


void main(){

    int M, N, S;
    int i, j, ir, jr, sentido, bandeira;
    char comando, matriz[100][100];

    while(1){

        scanf("%d %d %d", &N, &M, &S);
        if (!M) break;

        bandeira = 0;
        i = 0;
        while(i < N){
            j = 0;
            while(j < M){
                scanf(" %c", &matriz[i][j]);
                if (matriz[i][j] > 60){
                    switch (matriz[i][j]){
                        case 'N':
                            sentido = 1;
                            break;
                        case 'L':
                            sentido = 2;
                            break;
                        case 'S':
                            sentido = 3;
                            break;
                        case 'O':
                            sentido = 4;
                            break;
                    }
                    ir = i;
                    jr = j;
                }
                j++;
            }
            i++;
        }
        i = ir;
        j = jr;

        while(S--){
            scanf(" %c", &comando);

            switch (comando){
                case 'D':
                    if (++sentido == 5) sentido = 1;
                    break;
                case 'E':
                    if (!--sentido) sentido = 4;
                    break;
                case 'F':
                    switch (sentido){
                        case 1:
                            i--;
                            if (i < 0 || matriz[i][j] == '#'){
                                i++;
                                break;
                            }
                            if (matriz[i][j] == '*') {
                                bandeira++;
                                matriz[i][j] = '.';
                            }
                            break;
                        case 2:
                            j++;
                            if (j == M || matriz[i][j] == '#'){
                                j--;
                                break;
                            }
                            if (matriz[i][j] == '*'){
                                bandeira++;
                                matriz[i][j] = '.';
                            }
                            break;
                        case 3:
                            i++;
                            if (i == N || matriz[i][j] == '#'){
                                i--;
                                break;
                            }
                            if (matriz[i][j] == '*'){
                                bandeira++;
                                matriz[i][j] = '.';
                            }
                            break;
                        case 4:
                            j--;
                            if (j < 0 || matriz[i][j] == '#'){
                                j++;
                                break;
                            }
                            if (matriz[i][j] == '*'){
                                bandeira++;
                                matriz[i][j] = '.';
                            }
                            break;
                    }
                    break;
            }
        }

        printf("%d\n", bandeira);
    }

}
