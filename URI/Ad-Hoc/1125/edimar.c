#include <stdio.h>


void main(){

    int G, P, colocacao[100][100], i, j, k, S, K, ponto[100], sistPonto[101], vencedor[100], maior, cont;

    while(1){
        scanf("%d %d", &G, &P);
        if (!P) break;

        i = 0;
        j = 0;
        while(i < G){
            j = 0;
            while(j < P){
                scanf("%d", &colocacao[i][j++]);
            }
            i++;
        }

        k = 0;
        scanf("%d", &S);

        while(k++ < S){
            i = 0;
            cont = 0;
            scanf("%d", &K);
            while(i++ < K){
                scanf("%d", &sistPonto[i]);
            }
            i = 0;
            while(i < P){
                ponto[i++] = 0;
            }

            j = 0;
            maior = 0;
            while(j < P){
                i = 0;
                while(i < G){
                    if (colocacao[i][j] <= K){
                        ponto[j] += sistPonto[colocacao[i][j]];
                    }
                    i++;
                }
                if (maior < ponto[j]){
                    maior = ponto[j];
                    cont = 0;
                    vencedor[cont] = j + 1;
                }else if (maior == ponto[j]){
                    vencedor[++cont] = j + 1;
                }
                j++;
            }

            i = 0;
            while(i < cont)
                printf("%d ", vencedor[i++]);
            printf("%d\n", vencedor[i]);
        }
    }
}
