#include <stdio.h>


void main(){

    int n, i, a, resposta;
    char aluno;

    while(1){
        scanf("%d", &n);
        if (!n) break;

        while(n--){
            i = 0;
            aluno = '*';
            resposta = 0;
            while(i < 5){
                scanf("%d", &a);

                if (a < 128){
                    if (!resposta){
                        aluno = i + 65;
                        resposta = 1;
                    }else{
                        aluno = '*';
                    }
                }
                i++;
            }
            
            printf("%c\n", aluno);
        }
    }
}
