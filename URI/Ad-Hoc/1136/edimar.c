#include <stdio.h>


void main(){

    int n, b, i, j, aux, bola[90], sobe, desce, alvo, diferenca, achou;
    char resposta;

    while(1){
        scanf("%d %d", &n, &b);
        if (!n) break;

        i = 0;
        scanf("%d", &bola[i]);

        while(--b){
            scanf("%d", &bola[++i]);
            j = i;
            while(bola[j] < bola[j-1]){
                aux = bola[j];
                bola[j] = bola[j-1];
                bola[j-1] = aux;
                if (!--j) break;
            }
        }
        if (bola[0] != 0 || bola[i] != n){
            puts("N");
            continue;
        }

        //j=0;
        //while(j <= i)
         //   printf("%d ", bola[j++]);
       // puts("");

        resposta = 'Y';
        alvo = n;
        while(--alvo){
            //printf("%d procurado\n", alvo);
            achou = 0;
            desce = i;
            sobe = 0;
            diferenca = bola[desce] - bola[sobe];
            while(1){
                while(1){
                    if (diferenca < alvo || desce <= sobe){
                        break;
                    }
                    if (diferenca == alvo){
                        //printf("Achou %d na diferenca entre %d e %d. Sobe = %d  e desce = %d\n", alvo, bola[desce], bola[sobe], sobe, desce);
                        achou = 1;
                        break;
                    }
                    diferenca = bola[--desce] - bola[sobe];
                }
                if (achou) break;
                if (++sobe == i){
                    break;
                }
                desce = i;
                diferenca = bola[desce] - bola[sobe];
            }
            if (!achou) {
                resposta = 'N';
                break;
            }
        }
        printf("%c\n", resposta);
    }

}
