#include <stdio.h>


void main(){

    char s[1050], letra, resposta;
    int i;

    while(1){
        scanf(" %[^\n]s", s);
        if (s[0] == '*') break;

        letra = s[0];
        if (letra > 90) letra -= 32;

        i = 0;
        resposta = 'Y';
        while(s[++i] != 0){
            while(s[i] != ' ' && s[i] != 0) i++;
            if (s[i] == ' '){
                if (s[++i] != letra && s[i] != (letra+32) ){
                    resposta = 'N';
                    break;
                }
                continue;
            }
            break;
        }
        printf("%c\n", resposta);
    }
}
