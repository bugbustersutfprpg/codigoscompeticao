#include <stdio.h>


void main(){

    int n, v[2001], i;
    i = 0;
    while(i < 2001)
        v[i++] = 0;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &i);
        v[i]++;
    }

    i = 0;
    while(i < 2001){
        if (v[i])
            printf("%d aparece %d vez(es)\n", i, v[i]);
        i++;
    }

}
