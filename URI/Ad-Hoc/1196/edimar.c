#include <stdio.h>
#include <string.h>


void main(){

    int i, j;
    char c;            //0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123
    char correto[94] = {"                                       ;    M0,.9`12345678 L -    VXSWDFGUHJKNBIO EARYCQZT P]["};

    while(scanf("%c", &c) != -1){
        if (c == '\n')
            puts("");
        else
            printf("%c", correto[c]);
    }
}
