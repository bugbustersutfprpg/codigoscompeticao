#include <stdio.h>


void main(){

    char s[64];
    int cont;
    double media, distancia;

    media = 0;
    cont = 0;
    while(scanf(" %[^\n]s", s) != -1){
        scanf("%lf", &distancia);
        media = media * cont + distancia;
        media /= ++cont;
    }
    printf("%.1lf\n", media);
}
