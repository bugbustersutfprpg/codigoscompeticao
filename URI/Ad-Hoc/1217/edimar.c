#include <stdio.h>
#include <string.h>

void main(){

    int n, tam, i, j, cont[367];
    double preco, mediaPreco, mediaQuant;
    char s[10000];

    j = 1;
    mediaQuant = 0;
    mediaPreco = 0;

    scanf("%d", &n);
    n++;

    while(j < n){
        scanf("%lf", &preco);
        scanf(" %[^\n]s", s);

        tam = strlen(s);

        i = 0;
        cont[j] = 1;
        while(i < tam){
            if (s[i++] == ' ')
                cont[j]++;
        }

        mediaPreco += preco;
        mediaQuant += cont[j];
        j++;
    }

    i = 1;
    while(i < n){
        printf("day %d: %d kg\n", i, cont[i]);
        i++;
    }
    n--;
    printf("%.2lf kg by day\n", mediaQuant / n );
    printf("R$ %.2lf by day\n", mediaPreco / n );
}
