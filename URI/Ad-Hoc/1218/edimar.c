#include <stdio.h>


void main(){

    int cont, f, m, i, acabou;
    char s[3], n[3], c;

    i = 0;
    acabou = 0;

    scanf("%s", n);
    while(1){

        cont = 0;
        m = 0;
        f = 0;
        i++;


        while(1){
            if (scanf(" %s", s) == -1){
                acabou = 1;
                break;
            }

            scanf(" %c", &c);

            if (s[0] == n[0])
                if (s[1] == n[1]){
                    cont++;
                    if (c == 'M')
                        m++;
                    else
                        f++;
                }

            scanf("%c", &c);
            if (c == '\n')
                break;
        }

        printf("Caso %d:\n", i);
        printf("Pares Iguais: %d\n", cont);
        printf("F: %d\n", f);
        printf("M: %d\n", m);

        if (acabou)
            break;

        if (scanf(" %s", n) == -1)
            break;
        puts("");
    }
}
