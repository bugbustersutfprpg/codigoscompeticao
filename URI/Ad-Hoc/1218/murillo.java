import java.io.*;
import java.util.StringTokenizer;
import java.lang.StringBuilder;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		StringBuilder sb = new StringBuilder();
		
		int n;
		int a;
		String b;
		
		String first;
		
		int teste = 1;
		int contagemM;
		int contagemF;
		
		boolean colocarLinha = false;
		
		while((first = br.readLine()) != null){ // se null chegamos no EOF
			a = Integer.parseInt(first);
			
			st = new StringTokenizer(br.readLine());
			contagemM = 0;
			contagemF = 0;
			
			if(colocarLinha)
				sb.append(System.lineSeparator());
			
			while(st.hasMoreTokens()){
			
				n = Integer.parseInt(st.nextToken());
				b = st.nextToken();
				
				if(a == n){
				
					if(b.equals("F"))
						contagemF++;
					else
						contagemM++;
				
				}	
			
			}
			
			sb.append("Caso ").append(teste++).append(":").append(System.lineSeparator())
			.append("Pares Iguais: ").append(contagemF + contagemM)
			.append(System.lineSeparator()).append("F: ").append(contagemF)
			.append(System.lineSeparator()).append("M: ").append(contagemM)
			.append(System.lineSeparator());			
		
			colocarLinha = true;
		}
		
		System.out.print(sb.toString());
	
	}

}
