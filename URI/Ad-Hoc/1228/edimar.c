#include <stdio.h>

int main(){

    int n, i, j, largada[25], chegada[25], cont, piloto;

    while(scanf("%d", &n) != -1){
        cont = 0;
        i = 0;
        while(i<n){
            scanf("%d", &largada[i++]);
        }
        i = 0;
        while(i<n){
            scanf("%d", &chegada[i++]);
        }

        i = 0;
        while(i<n){
            piloto = chegada[i];
            for (j=i; ; j++)
                if (piloto == largada[j])
                    break;
            cont += j-i;
            for ( ; j>i; j-- ){
                largada[j] = largada[j-1];
            }
            largada[j] = piloto;
            i++;
        }
        printf("%d\n", cont);
    }
    return 0;
}
