#include <stdio.h>


void main(){

    int n, tam[31], i, t, cont[31], total;
    char c;

    while(scanf("%d", &n) != -1){

        i = 0;
        while(i < 31){
            tam[i] = 0;
            cont[i++] = 0;
        }
        i = 0;
        while(i < n){
            scanf(" %d", &t);
            scanf(" %c", &c);

            t -= 30;
            cont[t]++;

            if (c == 'D'){
                tam[t]++;
            }else
                tam[t]--;

            i++;
        }

        total = 0;
        i = 0;
        while(i < 31){
            if (tam[i] < 0)
                tam[i] = 0 - tam[i];
            cont[i] = (cont[i] - tam[i]) / 2;
            total += cont[i];
            i++;
        }
        printf("%d\n", total);
    }
}
