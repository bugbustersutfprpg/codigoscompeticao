#include <stdio.h>


void main(){

    int n, tiro, altura[50], i, cont;
    char s[51];

    scanf("%d", &n);
    while(n--){

        scanf("%d", &tiro);

        cont = 0;
        i = 0;
        while(i < tiro){
            scanf("%d", &altura[i++]);
        }
        i = 0;
        scanf(" %s", s);
        while(i < tiro){
            if ( (altura[i] < 3 && s[i] == 'S') || (altura[i] > 2 && s[i] == 'J') ){
                cont++;
            }
            i++;
        }

        printf("%d\n", cont);
    }

}
