#include <iostream>

using namespace std;

int main(){

    int n, i, zero, cont, falta;

    cin >> n;

    while(n){
        falta = 0;
        zero = 0;
        cont = 0;

        cin >> i;
        n--;

        while(!i){
            zero++;
            cin >> i;
            n--;
            if (!n)
                break;
        }
        if (!n)
            if (!i)
                zero+=2;

        while(n--){
            cin >> i;
            if (falta){
                if (!i)
                    cont++;
                falta--;
            }else
                if (!i)
                    falta++;
        }
        if (falta)
            cont += (zero+1)/2;
        else
            cont += zero/2;
        cout << cont << "\n";

        cin >> n;
    }

    return 0;
}
