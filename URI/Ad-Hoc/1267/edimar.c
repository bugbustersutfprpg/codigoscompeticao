#include <stdio.h>



void main(){

    int numAluno, dinner, i, sim;
    char aluno[100], c;

    scanf("%d %d", &numAluno, &dinner);

    while(numAluno){
        i = 0;
        while(i < 100){
            aluno[i++] = '1';
        }

        while(dinner--){
            i = 0;
            while(i < numAluno){
                scanf(" %c", &c);
                if (aluno[i] == '0' || c == '0')
                    aluno[i] = '0';
                i++;
            }
        }

        i = 0;
        sim = 0;
        while(i < numAluno){
            if (aluno[i++] == '1'){
                sim = 1;
                break;
            }
        }
        if (sim)
            puts("yes");
        else
            puts("no");

        scanf("%d %d", &numAluno, &dinner);
    }

}
