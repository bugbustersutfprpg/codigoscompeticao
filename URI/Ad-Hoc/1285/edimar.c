#include <stdio.h>
#include <string.h>


void main(){

    int a, b, cont, t, t1, i, j, igual;
    char s[5];

    while(scanf("%d %d", &a, &b) != -1){
        cont = 0;
        b++;
        for ( ; a<b; a++){
            sprintf(s, "%d", a);
            t = strlen(s);
            t1 = t-1;
            igual = 0;
            for (i=0; i<t1; i++){
                for (j=i+1; j<t; j++){
                    if (s[i] == s[j]){
                        igual = 1;
                        break;
                    }
                }
                if (igual)
                    break;
            }
            if (igual)
                continue;
            cont++;
        }
        printf("%d\n", cont);
    }
}
