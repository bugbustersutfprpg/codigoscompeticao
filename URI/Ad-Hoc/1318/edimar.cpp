#include <iostream>

using namespace std;

int main(){

    int n, m, i, cont, t, bilhete[10001];

    cin >> n >> m;
    while(n){
        cont = 0;
        i = 0;
        while(i < 10001)
            bilhete[i++] = 0;

        i = m;
        while(i--){
            cin >> t;
            bilhete[t]++;
        }

        i = 0;
        while(i++ < n)
            if (bilhete[i] > 1)
                cont++;

        cout << cont << "\n";

        cin >> n >> m;
    }

    return 0;
}
