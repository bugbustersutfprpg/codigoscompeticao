#include <stdio.h>


void main(){


    int n, m, j, a;
    scanf("%d", &n);


    while(n){
        m = 0;
        j = 0;
        while(n--){
            scanf("%d", &a);
            if (a)
                j++;
            else
                m++;
        }

        printf("Mary won %d times and John won %d times\n", m, j);
        scanf("%d", &n);
    }
}
