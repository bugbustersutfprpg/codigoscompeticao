#include <stdio.h>

void main(){

    int a, b, n, A, B;

    scanf("%d", &n);

    while(n){
        A = 0;
        B = 0;
        while(n--){
            scanf("%d %d", &a, &b);
            if (a > b)
                A++;
            else if (b > a)
                B++;
        }
        printf("%d %d\n", A, B);
        scanf("%d", &n);
    }

}
