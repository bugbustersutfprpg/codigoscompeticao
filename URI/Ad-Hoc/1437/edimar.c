#include <stdio.h>


void main(){

    int n, i, d;
    char s[1001];

    scanf("%d", &n);

    while(n){
        d = 0;
        i = 0;
        scanf(" %s", s);

        while(s[i] != 0){
            if (s[i] == 'D')
                d++;
            else
                d--;
            if (d == -1)
                d = 3;
            else if (d == 4)
                d = 0;
            i++;
        }

        if (d == 0)
            puts("N");
        else if (d == 1)
            puts("L");
        else if (d == 2)
            puts("S");
        else
            puts("O");

        scanf("%d", &n);
    }

}
