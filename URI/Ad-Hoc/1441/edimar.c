#include <stdio.h>


void main(){

    int n, maior;

    scanf("%d", &n);

    while(n){
        maior = n;
        while(n != 1){
            if (n%2){
                n = n*3+1;
            }else
                n /= 2;

            if (n > maior)
                maior = n;
        }

        printf("%d\n", maior);
        scanf("%d", &n);
    }

}
