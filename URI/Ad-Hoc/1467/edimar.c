#include <stdio.h>


void main(){

    char a, b, c;

    while(scanf(" %c %c %c", &a, &b, &c) != -1){
        if (a == b && a == c){
            puts("*");
        }else if (a == b && a != c){
            puts("C");
        }else if (a == c && a != b){
            puts("B");
        }else
            puts("A");

    }

}
