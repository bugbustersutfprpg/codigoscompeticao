#include <stdio.h>

void main(){

    int n, r, v[10002], i, d;

    while(scanf("%d %d", &n, &r) != -1) {
        i = 0;
        n++;
        while(i<n){
            v[i++] = 0;
        }

        while(r--){
            scanf("%d", &d);
            v[d] = 1;
        }

        d = 0;
        i = 1;
        while(i < n){
            if (!v[i]){
                d = 1;
                printf("%d ", i);
            }
            i++;
        }
        if (!d)
            puts("*");
        else
            puts("");

    }


}
