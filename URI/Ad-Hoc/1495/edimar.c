#include <stdio.h>


int compara(const void *a, const void *b){
    if (*(int*)a > *(int*)b)
        return 1;
    else if (*(int*)a < *(int*)b)
        return -1;
    else
        return 0;
}

void main(){

    int n, g, i, contra, afavor, partida[100001], ponto;

    while(scanf("%d %d", &n, &g) != -1){
        ponto = 0;
        i = 0;
        while(i < n){
            scanf("%d %d", &afavor, &contra);
            partida[i++] = contra - afavor;
        }
        qsort (partida, n, sizeof(int), compara);

        i = 0;
        while(i < n){
            if (!g)
                break;
            if (partida[i] > -1){
                while(partida[i] > -1){
                    partida[i]--;
                    g--;
                    if (!g)
                        break;
                }
            }
            i++;
        }

        i = 0;
        while(i < n){
            if (partida[i] < 0)
                ponto += 3;
            else if (!partida[i])
                ponto++;
            i++;
        }

        printf("%d\n", ponto);
    }

}
