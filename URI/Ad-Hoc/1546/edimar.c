#include <stdio.h>


void main(){

    int n, k, a;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &k);
        while(k--){
            scanf("%d", &a);
            if (a == 1)
                puts("Rolien");
            else if (a == 2)
                puts("Naej");
            else if (a == 3)
                puts("Elehcim");
            else
                puts("Odranoel");
        }
    }

}
