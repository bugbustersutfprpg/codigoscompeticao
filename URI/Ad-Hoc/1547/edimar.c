#include <stdio.h>
#include <math.h>


void main(){

    int n, a, c, i, chute, resposta, posicao, acertou;

    scanf("%d", &c);

    while(c--){
        scanf("%d %d", &a, &n);
        i = 0;
        acertou = 0;
        resposta = 500;

        while(a--){
            scanf("%d", &chute);
            i++;
            if (chute == n && !acertou){
                posicao = i;
                acertou = 1;
            }else if (!acertou){
                if ( abs(resposta - n) > abs(chute - n) ){
                    resposta = chute;
                    posicao = i;
                }
            }
        }

        printf("%d\n", posicao);
    }

}
