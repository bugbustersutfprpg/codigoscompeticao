/*
    Autor: Edimar Jacob Bauer
    email: edimarjb@hotmail.com

    Problema Espiral retirado da primeira fase da maratona Brasileira de Programação do ano de 2015.
    O problema consiste em, dado um número n que representa o número de linhas de uma matriz quadrada, e b que é a posição
que eu quero achar nessa matriz, retornar a posição de b na matriz, primeiro a linha depois a coluna. Importante, a contagem
das posições é dada em circulos no sentido horário iniciando da posição a(1,1), como se fosse uma espiral que começa no canto
superior esquerdo e termina no seu centro.
    Outro detalhe importante, é que os números dados para teste eram extremamente grandes.
*/


#include <stdio.h>
#include <math.h>

int main(){

	unsigned long long int n, b, i, j=0, k, t=0, aux, l, c, x;

	scanf("%lli %lli", &n, &b);

	if ( (n*n)<b) {     // n é o número de linhas da matriz quadrada e b é a posição que eu quero achar
		puts("Nao existe");
		return 0;
	}
	//if ( ((n*n)==b) && (b%2!=0) ) {
		//printf("%lli %lli\n", n/2+1, n/2+1);
		//return 0;
	//}


	long double ii = (4.0*n - sqrt(16.0 * (long double)( n*n - b) ) ) / 8.0;      //fórmula base para se chegar nessa fórmula resultado
                                                                                            //=>  b < ((n-1)*4)*x - 8(x(x-1)/2), onde x = ii
 	i = ii;                             //o 'ii' vai dar 'exatamente'
	if ( (i < ii) || (i==0) )i++;                    //o 'i' vai ser o número de voltas necessárias para se chegar até a casa 'b'

	t = (n-1)*4*i - 8*( (i*(i-1)) /2);      //o 't' é a somatória do número de voltas dados por 'i'
	aux = n-1 - 2*(i-1);                  //o aux é o tamanho da linha ou coluna da volta em que se encontra o ponto

	for ( ; t>=b ;t-=aux ){
		j++;                            //o 'j' vai dar qual lado se encontra o ponto
	}
	k = b - t;                          //o 'k' vai dar a distância até o ponto
	if (j==1){                          //dependendo do j, existe uma colocação adequada de linha e coluna
		c = i;
		l = n -i+2-k;
	}
	else if (j==2){
		c = n-i+2 - k;
		l = n - i+1;
	}
	else if (j==3){
		c = n-i+1;
		l = i-1 + k;
	}
	else{
		c = i + k-1;
		l = i;
	}
	printf("%lli %lli\n", l, c);

	return 0;
}
