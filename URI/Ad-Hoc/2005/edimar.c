#include <stdio.h>

int compara(const void* a, const void* b){

    if (*(int*)a < *(int*)b)
        return 1;
    else
        return -1;

}

void main(){

    int i, j, n, m, cont, aux, rato[1001], comida[1001];

    while(scanf("%d %d", &n, &m) != -1){

        i = 0;
        while(n--){
            scanf("%d", &rato[i++]);
        }
        j = i;
        qsort(rato, i, sizeof(int), compara);

        i = 0;
        while(m--){
            scanf("%d", &comida[i++]);
        }

        cont = 0;
        m = 0;
        while(1){
            n = 0;
            while(rato[n] > comida[m] ){
                n++;
            }
            if (rato[n] < 1){
                if (!n)
                    break;
                n = 0;
            }

            rato[n] -= comida[m++];

            if (m == i){
                if (rato[n] < 1)
                    cont++;
                break;
            }
            
            aux = rato[n++];
            while(aux < rato[n] && n != j){
                rato[n-1] = rato[n];
                n++;
            }
            n--;
            rato[n] = aux;

            if (rato[n] < 1){
                cont++;
                j--;
            }
        }

        printf("%d\n", cont);
    }
}
