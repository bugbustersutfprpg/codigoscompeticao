#include <stdio.h>


void main(){

    int n, m, r, orcamento, daedelus, jogador, total, valor, rendimento = 0;

    scanf("%d %d", &n, &r);
    n--;

    while(r--){

        m = n;
        scanf("%d %d", &orcamento, &daedelus);
        total = 0;

        while(m--){
            scanf("%d", &jogador);
            total += jogador;
        }

        if (total >= orcamento)
            continue;

        valor = 1;
        while( (total + valor) <= orcamento){
            valor *= 10;
        }
        valor /= 10;
        if (valor > 10000)
            valor = 10000;

        if (daedelus > valor)
            rendimento += valor;
        else if (daedelus < valor)
            rendimento += (valor - daedelus);
    }

    printf("%d\n", rendimento);

}
