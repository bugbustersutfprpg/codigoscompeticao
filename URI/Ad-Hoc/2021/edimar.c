#include <stdio.h>


void main(){

    int a, b, c, p, cont;

    scanf("%d %d %d", &a, &b, &c);

    while(a){
        cont = 0;
        while(c--){
            scanf(" %d", &p);
            if (p > b)
                p = p%b;
            if (!p)
                p = b;
            cont += (b-p)+1;
        }

        printf("Lights: %d\n", cont);
        scanf("%d %d %d", &a, &b, &c);
    }

}
