#include <stdio.h>

void main(){

    int i, j, b, n, m, p1, p2;

    scanf("%d %d", &n, &m);
    if (n == 2){
        p1 = 2;
    }else{
        if ( !(n % 2) )
            n--;

        for (i = n; ; i -= 2){
            for (j = 3; j <= i; j += 2){
                if ( !(i % j) ){
                    break;
                }
            }
            if (j >= i)
                break;
        }
        p1 = i;
    }

    if (m == 2){
        p2 = 2;
    }else{
        if ( !(m % 2) )
            m--;

        for (i = m; ; i -= 2){
            for (j = 3; j <= i; j += 2){
                if ( !(i % j) ){
                    break;
                }
            }
            if (j >= i)
                break;
        }
        p2 = i;
    }

    printf("%d\n", p1*p2);
}
