#include <iostream>
#include <stdio.h>

using namespace std;

long long int r, g, b, total, azul;
char sr[32], sg[32], sb[32];

void imprimir(long long int valor){
	
	if (valor > 15){
		imprimir(valor / 16);
	}
	
	printf("%x", valor%16);
}

int main(){

	scanf("%x %x %x", &r, &g, &b);
	//printf("%lli %lli %lli\n", r, g, b);
	
	azul = (g/b) * (g/b);
	
	total = 1;
	
	total += (r/g) * (r/g);
	total = total + (total-1) * azul;
	
	
	imprimir(total);
	puts("");
	
}