#include <iostream>
#include <map>
#include <string.h>
#include <string>

using namespace std;

char c;
int peso;
string chave, contain, aux;
map<string, int> container;		//este map conterá todas os tipos diferentes de organização dos containers
multimap<int, string> fila;		//este map conterá a fila de Dijkstra. Uso de multimap para ordenar de forma crescente com chaves repetidas
map<string, int>::iterator it;
multimap<int, string>::iterator itFila;

//#############################################################################################################

int troca(int a, int b){
	int k = 0, j = 0;
	for (int i = 0; i < 4; i++){
		if (contain[a+i] < 45 && contain[b+i] < 45){
			return k + j;
		}
		c = contain[a+i];
		contain[a+i] = contain[b+i];
		contain[b+i] = c;
		
		if (c > 45){
			k = k * 10 + c - 48;
		}
		if (contain[a+i] > 45){
			j = j * 10 + contain[a+i] - 48;
		}
	}
	return k + j;
}

void inserir(int a, int b){
	int pes = troca(a, b) + peso;
	it = container.find(contain);
	if (it == container.end()){
		container.insert(make_pair(contain, pes) );
		fila.insert(make_pair(pes, contain));
	}else{
		for (itFila = fila.find(it->second); itFila != fila.end() && pes < itFila->first; itFila++){
			if (itFila->second == it->first){
				fila.erase(itFila);
				fila.insert(make_pair(pes, it->first));
				it->second = pes;
				break;
			}
		}
	}
	troca(a, b);
}

void entrada(string &s){
	int j;
	for (int i = 0; i < 8; ){
		cin >> aux;
		s += aux;
		i++;
		for (j = s.length(); j < (i*4); j++)
			s += ' ';
	}
}

//##########################################################################################################
//##########################################################################################################

int main(){
	
	int j;
	
	entrada(contain);
	entrada(chave);	
	
	container.insert(make_pair(contain, 0) );
	fila.insert(make_pair(0, contain) );
	
	while(1){
		//pega próximo da fila
		itFila = fila.begin();
		peso = itFila->first;
		contain = itFila->second;
		fila.erase(itFila);
		
		if (contain == chave){
			break;
		}
		
		//percorre as possíveis mudanças para este container e insere na fila
		for (int i = 0 ; i < 4; i++){
			j = i * 4;
			if (i != 3){
				inserir(j, j+4);
				inserir(j+16, j+20);
			}
			inserir(j, j+16);
		}
	}
	
	cout << peso << endl;
}