#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#define SWAP(x, a, b) do{\
		char c = x[a];\
		x[a] = x[b];\
		x[b] = c;\
	}while(0)

#define SWAP_HEAP(x, a, b, tmp) do{\
			x[a].node -> posHeap = b;\
			x[b].node -> posHeap = a;\
			memcpy(tmp, x + a, sizeof *tmp);\
			memcpy(x + a, x + b, sizeof *tmp);\
			memcpy(x + b, tmp, sizeof *tmp);\
		}while(0)

#define MIN(a, b) (a) < (b) ? (a) : (b)

typedef struct heapElement_s{
	struct node_s *node;
}heapElement_t;

typedef struct heap_s{

	size_t size;
	size_t last;
	heapElement_t *element;

}heap_t;

typedef struct node_s{

	char value[9];
	char marked;
	unsigned int cost;
	size_t posHeap;
	struct node_s *left;
	struct node_s *right;

}node_t;


unsigned int G_used[9];


unsigned int cost(char a, char b){
	return G_used[a - 'a'] + G_used[b - 'a'];
}

void printTree(node_t *root){
	if(root == NULL) return;
	printTree(root -> left);
	printf("value = %s\tcost = %10u\theapPos = %10zu\tmarked = %d\n", root -> value, root -> cost, root -> posHeap, root -> marked);
	printTree(root -> right);
}

void addHeap(heap_t *heap, node_t *n){

	size_t tmp;

	if(n -> marked)
		return;

	if(heap -> last == heap -> size){
		heap -> size *= 2;
		heap -> element = realloc(heap -> element, sizeof *(heap -> element) * heap -> size);
	}

	if(n -> posHeap == 0){
		heap -> element[++(heap -> last)].node = n;
		n -> posHeap = heap -> last;
		
	}

	heapElement_t *he = malloc(sizeof *he);

	while(n -> posHeap / 2 >= 1){

		if(heap -> element[n -> posHeap].node -> cost < heap -> element[n -> posHeap / 2].node -> cost){
			//printf("pre %zu\n", n -> posHeap);
			tmp = n -> posHeap;
			SWAP_HEAP(heap -> element, (tmp), (tmp / 2), he);
			//printf("pos %zu\n", n -> posHeap);
		}

		else
			break;
	}

	free(he);
	
	
}

heapElement_t *extractMin(heap_t *heap){

	memcpy(heap -> element, heap -> element + 1, sizeof *(heap -> element));

	if(heap -> last > 1)
		memcpy(heap -> element + 1, heap -> element + heap -> last, sizeof *(heap -> element));

	(heap -> last)--;

	size_t i = 1;
	size_t lesser;

	heapElement_t *he = malloc(sizeof *he);

	while(i <= heap -> last){

		if(i * 2 > heap -> last)
			break;

		if(i * 2 + 1 <= heap -> last){
			lesser = heap -> element[i*2].node -> cost < heap -> element[i*2 + 1].node -> cost ? i*2 : i*2 + 1;
		}

		else
			lesser = i * 2;

		if(heap -> element[i].node -> cost > heap -> element[lesser].node -> cost){
			SWAP_HEAP(heap -> element, i, lesser, he);
			i = lesser;
		}

		else
			break;
		
	}

	free(he);
	heap -> element -> node -> marked = 1;
	heap -> element -> node -> posHeap = 0;
	return heap -> element;

}

node_t *newNode(char *value){
	node_t *node = malloc(sizeof *node);
	strcpy(node -> value, value);
	node -> left = NULL;
	node -> right = NULL;
	node -> posHeap = 0;
	node -> marked = 0;
	node -> cost = -1;
	return node;
}
// inserts a new node if not exists.
node_t *insertNode(node_t *root, char *value, node_t **new){

	int cmp;

	if(root == NULL){
		root = newNode(value);
		if(new != NULL) *new = root;
		return root;
	}

	cmp = strcmp(value, root -> value);

	if(cmp > 0)
		root -> right = insertNode(root -> right, value, new);
	
	else if(cmp < 0)
		root -> left = insertNode(root -> left, value, new);

	else{
		if(new != NULL) *new = root;
		return root;
	}

	return root;
}



void generate(node_t *root, heap_t *heap, node_t *source){

	node_t *newNode;
	char aux[9];
	int i;
	strcpy(aux, source -> value);
	for(i = 0; i < 3; i++){

		SWAP(aux, i, i+1);
		insertNode(root, aux, &newNode);
		newNode -> cost = MIN(source -> cost + cost(aux[i], aux[i+1]), newNode -> cost);
		addHeap(heap, newNode);
		SWAP(aux, i, i+1);

		SWAP(aux, i, i+4);
		insertNode(root, aux, &newNode);
		newNode -> cost = MIN(source -> cost + cost(aux[i], aux[i+4]), newNode -> cost);
		addHeap(heap, newNode);
		SWAP(aux, i, i+4);
	}
	for(i = 4; i < 7; i++){
		SWAP(aux, i, i+1);
		insertNode(root, aux, &newNode);
		newNode -> cost = MIN(source -> cost + cost(aux[i], aux[i+1]), newNode -> cost);
		addHeap(heap, newNode);
		SWAP(aux, i, i+1);
	}
	
	SWAP(aux, 3, 7);
	insertNode(root, aux, &newNode);
	newNode -> cost = MIN(source -> cost + cost(aux[3], aux[7]), newNode -> cost);
	addHeap(heap, newNode);

}

char mapNumber(int n){

	int i;
	for(i = 0; i < 9; i++){

		if(G_used[i] == n)
			return 'a' + i;

		else if(G_used[i] == 0)
			break;
	}

	G_used[i] = n;
	return 'a' + i;

}


void solve(char *start, char *target){

	heap_t *heap = malloc(sizeof *heap);
	heap -> size = 50000;
	heap -> last = 0;
	heap -> element = malloc(sizeof *(heap->element) * heap -> size);
	
	node_t *root = insertNode(NULL, start, NULL);
	root -> cost = 0;
	addHeap(heap, root);
	heapElement_t *he;
	//generate(root, heap, root);
	
	while(1){
	//	printTree(root);
	//	puts("---");
		he = extractMin(heap);

		if(strcmp(he -> node -> value, target) != 0)
			generate(root, heap, he -> node);
		else
			break;
	}
	printf("%u\n", he -> node -> cost);
}

int main(){

	int i;
	int input;
	char start[9];
	char target[9];
	

	for(i = 0; i < 8; i++){
		scanf("%d", &input);
		start[i] = mapNumber(input);
	}

	for(i = 0; i < 8; i++){
		scanf("%d", &input);
		target[i] = mapNumber(input);
	}

	start[8] = '\0';
	target[8] = '\0';

	if(strcmp(start, target) == 0){
		printf("0\n");
		return 0;
	}

	solve(start, target);
	
	return 0;
}
