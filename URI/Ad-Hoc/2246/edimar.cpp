#include <iostream>

using namespace std;

int total, ladrilhos[200][200], i, j, m, n;


int contar(int cor, int l, int c){
	
	int qtd = 1;
	
	ladrilhos[l][c] = -1;
	
	if (l != 0 && ladrilhos[l-1][c] == cor){
		qtd += contar(cor, l-1, c);
	}

	if (c < (n-1) && ladrilhos[l][c+1] == cor){
		qtd += contar(cor, l, c+1);
	}
	
	if (l < (m-1) && ladrilhos[l+1][c] == cor){
		qtd += contar(cor, l+1, c);
	}
	
	if (c != 0 && ladrilhos[l][c-1] == cor){
		qtd += contar(cor, l, c-1);
	}
	
	return qtd;
}

void procurarMenor(){
	
	int valor;
	for (i = 0; i < m; i++){
		for (j = 0; j < n; j++){
			if (ladrilhos[i][j] != -1){
				valor = contar(ladrilhos[i][j], i, j);
				if (valor < total){
					total = valor;
					if (total == 1){
						return;
					}
				}
			}
		}
	}	
}

int main(){
	
	cin >> m >> n;
	
	for (i = 0 ;i < m; i++){
		for (j = 0; j < n; j++){
			cin >> ladrilhos[i][j];
		}
	}
	
	total = 400000;
	procurarMenor();
	
	cout << total << endl;
	
}