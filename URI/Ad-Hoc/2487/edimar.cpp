#include <iostream>
#include <stdio.h>

using namespace std;
#define T 1001

int main(){
	
	unsigned long long int tempo, e, v[T];
	int i, ativ;
	
	while(scanf("%llu %d", &tempo, &ativ) != -1){
		
		if (ativ == 1){
			v[0] = 0;
		}else{
			tempo -= ativ;
			tempo++;
			
			for (i = 0; i < ativ; i++){
				v[i] = 1;
			}
			
			e = 1;
			while (1){
				if (e >= tempo){
					break;
				}
				e = e << 1;
			}
	
			if (e == tempo){
				v[0] = e;
				tempo = 0;
			}else{
				e = e >> 1;
				v[0] = e;
				tempo -= e;
				tempo++;
				
				for (i = 1; e > 0; ){
					if (e <= tempo){
						v[i++] = e;
						tempo -= e;
						if (tempo == 0)
							break;
						tempo++;
						if (i == ativ)
							break;
					}else
						e = e >> 1;
				}
			}
			 
			if (tempo)
				tempo--;
		}
		
		cout << v[0] + tempo;
		for (i = 1; i < ativ; i++){
			cout << " " << v[i];
		}
		puts("");
	}
	
}