#include <stdio.h>


void main(){

    int i, n1, n2, d1, d2, num, den, num_s, den_s, r;
    char c;

    scanf("%d", &i);

    while(i--){

        scanf("%d / %d %c %d / %d", &n1, &d1, &c, &n2, &d2);

        if (c=='+'){
            num = n1*d2 + n2*d1;
            den = d1*d2;
        }
        if (c=='-'){
            num = n1*d2 - n2*d1;
            den = d1*d2;
        }
        if(c=='/'){
            num = n1*d2;
            den = d1*n2;
        }
        if(c=='*'){
            num = n1*n2;
            den = d1*d2;
        }

        num_s = num;
        den_s = den;
        while(den_s != 0){
            r = num_s % den_s;
            num_s = den_s;
            den_s = r;
        }
        num_s = abs(num_s);

        den_s = den / num_s;
        num_s = num / num_s;

        printf("%d/%d = %d/%d\n", num, den, num_s, den_s);
    }
}
