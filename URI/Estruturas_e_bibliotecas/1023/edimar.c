#include <stdio.h>
#define T 200


typedef struct p{

    int metroCubico;
    int quantPessoa;

}Person;


int main(){

    int i, j, n, q, somaPessoa, somaConsumo, pessoa, consumo;
    Person vetor[T];
    double media;
    char s[16];

    scanf("%d", &n);
    q = 1;

    while(1){
        media = 0;
        somaPessoa = 0;
        somaConsumo = 0;

        i = 0;
        while(i < T){
            vetor[i].metroCubico = 0;
            vetor[i++].quantPessoa = 0;
        }

        i = 0;
        while(i < n){
            scanf("%d %d", &pessoa, &consumo);
            somaPessoa += pessoa;
            somaConsumo += consumo;
            consumo = consumo / pessoa;
            vetor[consumo].metroCubico++;
            vetor[consumo].quantPessoa += pessoa;
            i++;
        }

        media = (double)somaConsumo / somaPessoa;

        printf("Cidade# %d:\n", q++);
        i = 0;
        j = 0;
        while(1){
            if (vetor[i].metroCubico){
                printf("%d-%d", vetor[i].quantPessoa, i);
                j += vetor[i].metroCubico;
                if (j < n)
                    printf(" ");
                else
                    break;
            }
            i++;
        }
        i = media * 100;
        sprintf(s, "%d", i);
        j = 0;
        while(s[j++]);
        s[j--] = 0;
        s[j] = s[j-1];
        j--;
        s[j] = s[j-1];
        s[j-1] = '.';
        printf("\nConsumo medio: %s m3.\n", s);

        scanf("%d", &n);
        if (!n)
            break;
        puts("");
    }
    return 0;
}
