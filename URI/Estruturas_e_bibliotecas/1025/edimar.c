#include <stdio.h>
#define T 10001

int main(){

    int i, j, n, q, cont, number[T], mapa[T], tentativa;

    scanf("%d %d", &n, &q);
    j = 1;

    i = 0;
    while(i < T)
        mapa[i++] = 0;

    while(1){
        i = 0;
        while(i < n){
            scanf("%d", &number[i]);
            mapa[number[i]]++;
            i++;
        }

        printf("CASE# %d:\n", j++);
        while(q--){
            scanf("%d", &tentativa);
            if (mapa[tentativa]){
                i = 0;
                cont = 0;
                while(i < tentativa){
                    cont += mapa[i++];
                }
                printf("%d found at %d\n", tentativa, cont+1);
            }else{
                printf("%d not found\n", tentativa);
            }
        }

        i = 0;
        while(i < n){
            mapa[number[i++]] = 0;
        }
        
        scanf("%d %d", &n, &q);
        if (!n && !q)
            break;
    }

    return 0;
}
