#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include <string.h>

using namespace std;

typedef struct a{

    char nome[32];
    int q;
    struct a* menor;
    struct a* maior;

}A;


A* insere(A* arv, char s[]){

    if (arv == NULL){
        arv = (A*)malloc(sizeof(A));
        arv->menor = NULL;
        arv->maior = NULL;
        strcpy(arv->nome, s);
        arv->q = 1;
    }else{
        int a = strcmp(arv->nome, s);
        if (a == 0){
            arv->q++;
        }else if (a == 1){
            arv->menor = insere(arv->menor, s);
        }else{
            arv->maior = insere(arv->maior, s);
        }
    }
    return arv;
}

void imprimir(A* arv, int t){

    if (arv == NULL)
        return;

    imprimir(arv->menor, t);
    printf("%s %.4lf\n", arv->nome, ((double) arv->q / t) * 100);
    imprimir(arv->maior, t);
    free(arv);
}

int main(){

    int n, i, t;
    char c[32], d;
    A *arv;

    cin >>n;
    cin.ignore();
    cin.ignore();

    while(n--){
        t = 0;
        arv = NULL;

        while (1){
            scanf("%[^\n]", c);
            d = getchar();

            if (c[0] == 0 || c[0] == 10 || c[0] == '\n')
                break;

            t++;
            arv = insere(arv, c);
            c[0] = 0;
        }

        imprimir(arv, t);
        if (n)
            puts("");
    }
}
