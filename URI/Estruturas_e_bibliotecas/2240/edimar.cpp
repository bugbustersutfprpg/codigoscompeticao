#include <iostream>

using namespace std;
#define T 10002

int n[T][3], m[T][3], esq, dir, maior, pai[3];
bool h;

void entrada(int v[][3], int &d){
	int total, a, b, c;
	
	cin >> d;
	for (int i = 0; i < d; i++){
		cin >> a >> b >> c;
		v[a][0] = i; v[a][1] = b; v[a][2] = c;
	}
}

void resolve(int i, int d, int e, int v[][3], int t){
	
	if (v[i][d])
		resolve(v[i][d], d, e, v, t+1);
	
	if (h){
		pai[d] = t;
		h = false;
	}
	
	if (t > maior)
		maior = t;
		
	if (v[i][e])
		resolve(v[i][e], d, e, v, 1);
	
	return;
}

//#####################################################################

int main(){
	
	int total;
	
	entrada(n, esq);
	entrada(m, dir);
	
	total = esq + dir;
	
	maior = 0; h = true;
	resolve(1, 2, 1, n, 1);
	esq = maior;
	
	maior = 0; h = true;
	resolve(1, 1, 2, m, 1);
	dir = maior;
	
	
	(dir > pai[1]) ? ((dir < pai[2]) ? maior = dir : maior = pai[2]) : ((pai[1] < pai[2]) ? maior = pai[1] : maior = pai[2] );
	(esq > pai[2]) ? ( (esq < pai[1]) ? dir = esq : dir = pai[1] ) : ( (pai[2] < pai[1]) ? dir = pai[2] : dir = pai[1] );
	(dir > maior) ? total -= dir : total -= maior;
	
	cout << total << endl;
}