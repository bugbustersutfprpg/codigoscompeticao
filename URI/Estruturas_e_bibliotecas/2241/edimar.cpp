#include <iostream>

using namespace std;

char tab[501][501];

int main(){

    int dim, qtde, x, y, totalBrancas, totalPretas;

    cin >> dim >> qtde;
    totalBrancas = qtde;
    totalPretas = qtde;

    for (int j = 1; j < 3; j++){
        for (int i = 1; i <= qtde; i++){
            cin >> x >> y;
            tab[x][y] = j;
        }
    }

    for (int k = 2; k < dim; k++){
        for (int i = 1; i <= (dim-k+1); i++){
            for (int j = 1; j <= (dim-k+1); j++){
                if (tab[i][j] == 3 || tab[i][j+1] == 3 || tab[i+1][j+1] == 3 || tab[i+1][j] == 3){
                    tab[i][j] = 3;
                }else if (tab[i][j] == 0 && tab[i][j+1] == 0 && tab[i+1][j+1] == 0 && tab[i+1][j] == 0){
                    tab[i][j] = 0;
                }else if ( (tab[i][j] == 0 || tab[i][j] == 1) && (tab[i][j+1] == 0 || tab[i][j+1] == 1) &&
                          (tab[i+1][j+1] == 0 || tab[i+1][j+1] == 1) && (tab[i+1][j] == 0 || tab[i+1][j] == 1)){
                    tab[i][j] = 1;
                    totalPretas++;
                }else if ( (tab[i][j] == 0 || tab[i][j] == 2) && (tab[i][j+1] == 0 || tab[i][j+1] == 2) &&
                          (tab[i+1][j+1] == 0 || tab[i+1][j+1] == 2) && (tab[i+1][j] == 0 || tab[i+1][j] == 2)){
                    tab[i][j] = 2;
                    totalBrancas++;
                }else{
                    tab[i][j] = 3;
                }
            }
        }
    }

    cout << totalPretas << " " << totalBrancas << endl;
}
