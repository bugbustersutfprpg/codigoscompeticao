import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedOutputStream;
import java.lang.StringBuilder;

public class Main{

	public static void main(String args[]) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		StringBuilder sb = new StringBuilder();
		
		String input[] = br.readLine().split(" ");
		int n = Integer.parseInt(input[0]);
		int p = Integer.parseInt(input[1]);
		
		byte matrix[][] = new byte[n][n];
		
		int line;
		int column;
		int totalBlack = p;
		int totalWhite = p;
		
		for(int i = 0; i < p*2; i++){
		
			input = br.readLine().split(" ");
			line = Integer.parseInt(input[0]);
			column = Integer.parseInt(input[1]);
			
			matrix[line-1][column-1] = i < p ? (byte) 1 : (byte) 2;

		}
		
		boolean stop;
		
		for(int k = 1; k < n-1; k++){
			stop = true;
			
			for(int i = 0; i < n - k; i++){
				for(int j = 0; j < n - k; j++){
				
					if(has(matrix, i, j, 3)){
						matrix[i][j] = 3;
					}

					else if(has(matrix, i, j, 1) && has(matrix, i, j, 2)){
						matrix[i][j] = 3;
					}
									
					else if(has(matrix, i, j, 1)){
						totalBlack++;
						matrix[i][j] = 1;
						stop = false;
					}
					
					else if(has(matrix, i, j, 2)){
						totalWhite++;
						matrix[i][j] = 2;
						stop = false;
					}
				
				
				}
			
			}
			
			if(stop) break;
		}
	
		
		sb.append(totalBlack)
		  .append(" ")
		  .append(totalWhite)
		  .append(System.lineSeparator());
		out.write(sb.toString().getBytes());
		out.flush();	
		
	
	}	
	
	static boolean has(byte matrix[][], int i, int j, int n){
		return matrix[i][j] == n || matrix[i+1][j] == n || matrix[i][j+1] == n || matrix[i+1][j+1] == n;
	}

}
