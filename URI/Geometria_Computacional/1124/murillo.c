#include <stdio.h>
#include <math.h>

#define YES "S"
#define NO "N"
 
/*
(r1 + r2) * (r1 + r2) = (r1 - r2) * (r1 - r2) + d * d
(r1 + r2) ^ 2 - (r1 - r2) ^ 2

 4 * r1 * r2 = d ^ 2
 d = 2.0 * sqrt(r1 * r2);
*/
            
int main() {
 
    unsigned int r1;
    unsigned int r2;
    unsigned int tmp;
    unsigned int w;
    unsigned int h;
    double d;
	double sq2 = sqrt(2.0);
    
    while(1){
        
        scanf("%u %u %u %u", &w, &h, &r1, &r2);
        if(r1 == 0) break;
        
        d = sqrt((double) (w * w + h * h));
	   if(d < sq2 * r1 + sq2 * r2 + r1 + r2)
		puts(NO);

		else
			puts(YES);
        
    }
    
    return 0;
}
