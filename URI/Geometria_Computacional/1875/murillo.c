#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct team_s{

	char tag[12];
	unsigned int goalsMade;
	unsigned int points;

} team_t;

typedef struct return_s{

	team_t *a;
	team_t *b;

} return_t;


/**
  * 	R goal B = 1 pt
  *	R goal G = 2 pt
  *
  *	B goal R = 2 pt
  *	B goal G = 1 pt
  *	
  *	G goal B = 2 pt
  *	G goal R = 1 pt
  *
  * @param a Who made the goal
  * @param b Who suffered the goal
  * @return 1 or 2, depending on who did the goal:	
  *
  */
char getGoalPoint(char a, char b){

	switch(a){

		case 'R':
			return b == 'B' ? 1 : 2;

		case 'B':
			return b == 'R' ? 2 : 1;

		default:
			return b == 'B' ? 2 : 1;

	}

}

/**
 * @return two teams which have the most goal points
 */
return_t* cmpTeam(team_t *ta, team_t *tb, team_t *tc){

	return_t *rt = malloc(sizeof(*rt));
	team_t *tmp;

	if(ta -> goalsMade > tb -> goalsMade){
		rt -> a = ta;
		rt -> b = tb;
	}

	else{
		rt -> a = tb;
		rt -> b = ta;
	}	

	if(rt -> b -> points == tc -> points)
		rt -> b = rt -> b -> goalsMade > tc -> goalsMade ? rt -> b : tc;

	else if(rt -> b -> points < tc -> points)
		rt -> b = tc;

	if(rt -> a -> points < rt -> b -> points || (rt -> a -> points == rt -> b -> points && rt -> a -> goalsMade < rt -> b -> goalsMade)){

		tmp = rt -> a;
		rt -> a = rt -> b;
		rt -> b = tmp;

	}
	

	return rt;

}

void tribol(unsigned int testNum){

	char first;
	char second;

	unsigned int n;

	team_t r;
	team_t g;
	team_t b;

	strcpy(r.tag, "red");
	r.goalsMade = 0;
	r.points = 0;

	strcpy(g.tag, "green");
	g.goalsMade = 0;
	g.points = 0;

	strcpy(b.tag, "blue");
	b.goalsMade = 0;
	b.points = 0;

	return_t *rt;

	scanf("%u", &n);

	while(n > 0){

		scanf(" %c %c", &first, &second);
		
		switch(first){

			case 'R':
				r.goalsMade++;
				r.points += getGoalPoint(first, second);
				break;

			case 'G':
				g.goalsMade++;
				g.points += getGoalPoint(first, second);
				break;

			default:
				b.goalsMade++;
				b.points += getGoalPoint(first, second);	

		}

		n--;
	}

	if(r.points == g.points && g.points == b.points)
		puts("trempate");

	else{

		rt = cmpTeam(&r, &g, &b);

		if(rt -> a -> points == rt -> b -> points)
			puts("empate");
		
		else{
			printf("%s\n", rt -> a -> tag);
		}

		free(rt);

	}

	//printf("Teste %u\nR.goalsMade: %u, R.points: %u\n\nG.goalsMade: %u, G.points: %u\n\nB.goalsMade: %u, B.points: %u\n\n", testNum, r.goalsMade, r.points, g.goalsMade, g.points, b.goalsMade, b.points);

}

int main(){

	unsigned int n;
	unsigned int i = 0;

	scanf("%u", &n);

	while(i < n){
		tribol(i);
		i++;
	}

}
