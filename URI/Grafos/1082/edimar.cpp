#include <iostream>
#include <set>

using namespace std;
#define T 26

void criar(int v, set<char>* arv){

    for (int j = 0; j < v; j++){
        arv[j].insert(j+97);
    }
}

int main(){
    set<char> arv[T];
    set<char>::iterator it;
    int i, j, k, n, v, e, t;
    char a, b;
    bool sai;

    cin>>n;

    i = 0;
    while(i++ < n){
        cin>>v>>e;

        t = v;
        criar(v, arv);
        while(e--){
            cin>>a>>b;

            sai = false;
            for(j = 0; j < v; j++){
                it = arv[j].find(a);
                if (it != arv[j].end()) {
                    for(k = 0; k < v; k++){
                        it = arv[k].find(b);
                        if (it != arv[k].end()) {
                            if (j != k){
						if(j > k){
							j^=k;
							k^=j;
							j^=k;
						}
                                for (it = arv[k].begin(); it != arv[k].end(); it++){
                                    arv[j].insert(*it);
                                }
                                arv[k].clear();
                                t--;
						
						
                            }
                            sai = true;
                            break;
                        }
                    }
                }
                if (sai)
                    break;
            }
        }

        cout<<"Case #"<<i<<":\n";
        for(j = 0; j < v; j++){
            k = 0;
            for (it = arv[j].begin(); it != arv[j].end(); it++){
                cout<<*it<<",";
                k++;
            }
            
            if (k){
                cout<<endl;
			arv[j].clear();
		}
        }
        cout<<t<<" connected components\n\n";
    }
}
