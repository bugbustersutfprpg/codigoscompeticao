import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Testing javadoc
 */
public class Main{


	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		double area = Double.parseDouble(br.readLine());
		double pi = 3.14159;
		DecimalFormat df = new DecimalFormat("0.0000");
		//df.setRoundingMode(RoundingMode.DOWN);
		System.out.println("A=" + df.format(area * area * pi));

	}

}
