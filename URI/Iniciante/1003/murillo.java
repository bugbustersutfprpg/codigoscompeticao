import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.BufferedOutputStream;
import java.io.OutputStream;

public class Main{

	public static void main(String args[]) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		OutputStream out = new BufferedOutputStream(System.out);

		String input = br.readLine();
		int a = Integer.parseInt(input);
		input = br.readLine();
		int b = Integer.parseInt(input);

		out.write(("SOMA = " + String.valueOf(a + b) + System.lineSeparator()).getBytes());
		out.flush();


	}

}
