import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedOutputStream;

public class Main{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
		int b = Integer.parseInt(br.readLine());
		
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		out.write(("PROD = " + a * b + System.lineSeparator()).getBytes());
		out.flush();
		out.close();

	}

}
