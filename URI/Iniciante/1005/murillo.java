import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

public class Main{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream out = new BufferedOutputStream(System.out);

		DecimalFormat df = new DecimalFormat("0.00000");

		double a = Double.parseDouble(br.readLine()) * 3.5;
		double b = Double.parseDouble(br.readLine()) * 7.5;

		out.write(("MEDIA = " + df.format((a + b) / 11d) + System.lineSeparator()).getBytes());

		out.flush();
		out.close();

	}

}
