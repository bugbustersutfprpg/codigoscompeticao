import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
//import java.math.RoundingMode;

public class Main{

	public static void main(String args[]) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream out = new BufferedOutputStream(System.out);

		DecimalFormat df = new DecimalFormat("#.0");
//		df.setRoundingMode(RoundingMode.UP);

		double a = Double.parseDouble(br.readLine()) * 2d	;
		double b = Double.parseDouble(br.readLine()) * 3d;
		double c = Double.parseDouble(br.readLine()) * 5d;

		String ans = "MEDIA = " + df.format((a + b + c) / 10d) + System.lineSeparator();

		out.write(ans.getBytes());
		out.flush();
		out.close();

	}

}
