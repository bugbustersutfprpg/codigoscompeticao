import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int number = Integer.parseInt(br.readLine());
		int work = Integer.parseInt(br.readLine());
		float salary = Float.parseFloat(br.readLine());
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		
		System.out.println("NUMBER = " + number);
		System.out.println("SALARY = U$ " + df.format(salary * work).replace(",", "."));
		
		
	
	}

}
