import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		br.readLine();
		double work = Double.parseDouble(br.readLine());
		double salary = Double.parseDouble(br.readLine());
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		System.out.println("TOTAL = R$ " + df.format(work + 15d/100d * salary).replace(",", "."));
		
		
	
	}

}
