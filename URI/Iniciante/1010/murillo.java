import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main{

	public static void main(String args[]) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String input[] = br.readLine().split(" ");
		int num1 = Integer.parseInt(input[1]);
		float price1 = Float.parseFloat(input[2]);
		input = br.readLine().split(" ");
		int num2 = Integer.parseInt(input[1]);
		float price2 = Float.parseFloat(input[2]);
		
		System.out.format("VALOR A PAGAR: R$ %.2f%n", price1 * num1 + num2 * price2);
		
	
	}

}
