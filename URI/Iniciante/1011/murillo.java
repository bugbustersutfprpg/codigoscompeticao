import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		double input = Double.parseDouble(br.readLine());

		System.out.format("VOLUME = %.3f%n", input*input*input * 4d/3d * 3.14159d);

	}

}
