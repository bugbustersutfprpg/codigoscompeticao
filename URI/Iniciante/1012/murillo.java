import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		String input[] = br.readLine().split(" ");
		
		double a = Double.parseDouble(input[0]);
		double b = Double.parseDouble(input[1]);
		double c = Double.parseDouble(input[2]);
		
		double pi = 3.14159;
		
		System.out.format("TRIANGULO: %.3f%nCIRCULO: %.3f%nTRAPEZIO: %.3f%nQUADRADO: %.3f%nRETANGULO: %.3f%n",
			a*c/2d, pi*c*c, (a + b)/2d * c, b*b, a*b);

		

	}

}
