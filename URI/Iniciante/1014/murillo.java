import java.io.*;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(br.readLine());
		float b = Float.parseFloat(br.readLine());
		
		System.out.format("%.3f km/l%n", a/b);
	
	
	}

}
