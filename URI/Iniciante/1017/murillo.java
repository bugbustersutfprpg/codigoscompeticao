import java.io.*;
import java.util.Scanner;

public class Main{

	public static void main(String[] args) throws IOException{

		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();
		
		System.out.format("%.3f%n", (a * b) / 12f);
	
	}

}
