#include <stdio.h>
 
int main() {
 
    int a, b, c, d, e, f, g, h;
    scanf("%d", &a);
    h = a;
    b= a/100;
    a-= b*100;
    c= a/50;
    a-= c*50;
    d= a/20;
    a-= d*20;
    e= a/10;
    a-= e*10;
    f= a/5;
    a-= f*5;
    g= a/2;
    a-= g*2;

    printf("%d\n%d nota(s) de R$ 100,00\n", h, b);
    printf("%d nota(s) de R$ 50,00\n", c);
    printf("%d nota(s) de R$ 20,00\n", d);
    printf("%d nota(s) de R$ 10,00\n", e);
    printf("%d nota(s) de R$ 5,00\n", f);
    printf("%d nota(s) de R$ 2,00\n", g);
    printf("%d nota(s) de R$ 1,00\n", a);
 
    return 0;
}