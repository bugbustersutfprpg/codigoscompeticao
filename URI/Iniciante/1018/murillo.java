import java.io.*;
import java.lang.StringBuilder;
import java.util.Scanner;

public class Main{

	static int[] values = {100, 50, 20, 10, 5, 2, 1};

	public static void main(String[] args) throws IOException{
		
		int n = new Scanner(System.in).nextInt();
		int i = 0;

		StringBuilder sb = new StringBuilder();
		sb.append(n);

		func(n, sb, 0);
		System.out.println(sb.toString());
	}

	public static void func(int n, StringBuilder sb, int i){

		if(i == values.length) return;
		
		int k = n / values[i];
	
		sb.append(System.lineSeparator()).append(k).append(" nota(s) de R$ ")
		.append(values[i]).append(",00");

		func(n % values[i], sb, i+1);	
	}

}
