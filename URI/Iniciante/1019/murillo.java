import java.io.*;
import java.util.Scanner;

public class Main{

	public static void main(String[] args) throws IOException{
	
		int n = new Scanner(System.in).nextInt();
		
		int m = n / 60;
		int h = m / 60;
		m %= 60;
		n %= 60;
		System.out.println(h + ":" + m + ":" + n);
	
	}

}
