#include <stdio.h>
 
int main() {
 
    int a, b, c;
    scanf("%d", &a);
    b = a/365;
    a -= b*365;
    c = a/30;
    a -= c*30;
    
    printf ("%d ano(s)\n", b);
    printf ("%d mes(es)\n", c);
    printf ("%d dia(s)\n", a);
 
    return 0;
}