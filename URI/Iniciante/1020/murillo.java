import java.io.IOException;
import java.util.Scanner;

public class Main{

	public static void main(String[] args) throws IOException{
	
		int n = new Scanner(System.in).nextInt();
		
		int y = n / 365;
		n %= 365;
		int m = n / 30;
		n %= 30;
		
		System.out.println(y + " ano(s)");
		System.out.println(m + " mes(es)");
		System.out.println(n + " dia(s)");
	
	}

}
