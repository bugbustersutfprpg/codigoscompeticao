#include <stdio.h>
 
int main() {
 
    float a;
    int b, c, d, e, f, g, j, k ,l ,m ,n;
    scanf("%f", &a);
    b= a/100;
    a-= b*100;
    c= a/50;
    a-= c*50;
    d= a/20;
    a-= d*20;
    e= a/10;
    a-= e*10;
    f= a/5;
    a-= f*5;
    g= a/2;
    a-= g*2;

    j = a;
    a -= j;
    a *= 100;
    k = a/50;
    a -= k*50;
    l = a/25;
    a -= l*25;
    m = a/10;
    a -= m*10;
    n = a/5;
    a -= n*5;

    printf("NOTAS:\n%d nota(s) de R$ 100.00\n", b);
    printf("%d nota(s) de R$ 50.00\n", c);
    printf("%d nota(s) de R$ 20.00\n", d);
    printf("%d nota(s) de R$ 10.00\n", e);
    printf("%d nota(s) de R$ 5.00\n", f);
    printf("%d nota(s) de R$ 2.00\n", g);

    printf("MOEDAS:\n%d moeda(s) de R$ 1.00\n", j);
    printf("%d moeda(s) de R$ 0.50\n", k);
    printf("%d moeda(s) de R$ 0.25\n", l);
    printf("%d moeda(s) de R$ 0.10\n", m);
    printf("%d moeda(s) de R$ 0.05\n", n);
    printf("%.0f moeda(s) de R$ 0.01\n", a);
 
    return 0;
}