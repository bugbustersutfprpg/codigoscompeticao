import java.io.*;
import java.lang.StringBuilder;
import java.util.Locale;

public class Main{

	static int val[] = {100, 50, 20, 10, 5, 2, 1};
	static int val2[] = {50, 25, 10, 5, 1};

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input[] = br.readLine().split("\\.");

		int a = Integer.valueOf(input[0]);
		int b = Integer.valueOf(input[1]);

		StringBuilder sb = new StringBuilder("NOTAS:" + System.lineSeparator());
		
		func(a, 0, sb);
		
		func2(b, 0, sb);

		System.out.print(sb.toString());
	}
	
	public static void func(int n, int i, StringBuilder sb){
		if(i == val.length) return;
		
		if(i == val.length - 1){
			sb.append("MOEDAS:").append(System.lineSeparator());
			sb.append(n / val[i]).append(" moeda(s) de R$ ").append(val[i])
				.append(".00").append(System.lineSeparator());
		}
		
		else{
			sb.append(n / val[i]).append(" nota(s) de R$ ").append(val[i])
				.append(".00").append(System.lineSeparator());
		}
		
		func(n % val[i], i+1, sb);
	}
	
	public static void func2(int n, int i, StringBuilder sb){
		if(i == val2.length) return;
		sb.append(n / val2[i]).append(" moeda(s) de R$ ")
			.append(String.format(Locale.US, "%.2f", val2[i] / 100d))
			.append(System.lineSeparator());
		func2(n % val2[i], i+1, sb);
	}
	
	

}
