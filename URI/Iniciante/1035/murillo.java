import java.io.*;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] input = br.readLine().split(" ");
		int a = Integer.parseInt(input[0]);
		int b = Integer.parseInt(input[1]);
		int c = Integer.parseInt(input[2]);
		int d = Integer.parseInt(input[3]);
		
		if(b > c && d > a && c + d > a + b && c >= 0 && d >= 0 && a % 2 == 0)
			System.out.println("Valores aceitos");
		else
			System.out.println("Valores nao aceitos");
	
	}

}
