#include <stdio.h>
 
int main() {
 
    float a, b, c, d, r;
    scanf ("%f %f %f", &a, &b, &c);
    if (a==0){
        puts("Impossivel calcular");
        return 0;
    }
    d = b*b -(4 * a * c);
    if (d<0){
        puts("Impossivel calcular");
        return 0;
    }
    r = sqrt(d);
    printf("R1 = %.5f\n", (-b+r)/(2*a));
    printf("R2 = %.5f\n", (-b-r)/(2*a));
}