import java.io.*;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] input = br.readLine().split(" ");
		
		double a = Double.parseDouble(input[0]);
		double b = Double.parseDouble(input[1]);
		double c = Double.parseDouble(input[2]);
		
		double d = b * b - 4 * a * c;

		if(d < 0 || a == 0){
			System.out.println("Impossivel calcular");
			return;
		}
		double r1 = (-b + Math.sqrt(d)) / (2*a);
		double r2 = (-b - Math.sqrt(d)) / (2*a);
		
		System.out.format("R1 = %.5f%nR2 = %.5f%n", r1, r2);
	
	}

}
