#include <stdio.h>
 
int main() {
 
    float a;
    scanf("%f", &a);
    if ((a<0) || (a>100)){
        puts("Fora de intervalo");
        return 0;
    }
    if (a>75){
        puts("Intervalo (75,100]");
        return 0;
    }
    if (a>50){
        puts("Intervalo (50,75]");
        return 0;
    }
    if (a>25){
        puts("Intervalo (25,50]");
        return 0;
    }
    puts("Intervalo [0,25]");
    
    return 0;
}