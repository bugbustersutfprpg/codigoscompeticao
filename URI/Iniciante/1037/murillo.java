import java.io.*;
import java.util.Scanner;
import java.util.Locale;

public class Main{

	public static void main(String[] args) throws IOException{
		Scanner s = new Scanner(System.in);
		s.useLocale(Locale.US);
		float a = s.nextFloat();

		if(a < 0f || a > 100f){
			System.out.println("Fora de intervalo");
		}
		
		else if(a > 75f){
			System.out.println("Intervalo (75,100]");		
		}
		
		else if(a > 50f){
			System.out.println("Intervalo (50,75]");
		}
		
		else if(a > 25f){
			System.out.println("Intervalo (25,50]");
		}
		
		else{
			System.out.println("Intervalo [0,25]");
		}
	
	}

}
