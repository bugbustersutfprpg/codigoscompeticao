#include <stdio.h>
 
int main() {
 
    int a, b;
    scanf ("%d %d", &a, &b);
    if (a==1){
        printf ("Total: R$ %.2f\n", b*4.0);
        return 0;
    }
    if (a==2){
        printf ("Total: R$ %.2f\n", b*4.5);
        return 0;
    }
    if (a==3){
        printf ("Total: R$ %.2f\n", b*5.0);
        return 0;
    }
    if (a==4){
        printf ("Total: R$ %.2f\n", b*2.0);
        return 0;
    }
    printf ("Total: R$ %.2f\n", b*1.5);
 
    return 0;
}