import java.io.*;

public class Main{

	public static void main(String[] args) throws IOException{
	
		float values[] = {4f, 4.5f, 5f, 2f, 1.5f};
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] input = br.readLine().split(" ");
		int a = Integer.parseInt(input[0]);
		int b = Integer.parseInt(input[1]);
		
		System.out.format("Total: R$ %.2f%n", values[a-1] * b);
		
	
	}

}
