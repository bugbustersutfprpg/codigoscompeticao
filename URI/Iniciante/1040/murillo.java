import java.io.*;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] input = br.readLine().split(" ");
		
		float avg = 0;
		avg += Float.parseFloat(input[0]) * 2;
		avg += Float.parseFloat(input[1]) * 3;
		avg += Float.parseFloat(input[2]) * 4;
		avg += Float.parseFloat(input[3]);
			
		avg /= 10f;

		if(avg >= 7f){
			System.out.format("Media: %.1f%nAluno aprovado.%n", avg);
		}
		else if(avg < 5f){
			System.out.format("Media: %.1f%nAluno reprovado.%n", avg);
		}
		
		else{
			float a = Float.parseFloat(br.readLine());
			System.out.format("Media: %.1f%nAluno em exame.%nNota do exame: %.1f%n", avg, a);
			avg = (avg + a) / 2f;
			System.out.format(avg >= 5f ? "Aluno aprovado.%n" : "Aluno reprovado.%n");
			System.out.format("Media final: %.1f%n", avg);
		}
	
	}

}
