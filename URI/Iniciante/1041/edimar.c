#include <stdio.h>
 
int main() {
 
    float a,b;
    scanf ("%f %f", &a, &b);
    
    if ((a==0) && (b==0)){
        puts ("Origem");
        return 0;
    }
    if (a==0){
        puts ("Eixo Y");
        return 0;
    }
    if (b==0){
        puts ("Eixo X");
        return 0;
    }
    if (a>0){
        if (b>0)
            puts ("Q1");
        else
            puts ("Q4");
    }else{
        if (b>0)
            puts ("Q2");
        else
            puts ("Q3");
    }
 
    return 0;
}