import java.io.IOException;
import java.util.Scanner;
import java.util.Locale;

public class Main{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		s.useLocale(Locale.US);
		
		float a = s.nextFloat();
		float b = s.nextFloat();
		
		if(a == 0f && b == 0f){
			System.out.println("Origem");
		}
		
		else if(a == 0f && b != 0f){
			System.out.println("Eixo Y");
		}
		
		else if(a != 0f && b == 0f){
			System.out.println("Eixo X");
		}
		
		else if(a >= 0f && b >= 0f){
			System.out.println("Q1");
		}
		
		else if(a <= 0f && b >= 0f){
			System.out.println("Q2");
		}
		
		else if(a <= 0f && b <= 0f){
			System.out.println("Q3");
		}
		else{
			System.out.println("Q4");
		}
	
	}

}
