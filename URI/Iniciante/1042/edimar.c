#include <stdio.h>
 
int main() {
 
    int a, b, c, i, j, k, u;
    scanf ("%d %d %d", &a, &b, &c);
    
    i = a;
    j = b;
    k = c;
    
    if (a>b){
        u = a;
        a = b;
        b = u;
    }
    if (a>c){
        u = a;
        a = c;
        c = u;
    }
    if (b>c){
        u = b;
        b = c;
        c = u;
    }
    printf ("%d\n%d\n%d\n\n%d\n%d\n%d\n", a, b, c, i, j, k);
        
    return 0;
}