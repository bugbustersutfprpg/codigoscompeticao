import java.io.*;
import java.util.Arrays;
import java.lang.StringBuilder;
public class Main{

	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] input = br.readLine().split(" ");
		StringBuilder sb = new StringBuilder();
		
		int[] val = new int[3];
		for(int i = 0; i < 3; i++)
			val[i] = Integer.parseInt(input[i]); 
			
		Arrays.sort(val);

		for(int a : val)
			sb.append(a + System.lineSeparator());
		
		sb.append(System.lineSeparator());
		
		for(String a : input)
			sb.append(a + System.lineSeparator());

		System.out.print(sb.toString());
	
	}

}


