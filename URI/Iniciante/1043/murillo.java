import java.io.*;
import java.util.Scanner;
import java.util.Locale;

public class Main{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		s.useLocale(Locale.US);
		
		float a = s.nextFloat();
		float b = s.nextFloat();		
		float c = s.nextFloat();
		
		float max = Math.max(a, Math.max(b, c));

		if(a + b + c - max > max)
			System.out.format("Perimetro = %.1f%n", a + b + c);
		
		else
			System.out.format("Area = %.1f%n", (a + b) / 2f * c);
	
	}

}
