# include <stdio.h>

int main (){

    float a, b, c, u;
    scanf ("%f %f %f", &a, &b, &c);

    if ((a==b) && (a==c)){
        puts ("TRIANGULO ACUTANGULO");
        puts ("TRIANGULO EQUILATERO");
        return 0;
    }
    if (a<b){
        u = a;
        a = b;
        b = u;
    }
    if (a<c){
        u = a;
        a = c;
        c = u;
    }
    if (b<c){
        u = b;
        b = c;
        c = u;
    }

    if (a>=(b+c)){
        puts ("NAO FORMA TRIANGULO");
        return 0;
    }
    if ((a*a)==(b*b + c*c)){
        puts ("TRIANGULO RETANGULO");
        return 0;
    }
    if ((a*a)> (b*b + c*c)){
        puts ("TRIANGULO OBTUSANGULO");
    }
    if ((a*a) < (b*b + c*c)){
        puts ("TRIANGULO ACUTANGULO");
    }
    if ((a==b) || (b==c)){
        puts ("TRIANGULO ISOSCELES");
    }

    return 0;
}
