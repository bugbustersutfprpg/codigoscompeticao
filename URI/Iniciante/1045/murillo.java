import java.io.*;
import java.util.Arrays;
public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String[] input = br.readLine().split(" ");
		double a = Double.parseDouble(input[0]);
		double b = Double.parseDouble(input[1]);
		double c = Double.parseDouble(input[2]);
		
		double vals[] = new double[3];
		vals[0] = a;
		vals[1] = b;
		vals[2] = c;
		
		Arrays.sort(vals);
		
		double ta = vals[2];
		double tb = vals[1];
		double tc = vals[0];

		a = ta;
		b = tb;
		c = tc;
		
		if(a >= b + c){
			System.out.println("NAO FORMA TRIANGULO");
			return;
		}
		
		a *= a;
		b *= b;
		c *= c;
		
		
		if(a == b + c){
			System.out.println("TRIANGULO RETANGULO");
		}
		
		else if(a > b + c){
			System.out.println("TRIANGULO OBTUSANGULO");
		}
		
		else if(a < b + c){
			System.out.println("TRIANGULO ACUTANGULO");
		}
		
		a = ta;
		b = tb;
		c = tc;
		
		if(a == b && b == c){
			System.out.println("TRIANGULO EQUILATERO");
		}
		
		else if(a == b || b == c){
			System.out.println("TRIANGULO ISOSCELES");
		}
		
	
	}

}
