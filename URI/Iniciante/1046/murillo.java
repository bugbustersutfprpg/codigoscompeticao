import java.io.*;
import java.util.Scanner;

public class Main{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		
		int a = s.nextInt();
		int b = s.nextInt();
		
		int time;
		
		if(a == b) time = 24;

		else if(a <= 12 && b <= 12 || a > 12 && b > 12){
			time = a < b ? b - a : 24 - (a - b);
		}
		else if(a <= 12 && b > 12){
			time = b - a;
		}
		else{
			time = 24 - (a - b);
		}
		
		System.out.println("O JOGO DUROU " + time + " HORA(S)"); 		
	
	}

}
