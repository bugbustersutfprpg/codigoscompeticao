#include <stdio.h>

int main (){

    int a, b, c, d;
    scanf ("%d %d %d %d", &a, &b, &c, &d);

    if ((d==b) && (a==c)){
        puts ("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)");
        return 0;
    }
    if (d<b){
        d += 60;
        c--;
        if (c==a){
            printf ("O JOGO DUROU 0 HORA(S) E %d MINUTO(S)\n", d-b);
            return 0;
        }
    }
    if (c<a){
        c += 24;
    }

    printf ("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n", c-a, d-b);

    return 0;
}
