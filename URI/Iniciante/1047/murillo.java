import java.io.*;
import java.util.Scanner;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);		
		
		int a = s.nextInt();
		int b = s.nextInt();
		int c = s.nextInt();
		int d = s.nextInt();
		
		int hour = a - c;
		int min = b - d;
		
		if(hour < 0)
			hour *= -1;
		
		else
			hour = 24 - hour;
			
		if(min <= 0)
			min *= -1;
		
		else
			min = 60 - min;
			
		if(d < b)
			hour--;
			
		System.out.println("O JOGO DUROU " + hour + " HORA(S) E " + min + " MINUTO(S)");
	
	}

}
