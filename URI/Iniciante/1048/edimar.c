#include <stdio.h>

int main (){

    float a, b;
    scanf ("%f", &a);

    if (a<400.01){
        b = a*1.15;
        printf ("Novo salario: %.2f\n", b);
        printf ("Reajuste ganho: %.2f\n", b-a);
        puts ("Em percentual: 15 %");
        return 0;
    }
    if (a<800.01){
        b = a*1.12;
        printf ("Novo salario: %.2f\n", b);
        printf ("Reajuste ganho: %.2f\n", b-a);
        puts ("Em percentual: 12 %");
        return 0;
    }
    if (a<1200.01){
        b = a*1.10;
        printf ("Novo salario: %.2f\n", b);
        printf ("Reajuste ganho: %.2f\n", b-a);
        puts ("Em percentual: 10 %");
        return 0;
    }
    if (a<2000.01){
        b = a*1.07;
        printf ("Novo salario: %.2f\n", b);
        printf ("Reajuste ganho: %.2f\n", b-a);
        puts ("Em percentual: 7 %");
        return 0;
    }

    b = a*1.04;
    printf ("Novo salario: %.2f\n", b);
    printf ("Reajuste ganho: %.2f\n", b-a);
    puts ("Em percentual: 4 %");

    return 0;
}
