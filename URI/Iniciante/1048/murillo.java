import java.io.IOException;
import java.util.Scanner;
import java.util.Locale;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		s.useLocale(Locale.US);
		
		float a = s.nextFloat();
		float b;
		
		float percentEarned;
		
		if(a > 2000f)
			percentEarned = 4f;
			
		else if(a > 1200f)
			percentEarned = 7f;
			
		else if(a > 800f)
			percentEarned = 10f;
			
		else if(a > 400f)
			percentEarned = 12f;
			
		else
			percentEarned = 15f;
			
		b = a * (1f + percentEarned / 100f);
			
		System.out.format("Novo salario: %.2f%nReajuste ganho: %.2f%nEm percentual: %.0f %%%n",
			b, b - a, percentEarned );
	
	}

}
