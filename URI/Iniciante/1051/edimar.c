#include <stdio.h>

int main(){

    float a, t;
    scanf("%f", &a);

    if (a<2000.01){
        puts("Isento");
        return 0;
    }
    if (a>4500.00){
        t = (a-4500) * 0.28;
        t += 1500 * 0.18 + 80;
        printf("R$ %.2f\n", t);
        return 0;
    }
    if (a>3000.00){
        t = (a-3000) * 0.18 + 80;
        printf("R$ %.2f\n", t);
        return 0;
    }
    printf("R$ %.2f\n", (a-2000) * 0.08);

    return 0;
}
