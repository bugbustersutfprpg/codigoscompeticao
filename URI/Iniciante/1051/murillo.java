import java.io.*;
import java.util.Scanner;
import java.util.Locale;

public class murillo{

	public static void main(String[] args) throws IOException{
	
			Scanner s = new Scanner(System.in);
			s.useLocale(Locale.US);
			double a = s.nextDouble();
			double tax = 0;
			
			if(a > 2000)
				a -= 2000.0;
				
			else{
				System.out.format("Isento%n");
				return;
			}
			
			if(a > 1000.0){
				a -= 1000.0;
				tax += 1000 * .08;
			}
			
			else{
				tax += a * .08;
				System.out.format("R$ %.2f%n", tax);
				return;
			}
			
			if(a > 1500.0){
				a -= 1500.0;
				tax += 1500 * .18;
			}
			
			else{
				tax += a * .18;
				System.out.format("R$ %.2f%n", tax);
				return;
			}
			
			tax += a * .28;
			System.out.format("R$ %.2f%n", tax);
		
	
	}

}
