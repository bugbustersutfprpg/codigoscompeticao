#include <stdio.h>

int main(){

    int d, h, m, s, df, hf, mf, sf;
    char p[10];
    scanf("%s %d", p, &d);
    scanf("%d : %d : %d", &h, &m, &s);
    scanf("%s %d", p, &df);
    scanf("%d : %d : %d", &hf, &mf, &sf);

    if (sf<s){
        sf += 60;
        mf--;
    }
    if (mf<m){
        mf += 60;
        hf--;
    }
    if (hf<h){
        hf += 24;
        df--;
    }

    printf ("%d dia(s)\n", df-d);
    printf ("%d hora(s)\n", hf-h);
    printf ("%d minuto(s)\n", mf-m);
    printf ("%d segundo(s)\n", sf-s);

    return 0;
}
