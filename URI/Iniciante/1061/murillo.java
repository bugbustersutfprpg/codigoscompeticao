import java.io.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input[] = br.readLine().split(" ");

		int startDay = Integer.parseInt(input[1]);
		input = br.readLine().split(":");
		
		int hStart = Integer.parseInt(input[0].trim());
		int mStart = Integer.parseInt(input[1].trim());
		int sStart = Integer.parseInt(input[2].trim());
		
		input = br.readLine().split(" ");
		int endDay = Integer.parseInt(input[1]);
		
		input = br.readLine().split(":");
		
		int hEnd = Integer.parseInt(input[0].trim());
		int mEnd = Integer.parseInt(input[1].trim());
		int sEnd = Integer.parseInt(input[2].trim());
		
		int diffDay = endDay - startDay;

		int diffH = hEnd - hStart;
		int diffM = mEnd - mStart;
		int diffS = sEnd - sStart;
		
		if(diffH < 0){
			diffDay--;
			diffH += 24;
		}
		
		if(diffM < 0){
			diffH--;
			diffM+= 60;
		}
		
		if(diffS < 0){
			diffM--;
			diffS += 60;
		}
			
		System.out.format("%d dia(s)%n%d hora(s)%n%d minuto(s)%n%d segundo(s)%n",
		Math.max(diffDay, 0), Math.max(diffH, 0), Math.max(diffM, 0), Math.max(diffS, 0));
		
	
	}

}
