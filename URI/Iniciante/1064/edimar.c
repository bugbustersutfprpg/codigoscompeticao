#include <stdio.h>

int main (){

    float a, t=0.0;
    int b, c=0;
    for (b=0; b<6; b++){
        scanf("%f", &a);
        if (a>0){
            c++;
            t += a;
        }
    }

    printf ("%d valores positivos\n%.1f\n", c, t/c);

    return 0;
}
