import java.io.*;
import java.util.Scanner;
import java.util.Locale;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		s.useLocale(Locale.US);
		
		float val[] = new float[6];
		int numPos = 0;
		float avg = 0;
		
		for(int i = 0; i < 6; i++){
			val[i] = s.nextFloat();
			
			if(val[i] > 0f){
				numPos++;
				avg += val[i];	
			}
			
		}
			
		System.out.format("%d valores positivos%n%.1f%n", numPos, avg / numPos);
		
	
	}

}
