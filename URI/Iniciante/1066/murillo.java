import java.io.IOException;
import java.util.Scanner;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		
		int evenCount = 0;
		int oddCount = 0;
		int positiveCount = 0;
		int negativeCount = 0;
		
		for(int i = 0; i < 5; i++){
		
			int val = s.nextInt();

			if(val > 0)
				positiveCount++;
			
			
			else if(val < 0)
				negativeCount++;	
				
			
			if(val % 2 == 0)
				evenCount++;
			
			else
				oddCount++;
		
		}
		
		System.out.format("%d valor(es) par(es)%n%d valor(es) impar(es)%n"
		+"%d valor(es) positivo(s)%n%d valor(es) negativo(s)%n", evenCount, 
		oddCount, positiveCount, negativeCount);
	
	}

}
