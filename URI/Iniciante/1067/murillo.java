import java.io.IOException;
import java.util.Scanner;
import java.lang.StringBuilder;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		int n = new Scanner(System.in).nextInt();

		StringBuilder sb = new StringBuilder();		
		
		for(int i = 1; i <= n; i += 2){
			sb.append(i).append(System.lineSeparator());
		}
		
		System.out.print(sb.toString());
	
	}

}
