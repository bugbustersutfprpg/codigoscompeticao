import java.io.*;
import java.util.StringTokenizer;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		int a = Integer.parseInt(st.nextToken());
		int count = 0;
		
		for(int i = a; count < 6; i++){
		
			if(i % 2 != 0){
				count++;
				out.write((i + System.lineSeparator()).getBytes());
			}
				
		
		}
		
		out.flush();
		
		
	
	}

}
