#include <stdio.h>

int main(){

    int a, b, c=0, u;
    scanf("%d %d", &a, &b);
    if (a>b){
        u=a;
        a=b;
        b=u;
    }
    a++;

    if (a%2==0)
        a++;

    for ( ; a<b; a+=2)
        c += a;

    printf("%d\n", c);

    return 0;
}
