import java.io.*;
import java.util.Scanner;
public class murillo{

	public static void main(String[] args) throws IOException{
	
		Scanner s = new Scanner(System.in);
		
		int a = s.nextInt();
		int b = s.nextInt();
		
		int lesser = Math.min(a, b) + 1;
		int greater = Math.max(a, b);
		
		int val = 0;

		if(lesser % 2 == 0)
			lesser++;
		
		for(int i = lesser; i < greater; i += 2)		
			val += i;
		
		
		
		System.out.println(val);
	
	}

}
