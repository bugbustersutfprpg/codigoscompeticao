import java.io.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		
		int a;
		int numIn = 0;
		int numOut = 0;
		
		for(int i = 0; i < n; i++){
		
			a = Integer.parseInt(br.readLine());
			if(a >= 10 && a <= 20)
				numIn++;
			else
				numOut++;
		}
		
		System.out.println(numIn + " in");
		System.out.println(numOut + " out");
	
	}

}
