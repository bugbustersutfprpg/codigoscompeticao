import java.io.*;
import java.lang.*;
import java.util.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		
		int n = Integer.parseInt(br.readLine());
		
		for(int i = 2; i <= n; i+= 2){
			sb.append(i).append("^2 = ").append(i*i).append(System.lineSeparator());
		}
		
		System.out.print(sb.toString());
	
	}

}
