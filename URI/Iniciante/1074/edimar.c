#include <stdio.h>

int main(){

    int a, b, c;
    scanf("%d", &a);
    char nome[6][10] = {"", "NULL", "EVEN", "ODD", " POSITIVE", " NEGATIVE"};

    int d[a], e[a];

    for (b=0 ;b<a ;b++ ){
        scanf("%d", &c);
        if (c==0){
            d[b]=1;
            e[b]=0;
            continue;
        }
        if (c%2==0){
            d[b]=2;
        }else{
            d[b]=3;
        }
        if (c>0){
            e[b]=4;
        }else{
            e[b]=5;
        }
    }
    for (b=0 ;b<a ;b++ ){
        printf("%s%s\n", nome[d[b]], nome[e[b]]);
    }

    return 0;
}
