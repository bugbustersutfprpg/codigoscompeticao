import java.io.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		
		int a = Integer.parseInt(br.readLine());
		int b;
		
		for(int i = 0; i < a; i++){
			b = Integer.parseInt(br.readLine());

			if(b == 0){
				sb.append("NULL").append(System.lineSeparator());
				continue;
			}
			
			if(b % 2 == 0)
				sb.append("EVEN ");
			else
				sb.append("ODD ");
				
			if(b > 0)
				sb.append("POSITIVE");
			else
				sb.append("NEGATIVE");
			
			sb.append(System.lineSeparator());
		}
		
		System.out.print(sb.toString());
	
	}

}
