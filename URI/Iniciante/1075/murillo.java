import java.io.*;
import java.util.Scanner;
import java.lang.StringBuilder;

class murillo{

	public static void main(String[] args) throws IOException{
	
		int n = new Scanner(System.in).nextInt();
		StringBuilder sb = new StringBuilder();
		
		for(int i = 2; i < 1e4; i += n)
			sb.append(i).append(System.lineSeparator());
			
		System.out.print(sb.toString());		
	
	}

}
