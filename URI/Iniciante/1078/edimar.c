#include <stdio.h>

int main(){

    int a, b;
    scanf("%d", &a);

    for (b=1; b<11; b++){
        printf("%d x %d = %d\n", b, a, b*a);
    }

    return 0;
}
