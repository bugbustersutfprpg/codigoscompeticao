import java.io.*;
import java.util.*;
import java.lang.StringBuilder;

public class murillo{

	public static void main(String args[]) throws IOException{
	
		int n = new Scanner(System.in).nextInt();
		StringBuilder sb = new StringBuilder();
		
		for(int i = 1; i <= 10; i++){
			sb.append(i).append(" x ").append(n).append(" = ").append(i*n)
			.append(System.lineSeparator());
			
		}
		
		System.out.print(sb.toString());
	
	}

}
