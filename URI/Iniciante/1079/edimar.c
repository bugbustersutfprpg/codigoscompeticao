#include <stdio.h>

int main(){

    int a, i;
    float b, c, d;
    scanf ("%d", &a);
    float t[a];

    for (i=0 ;i<a ;i++ ){
        scanf("%f %f %f", &b, &c, &d);
        t[i]= (b*2 + c*3 + d*5)/10;
    }

    for (i=0; i<a; i++){
        printf("%.1f\n", t[i]);
    }

    return 0;
}
