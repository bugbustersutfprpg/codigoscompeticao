import java.io.*;
import java.util.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		StringBuilder sb = new StringBuilder();
		int n = Integer.parseInt(br.readLine());
		
		double a, b, c;
		
		while(n-- > 0){
		
			st = new StringTokenizer(br.readLine());
			a = Double.parseDouble(st.nextToken());
			b = Double.parseDouble(st.nextToken());
			c = Double.parseDouble(st.nextToken());					
			sb.append(String.format(Locale.US, "%.1f%n", (2d * a + 3d * b + 5d * c) / 10d));
			
		}
		
		System.out.print(sb.toString());
	
	}

}

