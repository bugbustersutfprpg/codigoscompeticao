#include <stdio.h>

int main(){

    int a, b, i, p=0;
    scanf("%d", &a);
    
    for (i=1; i<1000; i++){
        scanf("%d", &b);
        if (a<b){
            a=b;
            p=i;
        }
    }
    
    printf("%d\n%d\n", a, p+1);

    return 0;
}
