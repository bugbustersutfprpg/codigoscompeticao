import java.io.*;
import java.util.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		int n;
		int greater = 0;
		int greaterPos = 1;
		
		for(int i = 1; i <= 100; i++){
			n = Integer.parseInt(br.readLine());
			if(greater < n){
				greaterPos = i;
				greater = n;
			}
			
		}
		
		System.out.print(greater + System.lineSeparator() + greaterPos + System.lineSeparator());
	
	}

}

