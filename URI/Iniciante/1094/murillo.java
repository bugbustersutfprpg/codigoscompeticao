import java.io.*;
import java.util.*;
import java.lang.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		StringBuilder sb = new StringBuilder();
		
		int n = Integer.parseInt(br.readLine());
		int a;
		String t;
		
		int countC = 0;
		int countR = 0;
		int countS = 0;
		int total = 0;
		
		while(n-- > 0){
		
			st = new StringTokenizer(br.readLine());
			
			a = Integer.parseInt(st.nextToken());
			t = st.nextToken();
			
			switch(t){
			
				case "R":
					countR += a;
					break;
					
				case "C":
					countC += a;
					break;
					
				default:
					countS += a;
			
			}
			
			total += a;
		
		}
		
		sb.append("Total: ").append(total).append(" cobaias").append(System.lineSeparator())
		.append("Total de coelhos: ").append(countC).append(System.lineSeparator())
		.append("Total de ratos: ").append(countR).append(System.lineSeparator())
		.append("Total de sapos: ").append(countS).append(System.lineSeparator())
		.append("Percentual de coelhos: ").append(String.format(Locale.US, "%.2f %%%n", countC / (double) total * 100d))
		.append("Percentual de ratos: ").append(String.format(Locale.US, "%.2f %%%n", countR / (double) total * 100d ))
		.append("Percentual de sapos: ").append(String.format(Locale.US, "%.2f %%%n", countS / (double) total * 100d ));
		
		System.out.print(sb.toString());
	
	}

}
