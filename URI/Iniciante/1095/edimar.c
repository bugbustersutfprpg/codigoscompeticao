#include <stdio.h>

int main(){

    int a, b, n, t=0, tr=0, tc=0, ts=0;
    char c;
    scanf ("%d", &a);

    for (b=0; b<a; b++){
        scanf("%d %c", &n, &c);
        t += n;
        if (c=='S'){
            ts += n;
            continue;
        }
        if (c=='R'){
            tr += n;
        }else{
            tc += n;
        }
    }

    printf("Total: %d cobaias\n", t);
    printf("Total de coelhos: %d\n", tc);
    printf("Total de ratos: %d\n", tr);
    printf("Total de sapos: %d\n", ts);
    printf("Percentual de coelhos: %.2f %\n", (float)tc/t*100);
    printf("Percentual de ratos: %.2f %\n", (float)tr/t*100);
    printf("Percentual de sapos: %.2f %\n", (float)ts/t*100);

    return 0;
}
