import java.io.*;
import java.lang.StringBuilder;

class murillo{

	public static void main(String[] args) throws IOException{
	
		StringBuilder sb = new StringBuilder();
	
		int k = 1;

		for(int i = 0; i <= 2; i++){
		
			sb.append("I=").append(i).append(" J=").append(k).append(System.lineSeparator());
			sb.append("I=").append(i).append(" J=").append(k+1).append(System.lineSeparator());
			sb.append("I=").append(i).append(" J=").append(k+2).append(System.lineSeparator());
			
			if(i == 2)
				break;
			
			for(int j = 2; j < 10; j+= 2){
			
				sb.append("I=").append(i).append(".").append(j)
				.append(" J=").append(k).append(".").append(j).
				append(System.lineSeparator());
				sb.append("I=").append(i).append(".").append(j)
				.append(" J=").append(k+1).append(".").append(j).
				append(System.lineSeparator());
				sb.append("I=").append(i).append(".").append(j)
				.append(" J=").append(k+2).append(".").append(j).
				append(System.lineSeparator());
		
			}

			k++;	
		}
		
		System.out.print(sb.toString());
	
	}

}
