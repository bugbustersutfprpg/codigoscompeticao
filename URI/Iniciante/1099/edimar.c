#include <stdio.h>

int main(){

    int a, b, n, i, u;
    scanf ("%d", &n);
    int soma[n];

    for (i=0; i<n; i++){
        soma[i]=0;
        scanf ("%d %d", &a, &b);
        if (a>b){
            u=a;
            a=b;
            b=u;
        }
        a++;
        if (a%2==0)
            a++;
        for ( ; a<b; a+=2)
            soma[i] += a;
    }

    for (i=0; i<n; i++){
        printf("%d\n", soma[i]);
    }

    return 0;
}
