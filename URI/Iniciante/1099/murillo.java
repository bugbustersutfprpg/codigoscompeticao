import java.util.*;
import java.lang.*;
import java.io.*;
public class murillo{

	public static void main(String[] args) throws IOException{
	
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		
		int n = Integer.parseInt(br.readLine());
		int a, b;
		int x, y;
		int total;
		String[] input;
		
		while(n-- > 0){
		
			input = br.readLine().split(" ");
			a = Integer.parseInt(input[0]);
			b = Integer.parseInt(input[1]);
			
			x = Math.min(a,b) + 1;
			y = Math.max(a,b);
			
			if(x % 2 == 0)
				x++;
				
			total = 0;
			for(int i = x; i < y; i+=2){
				total += i;
			}
			
			sb.append(total).append(System.lineSeparator());
			
		}
		
		System.out.print(sb.toString());
	
	}

}
