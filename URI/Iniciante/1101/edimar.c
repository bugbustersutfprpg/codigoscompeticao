#include <stdio.h>

int main(){

    int a[100], b[100], i, u, j=0, t;
    while(1){
        scanf("%d %d", &a[j], &b[j]);
        if ((a[j]<1) || (b[j]<1)){
            break;
        }
        if(a[j]>b[j]){
            u=a[j];
            a[j]=b[j];
            b[j]=u;
        }
        j++;
    }

    for (u=0 ;u<j ;u++ ){
        t=0;
        for (i= a[u] ;i<=b[u] ;i++ ){
            printf("%d ", i);
            t += i;
        }
        printf("Sum=%d\n", t);
    }

    return 0;
}
