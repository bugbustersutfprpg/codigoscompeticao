import java.io.*;
import java.util.*;
import java.lang.*;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		StringTokenizer st;
		
		int a, b, x, y;
		int total;
		while(true){
		
			st = new StringTokenizer(br.readLine());
			a = Integer.parseInt(st.nextToken());
			b = Integer.parseInt(st.nextToken());
			
			x = Math.min(a,b);
			y = Math.max(a,b);
			
			if(x <= 0 || y <= 0)
				break;
				
			total = (y-x+1)*(y+x)/2;
			
			for(int i = x; i <= y; i++)
				sb.append(i).append(" ");
				
			sb.append("Sum=").append(total).append(System.lineSeparator());
		
		}
		
		System.out.print(sb.toString());
		
		
	
	}

}
