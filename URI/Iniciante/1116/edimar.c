#include <stdio.h>

int main(){

    int a, b, d, i;
    scanf("%d", &a);
    float c[a];

    for (i=0; i<a; i++){
        scanf("%d %d", &b, &d);
        if (d==0){
            c[i] = 1234567;
            continue;
        }
        c[i] = (float) b/d;
    }

    for (i=0; i<a; i++){
        if (c[i]==1234567){
            puts("divisao impossivel");
            continue;
        }
        printf("%.1f\n", c[i]);
    }

    return 0;
}
