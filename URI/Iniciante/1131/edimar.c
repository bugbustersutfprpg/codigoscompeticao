#include <stdio.h>

int main(){

    int a, b, partida=0, d=1, inter=0, gremio=0, empate=0;

    while (d){
        scanf("%d %d", &a, &b);
        partida++;
        if (a>b){
            inter++;
        }else{
            if (a<b){
                gremio++;
            }else{
                empate++;
            }
        }
        puts("Novo grenal (1-sim 2-nao)");
        scanf("%d", &d);
        if (d!=1) break;
    }

    printf("%d grenais\nInter:%d\nGremio:%d\nEmpates:%d\n", partida, inter, gremio, empate);
    if (inter>gremio){
        puts("Inter venceu mais");
    }else {
        if (inter<gremio){
            puts("Gremio venceu mais");
        }else{
            puts("Nao houve vencedor");
        }
    }


    return 0;
}
