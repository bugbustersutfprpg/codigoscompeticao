#include <stdio.h>

int main(){

    int a, i=1;
    scanf("%d", &a);

    for ( ; a>0; a--){
        printf("%d %d %d\n", i, i*i, i*i*i);
        printf("%d %d %d\n", i, i*i+1, i*i*i+1);
        i++;
    }

    return 0;
}
