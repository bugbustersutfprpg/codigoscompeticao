#include <stdio.h>

int main(){

    int a, n, t=0;
    scanf("%d %d", &a, &n);
    if (n<1){
        while(1){
            scanf("%d", &n);
            if(n>0) break;
        }
    }
    while (n>0){
        t += a++;
        n--;
    }
    printf("%d\n", t);

    return 0;
}
