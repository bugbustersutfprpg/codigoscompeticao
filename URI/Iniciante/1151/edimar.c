#include <stdio.h>

int main(){

    int x, i=1, t=0, u;

    scanf("%d", &x);

    if(x==1){
        puts("0");
        return 0;
    }
    x--;
    x--;
    putchar('0');
    putchar(' ');
    putchar('1');
    while(x-->0){
        printf(" %d", t+i);
        u=t;
        t=i;
        i=u+t;
    }
    puts("");

    return 0;
}
