#include <stdio.h>

int main(){

    int x, t=0, c=0;

    while(1){
        scanf("%d", &x);
        if (x<0) break;
        t += x;
        c++;
    }
    printf("%.2f\n", (float)t/c);


    return 0;
}
