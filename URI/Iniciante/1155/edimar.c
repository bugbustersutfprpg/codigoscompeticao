#include <stdio.h>

int main(){

    float x=2;
    float t=1.0;

    while(x<101){
        t += (1/x);
        x++;
    }
    printf("%.2f\n", t);

    return 0;
}
