#include <stdio.h>

int main(){

    float x=2, y=3;
    float t=1.0;

    while(y<40){
        t += (y/x);
        x *= 2;
        y += 2;
    }
    printf("%.2f\n", t);

    return 0;
}
