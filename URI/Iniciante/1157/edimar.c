#include <stdio.h>

int main(){

    int i, j, x, k, s, b, ex, exp, to=1;

    while (1){
        scanf("%d", &x);
        if (x==0) break;

        if (x==1) {
            puts("1\n");
            continue;
        }
        b = 1;
        to = 1;
        exp = (x-1)*2;
        for (i=0; i<exp; i++){
            to *= 2;
        }

        exp = 1;
        for(i=0; to>9; to /= 10){
            exp++;
        }


        k = 1;
        s = x-1;
        for (i=0; i<x; i++){
            k = b;
            for(j=0; j<s; j++){
                ex = 1;
                for(to=k; to>9; to /= 10){
                    ex++;
                }
                for (ex= exp-ex ;ex>0 ;ex--){
                    printf(" ");
                }
                printf("%d ", k);
                k *= 2;
            }
            ex = 1;
            for(to=k; to>9; to /= 10){
                ex++;
            }
            for (ex= exp-ex ;ex>0 ;ex--){
                printf(" ");
            }
            printf("%d\n", k);
            b *= 2;
        }

        puts("");
    }

    return 0;
}
