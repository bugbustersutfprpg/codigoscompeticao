#include <stdio.h>

int main(){

    int n, x, y, i;
    scanf("%d", &n);
    int t[n];

    for (i=0; i<n; i++) t[i]=0;

    for (i=0; i<n; i++){
        scanf("%d %d", &x, &y);
        if (x%2==0) x++;
        for( ;y-->0 ;x += 2){
            t[i] += x;
        }
    }

    for (i=0; i<n; i++){
        printf("%d\n", t[i]);
    }

    return 0;
}
