#include <stdio.h>

int main(){

    int a, b, i, n;
    float x, y;

    scanf("%d", &n);
    int ano[n];
    for (i=0; i<n; i++) ano[i]=0;

    for (i=0; i<n; i++){
        scanf("%d %d %f %f", &a, &b, &x, &y);
        while (a<=b){
            a *= (1+x/100);
            b *= (1+y/100);
            ano[i]++;
            if (ano[i]>100) break;
        }
    }

    for (i=0; i<n; i++){
        if (ano[i]>100){
            puts("Mais de 1 seculo.");
            continue;
        }
        printf("%d anos.\n", ano[i]);
    }

    return 0;
}
