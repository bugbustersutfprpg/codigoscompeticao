#include <stdio.h>

int main(){

    int a[20];
    int i;

    for (i=0; i<20; i++){
        scanf("%d", &a[i]);
    }

    for (i=19; i>-1; i--){
        printf("N[%d] = %d\n", 19-i, a[i]);
    }

    return 0;
}
