#include <stdio.h>

int main(){

    int j, n;

    scanf("%d", &n);
    int xx[n];
    unsigned long long int to[n], x, i, t, u;

    for(j=0; j<n; j++){
        t=0; i=1;
        scanf("%llu", &x);

        xx[j] = x;
        if(x==0){
            to[j]=0;
            continue;
        }
        x--;
        while(x-->0){
            u=t;
            t=i;
            i=u+t;
        }
        to[j] = i;
    }

    for(j=0; j<n; j++){
        printf("Fib(%d) = %llu\n", xx[j], to[j]);
    }

    return 0;
}
