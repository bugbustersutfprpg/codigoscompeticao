#include <stdio.h>

int main(){

    int i, j, li;
    char c;
    float m[12][12], t=0.0;

    scanf("%d ", &li);
    scanf("%c", &c);

    for (i=0; i<12; i++){
        for (j=0; j<12; j++){
            scanf("%f", &m[i][j]);
        }
    }

    for (j=0; j<12; j++){
        t += m[j][li];
    }

    if (c=='S'){
        printf("%.1f\n", t);
    }else{
        printf("%.1f\n", t/12);
    }

    return 0;
}
