#include <stdio.h>

int main(){

    int i, j, d;
    char c;
    float m[12][12], t=0.0;

    scanf("%c", &c);

    for (i=0; i<12; i++){
        for (j=0; j<12; j++){
            scanf(" %f", &m[i][j]);
        }
    }
    d=1;
    for (i=0; i<11; i++){
        for (j=d; j<12; j++){
            t += m[i][j];
        }
        d++;
    }

    if (c=='S'){
        printf("%.1f\n", t);
    }else{
        printf("%.1f\n", t/66);
    }

    return 0;
}
