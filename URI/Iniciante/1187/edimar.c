#include <stdio.h>

int main(){

    int i, j, d;
    char c;
    double m[12][12], t=0.0;

    scanf("%c", &c);

    for (i=0; i<12; i++){
        for (j=0; j<12; j++){
            scanf("%lf", &m[i][j]);
        }
    }
    d=1;
    for (i=0; i<5; i++){
        for (j=d; j<(12-d); j++){
            t += m[i][j];
        }
        d++;
    }

    if (c=='S'){
        printf("%.1lf\n", t);
    }else{
        printf("%.1lf\n", t/30);
    }

    return 0;
}
