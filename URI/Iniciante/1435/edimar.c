#include <stdio.h>

int main(){

    int i, j, x, k, s, b;

    while (1){
        k=1;
        scanf("%d", &x);
        if (x==0) break;

        if (x==1) {
            puts("  1\n");
            continue;
        }
        if (x==2){
            puts("  1   1\n  1   1\n");
            continue;
        }
        if (x==3){
            puts("  1   1   1\n  1   2   1\n  1   1   1\n");
            continue;
        }

        s = x-1;
        for (i=0; i<s; i++){
            printf("  1 ");
        }
        puts("  1");
        printf("  1 ");
        for (i=1; i<s; i++){
            printf("  2 ");
        }
        puts("  1");

        b = s-1;
        for (i=2; i<b; i++){
            k=1;
            for (j=0; ; ){
                printf("%3d ", k);
                j++;
                if (j==s) {
                    puts("  1");
                    break;
                }
                if ( (j<=i) && (x-j>i) ){
                    k++;
                    continue;
                }
                if ( (j>i) && ((x-j)<=i) ) k--;
            }
        }

        printf("  1 ");
        for (i=1; i<s; i++){
            printf("  2 ");
        }
        puts("  1");

        for (i=0; i<s; i++){
            printf("  1 ");
        }
        puts("  1");

        puts("");
    }

    return 0;
}
