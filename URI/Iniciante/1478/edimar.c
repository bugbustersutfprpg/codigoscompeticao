#include <stdio.h>

int main(){

    int i, j, x, k, s, b;

    while (1){
        k=1;
        scanf("%d", &x);
        if (x==0) break;

        if (x==1) {
            puts("  1\n");
            continue;
        }
        b = 1;
        s = x-1;
        for (i=0; i<x; i++){
            k = b++;
            for (j=0; ; ){
                printf("%3d ", k);
                j++;
                if (j==s) {
                    if (i==s){
                        printf("%3d\n", k-1);
                        break;
                    }
                    printf("%3d\n", k+1);
                    break;
                }
                if (j<=i) {
                    k--;
                }else{
                    k++;
                }
            }
        }

        puts("");
    }

    return 0;
}
