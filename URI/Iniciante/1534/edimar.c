#include <stdio.h>

int main(){

    int a, i, j, x;

    while(1){
        a = scanf("%d", &x);
        if (a==-1){
            break;
        }

        for (i=0; i<x; i++){
            for (j=0; j<x; j++){
                if ((x-j-1)==i){
                    putchar('2');
                    continue;
                }
                if (i==j){
                    putchar('1');
                    continue;
                }
                putchar('3');
            }
            puts("");
        }
    }

    return 0;
}
