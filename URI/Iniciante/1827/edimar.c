#include <stdio.h>

int main(){

    int a, i, j, n, x, m;

    while(scanf("%d", &x)!=-1){

        n= x/2;
        m= (x+1)/6+1;

        for (i=0; i<x; i++){
            for (j=0; j<x; j++){
                if ( (i==j) && (i==n) ){
                    putchar('4');
                    continue;
                }
                if ( (i>(n-m)) && (i<(n+m)) && (j>(n-m)) && (j<(n+m))) {
                    putchar('1');
                    continue;
                }

                if ((x-j-1)==i){
                    putchar('3');
                    continue;
                }
                if (i==j){
                    putchar('2');
                    continue;
                }
                putchar('0');
            }
            puts("");
        }
        puts("");
    }

    return 0;
}
