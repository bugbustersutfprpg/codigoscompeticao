#include <stdio.h>

int main(){

    int i, t;
    char a[8], b[8];

    scanf("%d", &t);
    
    for (i=0; i<t; i++){
        scanf("%s %s", a, b);
        
        if (a[2]==b[2]){
            printf("Caso #%d: De novo!\n", i+1);
            continue;
        }
        
        if (a[2]=='d'){
            if ( (b[2]=='g') || (b[2]=='s') ){
                printf("Caso #%d: Bazinga!\n", i+1);
            }else{
                printf("Caso #%d: Raj trapaceou!\n", i+1);
            }
            continue;
        }
        if (a[2]=='p'){
            if ( (b[2]=='d') || (b[2]=='o') ){
                printf("Caso #%d: Bazinga!\n", i+1);
            }else{
                printf("Caso #%d: Raj trapaceou!\n", i+1);
            }
            continue;
        }
        if (a[2]=='s'){
            if ( (b[2]=='p') || (b[2]=='g') ){
                printf("Caso #%d: Bazinga!\n", i+1);
            }else{
                printf("Caso #%d: Raj trapaceou!\n", i+1);
            }
            continue;
        }
        if (a[2]=='o'){
            if ( (b[2]=='d') || (b[2]=='s') ){
                printf("Caso #%d: Bazinga!\n", i+1);
            }else{
                printf("Caso #%d: Raj trapaceou!\n", i+1);
            }
            continue;
        }

        if ( (b[2]=='o') || (b[2]=='p') ){
            printf("Caso #%d: Bazinga!\n", i+1);
        }else{
            printf("Caso #%d: Raj trapaceou!\n", i+1);
        }
    }

    return 0;
}

