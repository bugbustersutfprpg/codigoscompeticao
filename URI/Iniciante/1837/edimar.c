#include <stdio.h>

int main(){

    int a, b, q, r;

    scanf("%d %d", &a, &b);
    if (b==0) return 0;

    q= a/b;
    r= a%b;

    if (r<0){
        r += abs(b);
        if (q<0){
            q--;
        }else{
            if (q==0){
                if (b>0){
                    q--;
                }else{
                    q++;
                }
            }else{
                q++;
            }
        }
    }
    printf("%d %d\n", q, r);

    return 0;
}
