#include <stdio.h>

int main() {

    int a, b, c, d;

    scanf("%d %d %d", &a, &b, &c);

    if (a<b){
        d = b - a;
        d = b + d;
        if (c<d){
            puts(":(");
        }else{
            puts(":)");
        }
    }else{
        d = a - b;
        d = b - d;
        if (c>d){
            puts(":)");
        }else{
            puts(":(");
        }
    }
    return 0;
}
