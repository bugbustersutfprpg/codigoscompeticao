#include <stdio.h>

int main() {

    int a=3, n[3];
    char s[8], d;

    while(a--){
        n[a] = 0;
        while(1){
            scanf("%[^\n]", s);
            d=getchar();

            if (s[0]=='c') break;

            if (s[2]=='*') n[a] += 1;
            if (s[1]=='*') n[a] += 2;
            if (s[0]=='*') n[a] += 4;
        }
    }

    printf("%d\n%d\n%d\n", n[2], n[1], n[0]);

    return 0;
}
