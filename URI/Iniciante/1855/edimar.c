#include <stdio.h>

int main() {

    int x=0, y=0, a, b, i=0, direcao, max;
    scanf("%d", &a);
    scanf("%d", &b);

    max = a*b;
    char mapa[b][a+1];;

    while(i < b) {
        scanf(" %[^\n]", mapa[i++]);
    }

    if (mapa[y][x]=='>') {
        direcao=0;
        x++;
    }else{
        if (mapa[y][x]=='v'){
            direcao=1;
            y++;
        }else {
            puts("!");
            return 0;
        }
    }
    a--;
    b--;

    while(max--){
        if ( (x>a) || (x<0) || (y>b) || (y<0) ) {
            puts("!");
            return 0;
        }
        if (mapa[y][x]=='*') {
            puts("*");
            return 0;
        }
        if (mapa[y][x]=='.'){
            if (direcao == 0) {
                x++;
                continue;
            }
            if (direcao == 1) {
                y++;
                continue;
            }
            if (direcao == 2) {
                x--;
                continue;
            }
            else{
                y--;
                continue;
            }
        }
        if (mapa[y][x]=='>'){
            if (direcao==2){
                puts("!");
                return 0;
            }
            direcao = 0;
            x++;
            continue;
        }
        if (mapa[y][x]=='v'){
            if (direcao==3){
                puts("!");
                return 0;
            }
            direcao = 1;
            y++;
            continue;
        }
        if (mapa[y][x]=='<'){
            if (direcao==0){
                puts("!");
                return 0;
            }
            direcao = 2;
            x--;
            continue;
        }
        if (mapa[y][x]=='^'){
            if (direcao==1){
                puts("!");
                return 0;
            }
            direcao = 3;
            y--;
            continue;
        }else{
            puts("!");
            return 0;
        }
    }
    puts("!");
    return 0;
}
