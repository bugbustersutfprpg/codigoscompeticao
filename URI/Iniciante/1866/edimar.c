#include <stdio.h>

int main() {

    int c, i, j;
    scanf("%d", &c);

    for (i=0; i<c; i++){
        scanf(" %d", &j);
        if (j%2==0) puts("0");
        else puts("1");
    }

    return 0;
}
