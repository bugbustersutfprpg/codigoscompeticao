#include <stdio.h>

void main(){

    long long int n, valor1, valor2;

    char nome1[101], nome2[101], escolha[6], descarte[6];
    scanf("%d", &n);

    while(n--){

        scanf("%s %s %s %s", nome1, escolha, nome2, descarte);
        scanf("%ld %ld", &valor1, &valor2);

        if ( (valor1%2==0 && valor2%2==0) || (valor1%2!=0 && valor2%2!=0) ){
            if (escolha[0]=='P'){
                printf("%s\n", nome1);
            }else
                printf("%s\n", nome2);
        }else{
            if (escolha[0]=='I'){
                printf("%s\n", nome1);
            }else
                printf("%s\n", nome2);
        }
    }

}
