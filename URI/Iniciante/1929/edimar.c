#include <stdio.h>

void ordenar(int v[]){

    int menor, i, j=0, aux, indice;

    while(j<3){
        menor = v[j];
        indice = i = j;
        while(i<3){
            if (v[++i] < menor){
                menor = v[i];
                indice = i;
            }
        }
        aux = v[j];
        v[j++] = menor;
        v[indice] = aux;
    }
}

void main(){

    int v[4];

    scanf("%d %d %d %d", &v[0], &v[1], &v[2], &v[3]);

    ordenar(v);

    if ( (v[0] + v[1]) > v[2] || (v[1] + v[2]) > v[3]) {
        puts("S");
    }else
        puts("N");
}
