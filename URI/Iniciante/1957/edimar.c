#include <stdio.h>
#include <math.h>

void main(){

    int a, i=7, j=0, cont;
    char s[15];

    scanf("%d", &a);

    while(i){
        int b = pow(16, i--);
        cont = 0;
        while(a >= b){
            cont++;
            a -= b;
        }
        if (!cont && !j) {
            continue;
        }
        if (cont>9){
            s[j] = cont + 55;
        }else
            s[j] = cont + 48;
        j++;
    }
    if (a>9){
        s[j] = a + 55;
    }else
        s[j] = a + 48;
    s[j+1] = 0;
    
    printf("%s\n", s);
}
