#include <stdio.h>

void main(){

    double n;
    char s[15];
    scanf("%lf", &n);

    if (!n){
        sprintf(s, "%lf", n);
        if (s[0] == '-')
            printf("-0.0000E+00\n");
        else
            printf("+0.0000E+00\n");
        return;
    }

    int cont=0, negativo=0;

    if (n<0) negativo = 1;

    if (negativo){
        if (n <= -1){
            while(n <= -10000000000){
                n /= 10000000;
                cont += 7;
            }
            while(n <= -10){
                n /= 10;
                cont++;
            }
            printf("%.4lfE+%02d\n", n, cont);
        }else{
            while(n>-0.0000000001){
                n *= 10000000;
                cont += 7;
            }
            while(n>-1){
                n *= 10;
                cont++;
            }
            printf("%.4lfE-%02d\n", n, cont);
        }
    }else{
        if (n >= 1){
            while(n > 10000000000){
                n /= 10000000;
                cont += 7;
            }
            while(n >= 10){
                n /= 10;
                cont++;
            }
            printf("+%.4lfE+%02d\n", n, cont);
        }else{
            while(n < 0.0000000001){
                n *= 10000000;
                cont += 7;
            }
            while(n < 1){
                n *= 10;
                cont++;
            }
            printf("+%.4lfE-%02d\n", n, cont);
        }
    }
}
