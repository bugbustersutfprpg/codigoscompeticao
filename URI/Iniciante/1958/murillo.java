
	import java.io.BufferedReader;
	import java.io.InputStreamReader;
	import java.io.BufferedOutputStream;
	import java.io.IOException;
	import java.math.MathContext;
	import java.math.BigDecimal;
	import java.text.DecimalFormat;

	public class Main{

		public static void main(String[] args) throws IOException{

			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			BufferedOutputStream out = new BufferedOutputStream(System.out);
			String text = br.readLine();
			boolean saveSign = false;

			if(text.matches("\\-0*\\.?0*"))
				saveSign = true;
		
			BigDecimal input = new BigDecimal(text);
			DecimalFormat df = new DecimalFormat("0.0000E00");
			df.setPositivePrefix("+");
			text = df.format(input).replaceFirst(",", ".");
			String[] texts = text.split("E");
			texts[1] = texts[1].startsWith("-") ? texts[1] : "+" + texts[1];
			text = texts[0] + "E" + texts[1];

			if(saveSign)
				text = text.replaceFirst("\\+", "-");
			
			out.write(text.getBytes());
			out.write(System.lineSeparator().getBytes());
			out.flush();
			out.close();	
		
	
		}


	}
