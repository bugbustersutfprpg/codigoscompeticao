#include <stdio.h>
#include <string.h>

void main(){

    int n, cont=0;
    char s[15];
    char romano[30][5] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "", "X", "XX", "XXX", "XL", "L", "LX",
                            "LXX", "LXXX", "XC", "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};

    scanf("%d", &n);

    while (n>99){
        n -= 100;
        cont++;
    }
    strcpy(s, romano[cont+20]);

    cont = 0;
    while (n>9){
        n -= 10;
        cont++;
    }
    strcat(s, romano[cont+10]);

    cont = 0;
    while (n){
        n--;
        cont++;
    }
    strcat(s, romano[cont]);

    printf("%s\n", s);
}
