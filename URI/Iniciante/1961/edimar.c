#include <stdio.h>


void main(){

    int pulo, n, cano1, cano2, altura;

    scanf("%d %d %d", &pulo, &n, &cano1);

    while(--n){
        scanf("%d", &cano2);

        altura = abs(cano1-cano2);

        if (altura > pulo){
            printf("GAME OVER\n");
            return;
        }
        cano1 = cano2;
    }

    printf("YOU WIN\n");
}

