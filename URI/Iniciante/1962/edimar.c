#include <stdio.h>


void main(){

    int n, ano;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &ano);

        if (ano>2014){
            printf("%d A.C.\n", ano-2014);
        }else{
            printf("%d D.C.\n", 2015-ano);
        }

    }

}
