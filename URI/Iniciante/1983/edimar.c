#include <stdio.h>


void main(){

    int n, a, b;
    float nota1, nota2;

    scanf("%d", &n);
    scanf("%d %f", &a, &nota1);

    while(--n){
        scanf("%d %f", &b, &nota2);

        if (nota2>nota1){
            nota1 = nota2;
            a = b;
        }
    }
    if (nota1<8){
        printf("Minimum note not reached\n");
    }else
        printf("%d\n", a);
}
