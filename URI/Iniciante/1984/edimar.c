#include <stdio.h>
#include <string.h>

void main(){

    int t;
    char s[15];

    scanf("%s", s);

    t = strlen(s);

    while(t--){
        printf("%c", s[t]);
    }
    puts("");

}
