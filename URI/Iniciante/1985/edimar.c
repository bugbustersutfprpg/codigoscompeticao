#include <stdio.h>


void main(){

    int n, quant;
    float produto, soma=0;

    scanf("%d", &n);

    while(n--){
        scanf("%f %d", &produto, &quant);
        soma += quant * (produto-999.5);
    }

    printf("%.2f\n", soma);
}
