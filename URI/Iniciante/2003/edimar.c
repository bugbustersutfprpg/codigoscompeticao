#include <stdio.h>


void main(){

    int atraso;
    char s[5];

    while(scanf("%s", s) != -1){
        
        atraso = 0;
        if ( (s[0]-48) > 6){
            if (s[0]=='9')
                atraso = 120;
            else if (s[0]=='8')
                atraso = 60;

            atraso += (s[2]-48)*10;
            atraso += s[3]-48;

            printf("Atraso maximo: %d\n", atraso);
        }else{
            puts("Atraso maximo: 0");
        }
    }
}
