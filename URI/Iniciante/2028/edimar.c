#include <stdio.h>


void main(){

    int n, quantia, i = 1, j, k;

    while(scanf("%d", &n) != -1){
        if (n%2){
            quantia = (n+1)*(n/2)+(n/2)+2;
        }else{
            quantia = (n+1)*(n/2)+1;
        }

        if (quantia == 1)
            printf("Caso %d: %d numero\n", i++, quantia);
        else
            printf("Caso %d: %d numeros\n", i++, quantia);

        printf("0");

        j = 0;
        while(j <= n){
            k = j;
            while(k--){
                printf(" %d", j);
            }
            if (j == n){
                printf("\n\n");
            }
            j++;
        }
    }
}
