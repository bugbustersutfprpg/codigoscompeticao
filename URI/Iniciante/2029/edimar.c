#include <stdio.h>


void main(){

    double volume, diametro, altura, area;

    while(scanf("%lf %lf", &volume, &diametro) != -1){
        area = (diametro/2) * (diametro/2) * 3.14;
        altura = volume / area;

        printf("ALTURA = %.2lf\n", altura);
        printf("AREA = %.2lf\n", area);
    }
}
