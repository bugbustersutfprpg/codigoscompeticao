#include <stdio.h>


void main(){

    int saida, duracao, fuso, resposta;

    scanf("%d %d %d", &saida, &duracao, &fuso);

    resposta = saida + duracao + fuso;
    resposta = resposta%24;
    if (resposta < 0)
        resposta += 24;

    printf("%d\n", resposta);

}
