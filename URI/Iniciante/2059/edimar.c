#include <stdio.h>


void main(){

    int a, b, c, d, e, vencedor;
    scanf("%d %d %d %d %d", &a, &b, &c, &d, &e);

    if (e == 1){
        if (d == 1){
            vencedor = 2;
        }else{
            vencedor = 1;
        }
    }else if (d == 1){
        vencedor = 1;
    }else if ( (b + c)%2 == 0 ){
        if (a == 1){
            vencedor = 1;
        }else{
            vencedor = 2;
        }
    }else{
        if (a == 1){
            vencedor = 2;
        }else{
            vencedor = 1;
        }
    }

    printf("Jogador %d ganha!\n", vencedor);
}
