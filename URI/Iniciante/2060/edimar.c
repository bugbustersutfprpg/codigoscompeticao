#include <stdio.h>


void main(){

    int n, k, dois, tres, quatro, cinco;
    dois = 0;
    tres = 0;
    quatro = 0;
    cinco = 0;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &k);

        if (!(k%2))
            dois++;
        if (!(k%3))
            tres++;
        if (!(k%4))
            quatro++;
        if (!(k%5))
            cinco++;

    }
    printf("%d Multiplo(s) de 2\n", dois);
    printf("%d Multiplo(s) de 3\n", tres);
    printf("%d Multiplo(s) de 4\n", quatro);
    printf("%d Multiplo(s) de 5\n", cinco);
}
