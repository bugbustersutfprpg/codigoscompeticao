#include <stdio.h>


void main(){

    int n, m;
    char s[8];

    scanf("%d %d", &n, &m);

    while(m--){
        scanf("%s", s);
        if (s[0] == 'f')
            n++;
        else
            n--;
    }
    printf("%d\n", n);
}
