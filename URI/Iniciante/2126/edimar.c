#include <stdio.h>
#include <string.h>

void main(){

    char s[13], s1[35];
    int i, j, k, l, inicial, total, tam;
    k = 1;

    while(scanf("%s %s", s, s1) != -1){
        total = 0;
        tam = strlen(s);
        j = 0;
        i = 0;
        while(s1[j]){
            l = j;
            while(s[i] == s1[l]){
                if (!s1[l]){
                    break;
                }
                l++;
                i++;
            }
            if (!s[i]){
                total++;
                inicial = l - tam + 1;
            }
            j++;
            i = 0;
        }

        printf("Caso #%d:\n", k++);
        if (total){
            printf("Qtd.Subsequencias: %d\n", total);
            printf("Pos: %d\n\n", inicial);
        }else
            puts("Nao existe subsequencia\n");
    }

}
