#include <stdio.h>


void main(){

    int mes, dia, falta;

    while(scanf("%d %d", &mes, &dia) != -1 ){

        if (mes == 12 || mes == 11)
            falta = 0;
        else if (mes == 10 || mes == 9)
            falta = 1;
        else if (mes == 8)
            falta = 2;
        else if (mes == 7 || mes == 6)
            falta = 3;
        else if (mes == 5 || mes == 4 || mes == 2)
            falta = 4;
        else
            falta = 5;

        while(mes++ != 12){
            falta += 30;
        }
        falta += 25 - dia;

        if (falta < 0)
            puts("Ja passou!");
        else if (falta == 0)
            puts("E natal!");
        else if (falta == 1)
            puts("E vespera de natal!");
        else
            printf("Faltam %d dias para o natal!\n", falta);
    }
}
