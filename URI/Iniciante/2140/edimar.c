#include <stdio.h>


void main(){

    int compra, dinheiro, troco;

    while(1){
        scanf("%d %d", &compra, &dinheiro);
        
        if (!dinheiro)
            break;
    
        troco = dinheiro - compra;

        if (troco == 4 || troco == 7 || troco == 10 || troco == 12 || troco == 15 || troco == 20 || troco == 22
        || troco == 25 || troco == 30 || troco == 40 || troco == 52 || troco == 55 || troco == 60 || troco == 70
        || troco == 100 || troco == 102 || troco == 105 || troco == 110 || troco == 120 || troco == 150 || troco == 200)
            puts("possible");
        else
            puts("impossible");
    }
}
