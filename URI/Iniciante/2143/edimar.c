#include <stdio.h>


void main(){

    int n, qtde;

    while(1){
        scanf("%d", &n);
        if (!n)
            break;

        while(n--){
            scanf("%d", &qtde);
            if (qtde%2)
                printf("%d\n", qtde * 2 - 1);
            else
                printf("%d\n", qtde * 2 - 2);
        }
    }

}
