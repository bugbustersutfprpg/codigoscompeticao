#include <stdio.h>
#include <string.h>

void main(){

    int n;
    float tam;
    char s[10001];

    scanf("%d", &n);

    while(n--){
        scanf("%s", s);

        tam = strlen(s);
        tam /= 100;
        printf("%.2f\n", tam);
    }

}
