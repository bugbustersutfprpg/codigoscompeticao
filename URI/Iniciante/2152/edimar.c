#include <stdio.h>


void main(){

    int n, hora, minuto, fecha;

    scanf("%d", &n);

    while(n--){
        scanf("%d %d %d", &hora, &minuto, &fecha);

        if (fecha)
            printf("%02d:%02d - A porta abriu!\n", hora, minuto);
        else
            printf("%02d:%02d - A porta fechou!\n", hora, minuto);
    }

}
