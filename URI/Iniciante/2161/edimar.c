#include <stdio.h>


void main(){

    int n;
    double raiz, aux;

    scanf("%d", &n);
    raiz = 3;
    aux = 0;


    while(n--){
        aux = 1 / (6 + aux);
    }
    printf("%.10lf\n", raiz + aux);
}
