#include <stdio.h>


void main(){

    int n, altura, altAnterior, pico;

    scanf("%d", &n);
    scanf("%d", &altAnterior);
    scanf("%d", &altura);
    if (altAnterior == altura){
        puts("0");
        return;
    }
    if (altAnterior < altura)
        pico = 1;
    else
        pico = 0;

    altAnterior = altura;
    n -= 2;
    while(n--){
        scanf("%d", &altura);

        if (pico){
            if (altAnterior > altura){
                pico = 0;
                altAnterior = altura;
            }else{
                pico = 2;
                break;
            }
        }else{
            if (altAnterior < altura){
                pico = 1;
                altAnterior = altura;
            }else{
                pico = 2;
                break;
            }
        }
    }
    if (pico == 2)
        puts("0");
    else
        puts("1");
}
