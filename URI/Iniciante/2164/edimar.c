#include <stdio.h>
#include <math.h>

void main(){

    int n;
    double aux, cinco;

    scanf("%d", &n);

    cinco = sqrt(5);
    aux = ( pow(((1 + cinco) / 2), n) - pow(((1 - cinco) / 2), n) ) / cinco;

    printf("%.1lf\n", aux);
}
