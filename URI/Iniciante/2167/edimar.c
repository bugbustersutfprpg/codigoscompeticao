#include <stdio.h>


void main(){

    int n, i, aux, ant, queda;

    scanf("%d", &n);
    scanf("%d", &ant);

    queda = 0;
    i = 1;
    while(n--){
        scanf("%d", &aux);
        if (aux < ant){
            queda = 1;
            i++;
            break;
        }
        ant = aux;
        i++;
    }

    if (queda){
        printf("%d\n", i);
    }else
        puts("0");
}
