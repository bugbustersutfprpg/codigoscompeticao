#include <stdio.h>


void main(){

    int n, i, j, seguro;

    scanf("%d", &n);
    n++;

    int matriz[n][n];

    i = 0;
    while(i < n){
        j = 0;
        while(j < n){
            scanf("%d", &matriz[i][j++]);
        }
        i++;
    }

    n--;
    i = 0;
    while(i < n){
        j = 0;
        while(j < n){
            seguro = 0;
            if (matriz[i][j])
                seguro++;
            if (matriz[i][j+1])
                seguro++;
            if (matriz[i+1][j])
                seguro++;
            if (matriz[i+1][j+1])
                seguro++;
            if (seguro > 1)
                printf("S");
            else
                printf("U");
            j++;
        }
        puts("");
        i++;
    }
}
