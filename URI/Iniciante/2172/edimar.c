#include <stdio.h>
#include <iostream>

using namespace std;

int main(){

    int n;
    long long int v;

    while(scanf("%d %lli", &n, &v) != -1){
        if (!n && !v)
            break;
        printf("%lli\n", v * n);
    }

}
