#include <stdio.h>

int main(){

    char s[128];
    int i, cont;

    scanf("%s", s);

    i = 0;
    cont = 0;
    while(s[i])
        if (s[i++] == '1')
            cont++;

    printf("%s", s);
    if (cont%2)
        puts("1");
    else
        puts("0");
}
