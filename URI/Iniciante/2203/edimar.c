#include <stdio.h>
#include <iostream>
#include <math.h>

using namespace std;

int main(){

    int x, x1, y, y1, v, r, rc, k;
    float dist, fulga;

    while(scanf("%d %d %d %d %d %d %d", &x, &y, &x1, &y1, &v, &r, &rc) != -1){
        k = (x-x1)*(x-x1) + (y-y1)*(y-y1);
        dist = sqrt(k);

        fulga = v * 1.5;
        dist += fulga - r;

        if (dist > rc)
            puts("N");
        else
            puts("Y");
    }

}
