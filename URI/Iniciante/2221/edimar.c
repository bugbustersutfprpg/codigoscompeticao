#include <stdio.h>

int main(){

    int n, b, a, a1, d, d1, l, l1;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &b);

        scanf("%d %d %d", &a, &d, &l);
        scanf("%d %d %d", &a1, &d1, &l1);

        a = (a+d)/2;
        a1 = (a1+d1)/2;
        if ( (l%2) == 0)
            a += b;
        if ((l1%2) == 0)
            a1 += b;

        if (a < a1)
            puts("Guarte");
        else if (a > a1)
            puts("Dabriel");
        else
            puts("Empate");
    }
}
