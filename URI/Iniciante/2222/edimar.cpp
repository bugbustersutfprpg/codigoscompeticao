#include <iostream>

using namespace std;
char m[10001][61], ma[10001][61];

int interseccao(char ma[][61], char m[][61], int x, int y, int qx, int qy){

    int t = 0;
    if (qx < qy){
        for (int i = 1; i <= qx; i++){
            if (ma[y][ m[x][i] ] == 1){
                t++;
            }
        }
    }else{
        for (int i = 1; i <= qy; i++){
            if (ma[x][ m[y][i] ] == 1){
                t++;
            }
        }
    }
    return t;
}

int uniao(char ma[][61], char m[][61], int x, int y, int qx, int qy){

    int t = qx + qy;
    if (qx < qy){
        for (int i = 1; i <= qx; i++){
            if (ma[y][ m[x][i] ] == 1){
                t--;
            }
        }
    }else{
        for (int i = 1; i <= qy; i++){
            if (ma[x][ m[y][i] ] == 1){
                t--;
            }
        }
    }
    return t;
}

void zerar(char ma[][61], char m[][61], int k[], int qtd){
    for (int i = 1; i <= qtd; i++){
        for (int j = 1; j <= k[i]; j++){
            ma[i][ m[i][j] ] = 0;
        }
    }
}

int main(){

    int n, qtd, k[10001], op, x, y, t, q;

    cin >> n;

    while(n--){
        cin >> qtd;
        for (int i = 1; i <= qtd; i++){
            cin >> k[i];
            for (int j = 1; j <= k[i]; j++){
                cin >> op;
                if (ma[i][op] == 0){
                    ma[i][op] = 1;
                    m[i][j] = op;
                }else{
                    j--;
                    k[i]--;
                }
            }
        }

        cin >> q;
        for (int i = 0; i < q; i++){
            cin >> op >> x >> y;
            if (op == 1){
                t = interseccao(ma, m, x, y, k[x], k[y]);
            }else{
                t = uniao(ma, m, x, y, k[x], k[y]);
            }
            cout << t << endl;
        }
        if (n)
            zerar(ma, m, k, qtd);
    }
}
