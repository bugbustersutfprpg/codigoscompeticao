import java.io.*;
import java.util.*;
import java.lang.StringBuilder;

public class Main {

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		
		int contagem;
		int t = Integer.parseInt(br.readLine());
		String[] input;
		
		ArrayList<HashSet<Integer>> vetores = new ArrayList<>(1000);
		HashSet<Integer> conjunto;
		HashSet<Integer> aux = new HashSet<>(60);
		
		while(t-- > 0){
			
			vetores.clear();
			int numVetores = Integer.parseInt(br.readLine());

			while(numVetores-- > 0){

				input = br.readLine().split(" ");
				conjunto = new HashSet<>(60);
				
				for(int i = 1; i < input.length; i++)
					conjunto.add(Integer.parseInt(input[i]));
					
				vetores.add(conjunto);
			
			}
			
			int numPedidos = Integer.parseInt(br.readLine());
			
			while(numPedidos-- > 0){
			
				input = br.readLine().split(" ");
				int pedido = Integer.parseInt(input[0]);
				int conjunto1 = Integer.parseInt(input[1]) - 1;
				int conjunto2 = Integer.parseInt(input[2]) - 1;

				contagem = 0;
				aux.clear();
				
				if(pedido == 1){ // intersecção
				

					for(int a : vetores.get(conjunto1))
						aux.add(a);
						
					for(int b : vetores.get(conjunto2))
						if(aux.contains(b))
							contagem++;
					
				
				}
				
				else{ // união
				
					for(int a : vetores.get(conjunto1))
						aux.add(a);
					
					
					for(int a : vetores.get(conjunto2))
						aux.add(a);
					
					contagem = aux.size();
				
				}
				
				sb.append(contagem).append(System.lineSeparator());
			
			}
				
		
		}
		
		out.write(sb.toString().getBytes());
		out.flush();
	
	}
	
	
	

}
