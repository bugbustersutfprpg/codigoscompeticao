#include <stdio.h>
#include <math.h>

int main() {

    int n, i;

    scanf("%d", &i);
    while(i--){
        scanf("%d", &n);
        if (n == 1)
            puts("1");
        else
            printf("%d\n", (int)pow(2, n)-1);
    }
    return 0;
}
