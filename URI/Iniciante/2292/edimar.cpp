#include <iostream>

using namespace std;

int main(){
	
	int n, i, desligou;
	long long int c, d, e;
	string p;
	
	cin>>n;
	
	while(n--){
		cin>>p>>c;
		
		d = 0;
		e = 1;
		for(i = 0; i < p.length(); i++){
			if (p[i] == 'O'){
				d += e;
			}
			e *= 2;
		}
		//cout<<d<<endl;
		d += c;

		//cout<<d<<endl;
		
		for (i = 0; i < p.length(); i++){
			c = d%2;
			if (c == 1)
				p[i] = 'O';
			else
				p[i] = 'X';
			d /= 2;
		}
		
		cout<<p<<endl;
	}
	
}