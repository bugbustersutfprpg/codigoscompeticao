import java.io.*;
import java.util.StringTokenizer;

public class murillo{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		StringTokenizer st;
		
		int n = Integer.parseInt(br.readLine());
		String led;
		long sum;
		long total;
		long aux;
		
		for(int i = 0; i < n; i++){
		
			st = new StringTokenizer(br.readLine());
			led = st.nextToken();
			sum = Long.parseLong(st.nextToken());
			total = 0l;
			
			for(int j = 0; j < led.length(); j++){
			
				if(led.charAt(j) == 'O')
					total += 1l << j;
			
			}
			
			total += sum;
			
			for(int j = 0; j < led.length(); j++){
				if((total & 1l) == 0l)
					out.write('X');
				else
					out.write('O');
					
				total >>= 1;
			}
			
			out.write(System.lineSeparator().getBytes());
		
		}
	
		out.flush();
	}

}
