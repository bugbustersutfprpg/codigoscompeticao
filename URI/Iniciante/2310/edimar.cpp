#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
	
	int n, i, j, s, x, y, z, b, a, s1, b1, a1;
	string c;
	
	cin>>n;
	i = 0;
	s = 0; s1 = 0; b = 0; b1 = 0; a = 0; a1 = 0;
	
	while(i++ < n){
		cin>>c;
		cin>>x>>y>>z;
		s += x;
		b += y;
		a += z;	
		cin>>x>>y>>z;
		s1 += x;
		b1 += y;
		a1 += z;
	}
	
	if (s)
		printf("Pontos de Saque: %.2lf %%.\n", ( (double)s1/s) * 100.0);
	else
		printf("Pontos de Saque: 0.00 %%.\n");
	if (b)
		printf("Pontos de Bloqueio: %.2lf %%.\n", ( (double)b1/b) * 100.0);
	else
		printf("Pontos de Bloqueio: 0.00 %%.\n");
	if (a)
		printf("Pontos de Ataque: %.2lf %%.\n", ( (double)a1/a) * 100.0);
	else
		printf("Pontos de Ataque: 0.00 %%.\n");
}