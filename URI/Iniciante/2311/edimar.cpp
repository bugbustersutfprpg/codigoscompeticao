#include<iostream>
#include <stdio.h>

using namespace std;

int main(){

    int i, n;
    float j, nota, peso, menor, maior;
    string nome;

    cin>>n;
    while(n--){
        cin>>nome;
        cin>>peso;
        cin>>nota;
        menor = nota;
        maior = nota;
        i = 0;
        while(i++ < 6){
            cin>>j;
            nota += j;
            if (j < menor)
                menor = j;
            if (j > maior)
                maior = j;
        }

        nota -= maior+menor;

        cout<<nome;
        printf(" %.2f\n", nota*peso);
    }

}
