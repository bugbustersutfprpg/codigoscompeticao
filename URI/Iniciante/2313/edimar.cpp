#include <iostream>

using namespace std;

int main(){

	long long int a, b, c;

	cin>>a>>b>>c;
	
	if (a > b){
		b ^= a;
		a ^= b;
		b ^= a;
	}
	if (b > c){
		b ^= c;
		c ^= b;
		b ^= c;
		if (a > b){
			b ^= a;
			a ^= b;
			b ^= a;
		}
	}

	if (a == c){
		cout<<"Valido-Equilatero\nRetangulo: N\n";
	}else if ( (a+b) > c){
		if (a == b || b == c){
			cout<<"Valido-Isoceles\n";
			if ( (a*a + b*b) == (c*c) )
				cout<<"Retangulo: S\n";
			else
				cout<<"Retangulo: N\n";
		}else{
			cout<<"Valido-Escaleno\n";
			if ( (a*a + b*b) == (c*c) )
				cout<<"Retangulo: S\n";
			else
				cout<<"Retangulo: N\n";
		}
	}else{
		cout<<"Invalido\n";
	}

	return 0;
}