#include <stdio.h>
 
int main() {
 
    unsigned long long int i;
    
    while(1) {
        scanf("%llu", &i);
        if (i == -1)
            break;
        if (i > 0)
            printf("%llu\n", i-1);
        else
	        puts("0");
    }
    
 
    return 0;
}