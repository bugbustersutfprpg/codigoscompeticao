#include <iostream>

using namespace std;

int posicao(string letra, string morse[]){

    int i;
    for (i = 0; letra != morse[i]; i++){}
    return i;
}

int main(){

    string morse[] = {"=.===", "===.=.=.=", "===.=.===.=", "===.=.=", "=", "=.=.===.=", "===.===.=",
    "=.=.=.=", "=.=", "=.===.===.===", "===.=.===", "=.===.=.=", "===.===", "===.=", "===.===.===",
    "=.===.===.=", "===.===.=.===", "=.===.=", "=.=.=", "===", "=.=.===", "=.=.=.===", "=.===.===",
    "===.=.=.===", "===.=.===.===", "===.===.=.="};

    int n, t, a;
    char c;
    string resposta, letra, frase;

    cin >> n;
    getline(cin, resposta);

    while(n--){
        resposta = "";
        getline(cin, frase);
        t = frase.length();
        for (int i = 0; i < t; i++){
            if (frase[i] == '.'){
                if (frase[i+1] == '.'){
                    a = posicao(letra, morse);
                    resposta += a + 97;
                    if (frase[i+3] == '.'){
                        resposta += ' ';
                        i += 6;
                    }else{
                        i += 2;
                    }
                    letra = "";
                }else{
                    letra += frase[i];
                }
            }else{
                letra += frase[i];
            }
        }
        a = posicao(letra, morse);
        resposta += a + 97;
        letra = "";

        cout << resposta << endl;
    }

}
