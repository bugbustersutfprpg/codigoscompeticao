#include <iostream>

using namespace std;
int d;

int number(string s){
    int i, k = 0;
    for (i = 0; s[i] > 47; i++){
        k = k * 10 + s[i] - 48;
    }
    d = i-1;
    return k;
}

int main(){

    int t, n, total;
    string alimento;

    cin >> t;
    while(t){
        getline(cin, alimento);
        total = 0;
        while(t--){
            getline(cin, alimento);
            n = number(alimento);
            switch (alimento[2+d]){
                case 's':
                    total += 120 * n;
                    break;
                case 'g':
                    total += 70 * n;
                    break;
                case 'l':
                    total += 50 * n;
                    break;
                case 'b':
                    total += 34 * n;
                    break;
                default:
                    if (alimento[3+d] == 'o'){
                        total += 85 * n;
                    }else if (alimento[4+d] == 'm'){
                        total += 85 * n;
                    }else{
                        total += 56 * n;
                    }
            }
        }

        if (total < 110){
            cout << "Mais " << 110 - total;
        }else if (total > 130){
            cout << "Menos " << total - 130;
        }else{
            cout << total;
        }
        cout << " mg\n";
        cin >> t;
    }

}
