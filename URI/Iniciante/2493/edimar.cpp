#include <iostream>
#include <algorithm>
#include <stdio.h>

using namespace std;

int main(){

    char c;
    int n, j, a[50], b[50], r[50], qtd;
    string nome;
    string resposta[50];

    while(scanf("%d", &n) != -1){
        qtd = 0;
        for (int i = 0; i < n; i++){
            cin >> a[i] >> b[i] >> c >> r[i];
        }
        for (int i = 0; i < n; i++){
            cin >> nome >> j >> c;
            j--;
            switch (c){
                case '+':
                    if ( (a[j] + b[j]) != r[j]){
                        resposta[qtd++] = nome;
                    }
                    break;
                case '-':
                    if ( (a[j] - b[j]) != r[j]){
                        resposta[qtd++] = nome;
                    }
                    break;
                case '*':
                    if ( (a[j] * b[j]) != r[j]){
                        resposta[qtd++] = nome;
                    }
                    break;
                default:
                    if ( (a[j] + b[j]) == r[j] || (a[j] - b[j]) == r[j] || (a[j] * b[j]) == r[j] ){
                        resposta[qtd++] = nome;
                    }
            }
        }

        if (qtd == n){
            cout << "None Shall Pass!\n";
        }else if (qtd == 0){
            cout << "You Shall All Pass!\n";
        }else{
            sort(resposta, resposta + qtd);
            qtd--;
            for (int i = 0; i < qtd; i++){
                cout << resposta[i] << " ";
            }
            cout << resposta[qtd] << endl;
        }
    }
}
