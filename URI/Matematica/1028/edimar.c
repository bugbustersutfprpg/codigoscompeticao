#include <stdio.h>

void printint(short int a){
    short int n=0;
    char c[3];

    for (;a>9;a/=10)
        c[n++]=a%10+48;
    c[n]=a+48;
    for (;n>=0;n--)
        putchar(c[n]);
    puts("");
}

int main (){
    int n,resto,f,f1,f2;
    char s[10];
    gets(s);
    n= atoi(s);

    for ( ; n>0; n--){
        scanf("%d %d",&f1,&f2);

        if (f1>f2){
            while (f2 != 0){
                resto= f1%f2;
                f1= f2;
                f2= resto;
                f= f1;
            }
        }else{
            while (f1 != 0){
                resto= f2%f1;
                f2= f1;
                f1= resto;
                f= f2;
            }
        }
        printint(f);
    }
    return 0;
}