import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedOutputStream;

public class Main{

	public static void main(String args[]) throws IOException{
	
		int n;
		int a;
		int b;
		int r;
		
		String input[];
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		StringBuilder sb = new StringBuilder();
		
		n = Integer.parseInt(br.readLine());

		for(int i = 0; i < n; i++){
			input = br.readLine().split(" ");
			a = Integer.parseInt(input[0]);
			b = Integer.parseInt(input[1]);
			
			while(b != 0){
				r = a % b;
				a = b;
				b = r;
			}
			
			sb.append(a);
			sb.append(System.lineSeparator());

		}
		out.write(sb.toString().getBytes());	
		out.flush();
		out.close();
	
	
	}
	


}
