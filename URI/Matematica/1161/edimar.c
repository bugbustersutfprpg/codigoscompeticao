#include <stdio.h>


void main(){

    long long int m, n, i, j;

    while(scanf("%lli %lli", &m, &n) != -1){
        if (!m)
            m++;
        else{
            i = m;
            while(i-- > 1){
                m *= i;
            }
        }
        if (!n)
            n++;
        else{
            i = n;
            while(i-- > 1){
                n *= i;
            }
        }
        printf("%lli\n", m + n);
    }
}
