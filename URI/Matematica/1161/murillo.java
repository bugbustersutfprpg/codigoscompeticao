import java.io.BufferedReader;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.io.IOException;
import java.util.HashMap;
import java.math.BigInteger;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder sb = new StringBuilder();
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		
		HashMap<Integer, BigInteger> map = new HashMap<>(128);
		String input[];
		
		do{
			input = br.readLine().split(" ");
			sb.append(factorial(map, Integer.parseInt(input[0])).add(factorial(map, Integer.parseInt(input[1])))).
			append(System.lineSeparator());
		}while(br.ready());
		
		out.write(sb.toString().getBytes());
		out.flush();
	}
	
	public static BigInteger factorial(HashMap<Integer, BigInteger> map, int n){
	
		if(n == 0 && !map.containsKey(n)){
			map.put(n, new BigInteger("1"));
		}

		else if(!map.containsKey(n)){
			BigInteger b = factorial(map, n-1);
			map.put(n, b.multiply(new BigInteger(String.valueOf(n))));
		}
		
		return map.get(n);

	}

}
