#include <stdio.h>


void main(){

    unsigned long long int grao;
    int n, i;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &i);
        grao = 1;
        if (i == 64){
            grao = grao << --i;
            grao /= 12000;
            printf("%lli kg\n", grao * 2 + 1);
        }else{
            grao = grao << i;
            printf("%lli kg\n", grao / 12000);
        }
    }

}
