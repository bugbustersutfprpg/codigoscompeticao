#include <stdio.h>


void main(){

    int n, dias;
    double x;

    scanf("%d", &n);

    while(n--){
        scanf("%lf", &x);
        dias = 0;
        while(x > 1){
            x /= 2;
            dias++;
        }
        printf("%d dias\n", dias);
    }

}
