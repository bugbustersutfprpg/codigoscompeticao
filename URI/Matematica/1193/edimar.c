#include <stdio.h>
#include <string.h>


int binaryToDecimal(char s[]){

    int tam = strlen(s);
    int total = 0, potencia = 1;

    while(tam--){
        if (s[tam] == '1')
            total += potencia;
        potencia *= 2;
    }
    return total;
}


void main(){

    unsigned int n, i, j, k, de, dec;
    char c[8], s[32], s2[32];
    char hexa[16][5] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001",
                        "1010", "1011", "1100", "1101", "1110", "1111"};

    scanf("%d", &n);
    i = 1;
    while(n--){
        scanf("%s %s", s, c);
        j = 0;
        k = 1;

        if (c[0] == 'd'){
            sscanf(s, "%d", &de);
            dec = de;
            while(de > 1){
                s[j++] = (de % 2) + 48;
                de /= 2;
            }
            s2[0] = '1';
            while(j--){
                s2[k++] = s[j];
            }
            s2[k] = 0;
            printf("Case %d:\n%x hex\n%s bin\n\n", i, dec, s2);

        }else if (c[0] == 'h'){
            if (s[0] > 80)
                strcpy(s2, hexa[s[0] - 87]);
            else
                strcpy(s2, hexa[s[0] - 48]);

            while(s2[0] != '1'){
                k = 0;
                while(s2[k]){
                    s2[k] = s2[k+1];
                    k++;
                }
            }
            k = 1;
            while(s[k]){
                if (s[k] > 80)
                    strcat(s2, hexa[s[k] - 87]);
                else
                    strcat(s2, hexa[s[k] - 48]);
                k++;
            }

            dec = binaryToDecimal(s2);
            printf("Case %d:\n%d dec\n%s bin\n\n", i, dec, s2);
        }else{
            dec = binaryToDecimal(s);
            printf("Case %d:\n%d dec\n%x hex\n\n", i, dec, dec);
        }
        i++;
    }
}
