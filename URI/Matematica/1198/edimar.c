#include <stdio.h>
#include <math.h>

void main(){

    long long int n, m;

    while(scanf("%lli %lli", &m, &n) != -1){
        if (n > m)
            n = n - m;
        else
            n = m - n;
        printf("%lli\n", n);
    }
}
