#include <stdio.h>
#include <string.h>
#include <math.h>


int binaryToDecimal(char s[]){

    int tam = strlen(s);
    int total = 0, potencia = 1;

    while(tam--){
        if (s[tam] == '1')
            total += potencia;
        potencia *= 2;
    }
    return total;
}


void main(){

    unsigned int n, i, j, k, de, dec;
    char c[8], s[32], s2[32];
    char hexa[16][5] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001",
                        "1010", "1011", "1100", "1101", "1110", "1111"};

    while(1){
        scanf("%s", s);
        if (s[0] == '-')
            break;
        if (s[1] != 'x')
            printf("0x%X\n", atoi(s));
        else{
            i = 0;
            while(s[i]){
                s[i] = s[i+2];
                i++;
            }
            if (s[0] > 80)
                strcpy(s2, hexa[s[0] - 87]);
            else
                strcpy(s2, hexa[s[0] - 48]);

            while(s2[0] != '1'){
                k = 0;
                while(s2[k]){
                    s2[k] = s2[k+1];
                    k++;
                }
            }
            k = 1;
            while(s[k]){
                if (s[k] > 80)
                    strcat(s2, hexa[s[k] - 87]);
                else
                    strcat(s2, hexa[s[k] - 48]);
                k++;
            }

            dec = binaryToDecimal(s2);

            printf("%d\n", dec);
        }
    }
}

