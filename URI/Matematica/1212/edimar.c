#include <stdio.h>
#include <string.h>

int main(){

    int carry, cay, t1, t2;
    char x[10], y[10];

    while(1){
        scanf("%s %s", x, y);
        t1 = strlen(x);
        t2 = strlen(y);
        if (x[0] == '0' && y[0] == '0' && t1 == 1 && t2 == 1)
            break;

        carry = 0;
        cay = 0;


        if (t1 > t2){
            while(t2--){
                if ((x[--t1] + y[t2] + cay) >= 106){
                    cay = 1;
                    carry++;
                }else
                    cay = 0;
            }
            while(t1--){
                if ((x[t1] + cay) >= 58)
                    carry++;
                else
                    break;
            }
        }else{
            while(t1--){
                if ((x[t1] + y[--t2] + cay) >= 106){
                    cay = 1;
                    carry++;
                }else
                    cay = 0;
            }
            while(t2--){
                if ((y[t2] + cay) >= 58)
                    carry++;
                else
                    break;
            }
        }

        if (!carry)
            puts("No carry operation.");
        else if (carry == 1)
            puts("1 carry operation.");
        else
            printf("%d carry operations.\n", carry);
    }
    return 0;
}
