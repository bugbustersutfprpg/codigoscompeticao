#include <stdio.h>


void main(){

    int n, one;
    long long int vez, mult;

    while(scanf("%d", &n) != -1){

        vez = 1;
        mult = 10%n;
        one = 1;

        while(vez){
            one++;
            vez = (vez * mult + 1) % n;
        }

        printf("%d\n", one);
    }
}
