#include <stdio.h>


int main(){
	
	int i, n, aluno, cont, nota[1000];
	double media;
	
	scanf("%d", &n);
	
	while(n--){
		
		scanf("%d", &aluno);
		
		media = 0;
		i = 0;
		while(i < aluno){
			scanf("%d", &nota[i]);
			media += nota[i++];
		}
		media /= aluno;
		
		cont = 0;
		i = 0;
		while(i < aluno){
			if (nota[i++] > media)
				cont++;
		}
		
		media = (double)cont/aluno;
		media *= 100;
		
		printf("%.3lf%%\n", media);
	}
	
}