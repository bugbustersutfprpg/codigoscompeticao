#include <iostream>
#include <stdio.h>
#include <cmath>

using namespace std;

int main(){

    int a, b, c;
    double semi, raio, raioMaior, area, circ, circao;

    while(scanf("%d %d %d", &a, &b, &c) != -1){
        semi = (a+b+c)/2.0;
        area = sqrt(semi*((semi-a)*(semi-b)*(semi-c) ) );
        raioMaior = (a*b*c)/(area*4);
        raio = area/semi;
        circ = raio*raio*M_PI;
        circao = raioMaior*raioMaior*M_PI - area;
        
        cout.precision(4);
        cout.setf(ios::fixed);
        cout<<circao<<" "<<area-circ<<" "<<circ <<endl;
    }

}