#include <stdio.h>
#include <math.h>

void main(){

    int n, raiz, i;
    long long int num;

    scanf("%d", &n);

    while(n--){
        scanf("%lli", &num);
        if (num == 2){
            puts("Prime");
            continue;
        }
        if (num%2){
            raiz = (int)sqrt(num);
            for(i = 3; i <= raiz; i += 2){
                if (!(num % i))
                    break;
            }
            if (i > raiz)
                puts("Prime");
            else
               puts("Not Prime");
        }else
            puts("Not Prime");
    }
}
