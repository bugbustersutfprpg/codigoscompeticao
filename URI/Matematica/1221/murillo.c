#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stddef.h>

#define VECTOR_SIZE 46342 /* sqrt (2^31) */
#define PRIME "Prime"
#define NOT_PRIME "Not Prime"

unsigned int vet[VECTOR_SIZE];
unsigned int primes[VECTOR_SIZE / 2];

unsigned int numPrimes = 0;

int main(){

	unsigned int i = 0;
	unsigned int numTests;

	scanf("%u", &numTests);

	unsigned int test[numTests];
	unsigned int greater = 0;
	
	while(i < numTests){
		scanf("%u", &test[i]);
		if(test[i] > greater) greater = test[i];
		i++;
	}

	unsigned int max = (unsigned int) sqrt((double) greater);
	unsigned int power;
	unsigned int aux;
	unsigned int j;

	i = 2;
	while(1){

		primes[numPrimes++] = i;

		power = i * i;
		if(power > max) break;

		j = 0;
		aux = power + i * j;

		while(aux < max){
			vet[aux] = 1;
			j++;
			aux = power + i * j;
		}
		
		while(i < max && vet[++i] == 1);
		if(i >= max) break;

	}

	i = 0;
	while(i < numTests){

		max = (unsigned int) sqrt((double) test[i]);
		j = 0;
		while(j < numPrimes){
			if(primes[j] > max) {
				j = numPrimes;
				break;
			}
			else if(test[i] % primes[j] == 0) {
				puts(NOT_PRIME);
				break;
			}
			j++;
		}

		if(j == numPrimes) puts(PRIME);

		i++;
	}

	return 0;
}
