#include <stdio.h>


void main(){

    int n, a, b, i;

    scanf("%d", &n);

    while(n--){
        scanf("%d %d", &a, &b);

        a = a - b;
        if (!a){
            puts("encaixa");
            continue;
        }

        i = 10;
        while( !(a%i) )
            i *= 10;

        i /= 10;

        if (i > b)
            puts("encaixa");
        else
            puts("nao encaixa");
    }

}
