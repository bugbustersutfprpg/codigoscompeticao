#include <stdio.h>
#include <math.h>


void main(){

    int distancia, policia, bandido;
    double vb, vp;

    while(scanf("%d %d %d", &distancia, &bandido, &policia) != -1){

        if (bandido >= policia)
            puts("N");
        else{
            vb = (double)12 / bandido;

            vp = sqrt(144 + distancia * distancia);

            if (vp > (policia * vb))
                puts("N");
            else
                puts("S");
        }
    }
}
