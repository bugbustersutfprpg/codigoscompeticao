#include <stdio.h>
#include <string.h>


void main(){

    char s[32];
    int centavos, cont, i, tam;

    while(scanf("%s", s) != -1){
        scanf("%d", &centavos);

        tam = strlen(s);
        i = (tam-1) / 3 + tam;
        cont = 0;

        while(tam){
            s[i--] = s[tam--];
            if (cont++ == 3){
                cont = 1;
                s[i--] = ',';
            }
        }

        printf("$%s.%02d\n", s, centavos);
    }

}
