#include <stdio.h>

void main(){

    int n;
    unsigned long long int result;

    scanf("%d", &n);

    while(n){
        result = 0;

        while(n){
            result += n * n;
            n--;
        }

        printf("%llu\n", result);
        scanf("%d", &n);
    }

}
