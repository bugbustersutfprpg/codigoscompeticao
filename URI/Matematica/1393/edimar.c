#include <stdio.h>
#include <math.h>


void main(){

    long long int ant, fib;
    int n;

    scanf("%d", &n);

    while(n){
        fib = 2;
        ant = 1;

        while(--n){
            fib += ant;
            ant = fib - ant;
        }


        printf("%lli\n", ant);
        scanf("%d", &n);
    }

}
