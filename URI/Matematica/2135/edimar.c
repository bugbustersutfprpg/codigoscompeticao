#include <stdio.h>
#define Initial 200000000

void main(){

    int n, i, valor;
    long long int j, v;

    j = 1;
    scanf("%d", &n);
    while(1){
        valor = Initial;
        v = 0;
        while(n--){
            scanf("%d", &i);
            if (i == v){
                valor = i;
                while(n--){
                    scanf("%d", &i);
                }
                break;
            }
            v += i;
        }

        if (valor != Initial)
            printf("Instancia %lli\n%d\n", j, valor);
        else
            printf("Instancia %lli\nnao achei\n", j);
        puts("");
        if (scanf("%d", &n) == -1)
            break;
        
        j++;
    }
}

