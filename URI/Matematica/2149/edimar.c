#include <stdio.h>


void main(){

    int n, i;
    unsigned long long int result, fib;

    while(scanf("%d", &n) != -1){
        result = 0;
        fib = 1;
        i = 1;

        while(i++ < n){
            fib += result;
            result = fib - result;
            if (i++ < n){
                if (i == 17){
                    result = fib;
                    break;
                }
                fib *= result;
                result = fib / result;
            }
        }

        printf("%llu\n", result);
    }

}
