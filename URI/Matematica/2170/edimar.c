#include <stdio.h>

int main(){

    int j = 1;
    double x, y, v;

    while(scanf("%lf %lf", &x, &y) != EOF){

        v = ((y/x)-1)*100;
        printf("Projeto %d:\nPercentual dos juros da aplicacao: %.2lf %%\n\n", j++, v);
    }
}
