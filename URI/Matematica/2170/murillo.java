import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.io.BufferedOutputStream;
import java.text.DecimalFormat;

public class Main{

	public static void main(String[] args) throws IOException{
	
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedOutputStream out = new BufferedOutputStream(System.out);
		
		StringBuilder sb = new StringBuilder();
		DecimalFormat df =  new DecimalFormat("0.00");
		
		int projectCount = 1;
		double a;
		double b;
		double c;
		String[] input;
		
		do{
		
			input = br.readLine().split(" ");
			a = Double.parseDouble(input[0]);
			b = Double.parseDouble(input[1]);
			
			c = b * 100.0 / a;
			
			sb.append("Projeto ").append(projectCount++).append(":")
			.append(System.lineSeparator())
			.append("Percentual dos juros da aplicacao: ")
			.append(df.format(c - 100.0d).replaceFirst(",", "."))
			.append(" %")
			.append(System.lineSeparator())
			.append(System.lineSeparator());
			
			
		
		}while(br.ready());
		
		out.write(sb.toString().getBytes());
		out.flush();
	
	}

}
