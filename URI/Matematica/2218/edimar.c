#include <stdio.h>


void main(){

    int n, i, num;
    unsigned long long int result;

    scanf("%d", &n);

    while(n--){
        scanf("%d", &num);

        i = num / 2;
        if (num%2){
            result = num * i + num + 1;
        }else{
            result = num * i + i + 1;
        }

        printf("%llu\n", result);
    }

}
