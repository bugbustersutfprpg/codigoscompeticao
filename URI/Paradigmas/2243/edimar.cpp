#include <iostream>

using namespace std;

int main(){
	
	int i, maior, alt, n, v[50000];
	
	cin>>n;
	
	alt = 0;
	for(i = 0; i < n; i++){
		cin>>v[i];
		if (v[i] > alt){
			alt++;
			v[i] = alt;
		}else{
			alt = v[i];
		}	
	}
	
	alt = 0;
	maior = 1;
	for (i = n-1; i > 0; i--){
		if (v[i] > alt){
			alt++;
			v[i] = alt;
		}else{
			if (maior < alt)
				maior = alt;
			alt = v[i];
		}
	}
	
	cout<< maior <<endl;
}