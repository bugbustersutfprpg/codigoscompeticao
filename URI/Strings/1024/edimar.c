#include <stdio.h>
#include <string.h>


void main(){

    char s[1001], resposta[1001];
    int n, i, j, tam, metade;

    scanf("%d", &n);

    while(n--){
        scanf(" %[^\n]s", s);
        tam = strlen(s);
        if (tam%2)
            metade = tam/2+1;
        else
            metade = tam/2;
        i = 0;
        while(tam--){
            if ( (s[tam] > 64 && s[tam] < 91) || (s[tam] > 96 && s[tam] < 123) ){
                resposta[i] = s[tam] + 3;
            }else{
                resposta[i] = s[tam];
            }
            if (metade > tam){
                resposta[i]--;
            }
            i++;
        }
        resposta[i] = '\0';

        printf("%s\n", resposta);
    }

}
