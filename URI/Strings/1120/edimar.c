#include <stdio.h>
#include <string.h>


void main(){

    char s[101];
    char c;
    int tam, i, j, k;

    while(1){
        scanf(" %c %s", &c, s);
        if (c == '0')
            break;

        tam = strlen(s);
        i = 0;
        while(i < tam){
            if (s[i] == c){
                j = i;
                k = j + 1;
                if (!i){
                    while(s[k]){
                        if (s[k] == '0')
                            k++;
                        else
                            break;
                    }
                }
                while(s[k]){
                    s[j++] = s[k++];
                }
                s[j] = 0;
                tam -= k - j;
                i--;
            }
            i++;
        }

        if (!tam){
            s[0] = '0';
            s[1] = 0;
        }

        printf("%s\n", s);
    }

}
