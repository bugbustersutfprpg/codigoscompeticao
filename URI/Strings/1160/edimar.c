#include <stdio.h>
#include <string.h>


void main(){

    int vetor[10] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
    int n, total, i;
    char s[101];

    scanf("%d", &n);
    while(n--){
        scanf("%s", s);

        total = 0;
        i = 0;
        while(s[i]){
            total += vetor[s[i] - 48];
            i++;
        }

        printf("%d leds\n", total);
    }

}
