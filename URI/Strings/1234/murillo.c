#include <stdio.h>
#include <string.h>

#define INPUT_SIZE 52
#define CASE_DIFFERENCE 32

char upperCase(char s){

	if(s >= 'a' && s <= 'z') 
		return s - CASE_DIFFERENCE;

	return s;

}

char lowerCase(char s){

	if(s >= 'A' && s <= 'Z')
		return s + CASE_DIFFERENCE;

	return s;

}

void dancing(char *s, unsigned int len){

	if(s == NULL) return;

	unsigned int i = 0;
	char toggle = 1;

	while(i < len){
	
		if(s[i] == ' '){ // skip space
			i++;
			continue;
		}

		if(toggle){
			s[i] = upperCase(s[i]);
			toggle = 0;
		}

		else{
			s[i] = lowerCase(s[i]);
			toggle = 1;
		}

		i++;
		
	}

	s[i-1] = '\0';

}

int main(int argc, char **argv){

	unsigned int len;
	char input[INPUT_SIZE];

	while(fgets(input, INPUT_SIZE, stdin) != NULL){
		len = strlen(input);

		dancing(input, len);
		printf("%s\n", input); 
	}

	return 0;
}
