#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

int main(){
	
	int n, j, t;
	char s[101], s2[101];
	
	scanf("%d", &n);
	getchar();
	
	while(n--){
		scanf("%[^\n]", s);
		getchar();
		
		t = strlen(s);
			
		j = 0;
		for (int i = t/2-1; i >= 0; i--){
			s2[j++] = s[i];
		}

		for (int i = t-1; i >= t/2; i--){
			s2[j++] = s[i];
		}
		s2[t] = 0;
	
		printf("%s\n", s2);
	}
	
}