#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define MAX 110


int main(){

	int n;
	int len;
	int i;
	int lim;
	char s[MAX];
	fgets(s, MAX, stdin);
	n = atoi(s);

	while(n--){
		fgets(s, MAX, stdin);
		len = strlen(s);
		len-=2;
		i = len >> 1;
		lim = i;

		while(i >= 0){
			printf("%c", s[i--]);
		}
		i = len;
		while(i > lim){
			printf("%c", s[i--]);
		}
		printf("\n");
	}

	return 0;
}
