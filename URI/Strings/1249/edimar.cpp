#include <iostream>
#include <stdio.h>

using namespace std;

int main(){

    string s;

    getline(cin, s);

    do{
        for (int i = 0; i < s.length(); i++){
            if (s[i] >= 'a' && s[i] <= 'z'){
                if (s[i] > 'm')
                    s[i] -= 26;
                s[i] += 13;
            }else if (s[i] >= 'A' && s[i] <= 'Z'){
                s[i] += 13;
                if (s[i] > 'Z') s[i] = s[i] - 26;
            }
        }

        cout << s << endl;
        getline(cin, s);
    }while(!feof(stdin));
}
