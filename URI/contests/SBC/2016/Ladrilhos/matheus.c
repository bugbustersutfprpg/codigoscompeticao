#include <stdio.h>

int m[201][201];
int pd[201][201];

int calcPD(int i, int j, int h, int w, int li, int lj, int c){
	if(i < 0 || j < 0 || i >= h || j >= w || m[i][j] != c)
		return 0;
	
	int cont = 1;
	pd[i][j] = 1;
	if(!pd[i-1][j])
		cont += calcPD(i - 1, j, h, w, i, j, c);
	
	if(!pd[i+1][j])
		cont += calcPD(i + 1, j, h, w, i, j, c);
	
	if(!pd[i][j-1])
		cont += calcPD(i, j - 1, h, w, i, j, c);
	
	if(!pd[i][j+1])
		cont += calcPD(i, j + 1, h, w, i, j, c);
	
	return cont;
}

int calcula(int h, int w){
	int menor = calcPD(0, 0, h, w, 0, 0, m[0][0]), i, j;
	for(i = 1; i < h; i++)
		for(j = 1; j < w; j++)
			if(!pd[i][j]){
				int aux = calcPD(i, j, h, w, i, j, m[i][j]);
				if(menor > aux)
					menor = aux;
			}
	return menor;
}

int main(){
	
	int h, w, i, j;
	scanf(" %d %d", &h, &w);
	for(i = 0; i < h; i++)
		for(j = 0; j < w; j++)
			scanf(" %d", *(m + i) + j);
		
	printf("%d\n", calcula(h, w));
	
	return 0;
}